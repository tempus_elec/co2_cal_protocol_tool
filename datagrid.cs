﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		int MAX_DG_COLUMN_NUMBER = 19;
		string[] column_name = {    "Time", "No", "st0", "nt0", "st1", "nt1", "dC", "sC",
									"dR",	"sR", "adc", "ratio", "ppm", "temp", "ts", "gain", "Winsen", "Rsv0", "Rsv1" };

		public void SetupDataGridView()
		{
			this.Controls.Add(dataGridView1);
			// DataGridView의 컬럼 갯수를 설정합니다.
			dataGridView1.ColumnCount = MAX_DG_COLUMN_NUMBER;

			// DataGridView에 컬럼을 추가합니다.
			for (int i = 0; i < MAX_DG_COLUMN_NUMBER; i++)
			{
				dataGridView1.Columns[i].Name = column_name[i];
				dataGridView1.Columns[column_name[i]].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			}

			for (int i = 0; i < MAX_DG_COLUMN_NUMBER; i++)
			{
				dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			}
			dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dataGridView1.RowPrePaint += new DataGridViewRowPrePaintEventHandler(this.dataGridView1_RowPrePaint);
		}

		public void DataGridViewClear(bool gridNum)
		{
			dataGridView1.Rows.Clear();
			//dataGridView2.Refresh();		// 화면이 깜빡거려서 눈이 아프다
		}

		public void PopulateDataGridView(string[][] str)
		{
			try
			{
				{
					if (dataGridView1.InvokeRequired)
					{
						dataGridView1.Invoke(new MethodInvoker(delegate ()
						{
							dataGridView1.Rows.Clear();

							if(sPort_Tempus1 != null)
							{
								dataGridView1.Rows.Add(str[0]);
								CSV_SAVE(ConvertStringArrayToString(str[0]));
							}

							if(sPort_Tempus2 != null)
							{
								dataGridView1.Rows.Add(str[1]);
								CSV_SAVE(ConvertStringArrayToString(str[1]));
							}

							if(sPort_Tempus3 != null)
							{
								dataGridView1.Rows.Add(str[2]);
								CSV_SAVE(ConvertStringArrayToString(str[2]));
							}

							if(sPort_Tempus4 != null)
							{
								dataGridView1.Rows.Add(str[3]);
								CSV_SAVE(ConvertStringArrayToString(str[3]));
							}
						}));
					}
					else
					{
						dataGridView1.Rows.Clear();

						if (sPort_Tempus1 != null)
						{
							dataGridView1.Rows.Add(str[0]);
							CSV_SAVE(ConvertStringArrayToString(str[0]));
						}

						if(sPort_Tempus2 != null)
						{
							dataGridView1.Rows.Add(str[1]);
							CSV_SAVE(ConvertStringArrayToString(str[1]));
						}

						if(sPort_Tempus3 != null)
						{
							dataGridView1.Rows.Add(str[2]);
							CSV_SAVE(ConvertStringArrayToString(str[2]));
						}

						if(sPort_Tempus4 != null)
						{
							dataGridView1.Rows.Add(str[3]);
							CSV_SAVE(ConvertStringArrayToString(str[3]));
						}
					}
				}
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message + '\n');
			}
		}

		private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			try
			{
				string str = (string)dataGridView1.Rows[e.RowIndex].Cells[2].Value;

				if (str == null)
					return;

			}
			catch (Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message + "\n");
			}
		}
	}
}
