﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CO2_CAL_Protocol_Tool
{
    public partial class Form1 : Form
    {
        public void CheckBoxSet(CheckBox cb, bool set)
        {
            try
            {
                if (cb.InvokeRequired) { cb.Invoke(new MethodInvoker(delegate () { cb.Checked = set; })); }
                else { cb.Checked = set; }
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
            }
        }

        public bool CheckBoxGet(CheckBox cb)
        {
            bool set = false;

            try
            {
                if (cb.InvokeRequired) { cb.Invoke(new MethodInvoker(delegate () { set = cb.Checked; })); }
                else { set = cb.Checked; }

                return set;
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
                return set;
            }
        }

        public void TextBoxSet(TextBox tb, string str)
        {
            try
            {
                if (tb.InvokeRequired) { tb.Invoke(new MethodInvoker(delegate () { tb.Text = str; })); }
                else { tb.Text = str; }
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
            }
        }

        public string TextBoxGet(TextBox tb)
        {
            string str = "";

            try
            {
                if (tb.InvokeRequired) { tb.Invoke(new MethodInvoker(delegate () { str = tb.Text; })); }
                else { str = tb.Text; }

                return str;
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
                return str;
            }
        }

        public void CheckTextBoxSet(CheckBox cb, TextBox tb, string str)
        {
            try
            {
                if (cb.Checked)
                {
                    TextBoxSet(tb, str);
                }
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
            }
        }

        public string CheckTextBoxGet(CheckBox cb, TextBox tb)
        {
            string str = "";

            try
            {
                if (cb.Checked)
                {
                    str = TextBoxGet(tb);
                    return str;
                }

                return str;
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
                return str;
            }
        }

        public void ChartCheckBox(Chart ch, CheckBox cb, UInt32 count, string series, double val)
        {
            try
            {
                if (ch.InvokeRequired)
                {
                    ch.Invoke(new MethodInvoker(delegate ()
                    {
                        try
                        {
                            if (cb.Checked)
                            {
                                ch.Series[series].Points.AddXY(count, val);
                            }

                            ch.Invalidate();
                        }
                        catch (Exception ex)
                        {
                            ERR(ex.Message + '\n');
                        }
                    }));
                }
                else
                {
                    try
                    {
                        if (cb.Checked)
                        {
                            ch.Series[series].Points.AddXY(count, val);
                        }

                        ch.Invalidate();
                    }
                    catch (Exception ex)
                    {
                        ERR(ex.Message + '\n');
                    }
                }
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
            }
        }

        public void ChartSet(Chart ch, UInt32 count, string series, double val)
        {
            try
            {
                if (ch.InvokeRequired)
                {
                    ch.Invoke(new MethodInvoker(delegate ()
                    {
                        try
                        {
                            ch.Series[series].Points.AddXY(count, val);

                            ch.Invalidate();
                        }
                        catch (Exception ex)
                        {
                            ERR(ex.Message + '\n');
                        }
                    }));
                }
                else
                {
                    try
                    {
                        ch.Series[series].Points.AddXY(count, val);

                        ch.Invalidate();
                    }
                    catch (Exception ex)
                    {
                        ERR(ex.Message + '\n');
                    }
                }
            }
            catch (Exception ex)
            {
                ERR(ex.Message);
            }
        }

        public void CheckBoxUpdate(CheckBox cb, bool check)
        {
            if (cb.InvokeRequired)
            {
                cb.Invoke(new MethodInvoker(delegate () { cb.Checked = check; }));
            }
            else
            {
                cb.Checked = check;
            }
        }

        public void TextBoxUpdate(TextBox tb, string str)
        {
            if (tb.InvokeRequired)
            {
                tb.Invoke(new MethodInvoker(delegate () { tb.Text = str; }));
            }
            else
            {
                tb.Text = str;
            }
        }

        public void RadioCheckUpdate(RadioButton rb, bool set)
        {
            rb.Checked = set;
        }
    }
}
