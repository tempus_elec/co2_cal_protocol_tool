﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
    public partial class Form1 : Form
    {
        SerialPort sPort_Elt = null;
        double elt_ppm = 0;

        void SerialPort_Elt_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int intRecSize = sPort_Elt.BytesToRead;
            string str = null;
            string[] spData;

            try
            {
                if (intRecSize != 0)
                {
                    try
                    {
                        str = sPort_Elt.ReadLine();
                        LOG("ELT : " + str + "\n");
                    }
                    catch (Exception ex)
                    {
                        ERR(CallerName() + ex.Message);
                    }
                    spData = str.Split(' ');
                    if (spData[1] == "ppm\r")
                    {
                        elt_ppm = Convert.ToDouble(spData[0]);
                        update_elt_ppm(spData[0]);
                    }
                    else if (spData[2] == "ppm\r")
                    {
                        elt_ppm = Convert.ToDouble(spData[1]);
                        update_elt_ppm(spData[1]);
                    }
                    else if (spData[3] == "ppm\r")
                    {
                        elt_ppm = Convert.ToDouble(spData[2]);
                        update_elt_ppm(spData[2]);
                    }
                    else if (spData[4] == "ppm\r")
                    {
                        elt_ppm = Convert.ToDouble(spData[3]);
                        update_elt_ppm(spData[3]);
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)  // CS0168
            {
                LOG("uart rx error : " + ex.Message + '\n');
            }
        }

        private void UartOpenElt(string comName)
        {
            try
            {
                if (null == sPort_Elt)
                {
                    if (comName == null)
                    {
                        LOG("Please select serial port!!!");
                    }
                    else
                    {
                        sPort_Elt = new SerialPort();
                        sPort_Elt.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Elt_DataReceived);
                        sPort_Elt.PortName = comName;
                        sPort_Elt.BaudRate = 38400;
                        sPort_Elt.DataBits = (int)8;
                        sPort_Elt.Parity = Parity.None;
                        sPort_Elt.StopBits = StopBits.One;
                        sPort_Elt.ReadTimeout = -1;
                        sPort_Elt.WriteTimeout = -1;
                        sPort_Elt.NewLine = "\n";
                        sPort_Elt.Open();
                    }
                }

                if (sPort_Elt.IsOpen)
                {
                    LOG(CallerName() + "Serial port connected... : )\n");
                    CheckBoxSet(checkBoxElt, true);
                    //PrdTimerStart();
                }
                else
                {
                    if (comboBoxElt.SelectedItem != null)
                        LOG(CallerName() + "Serial port is in use\n");
                }
            }
            catch (System.Exception ex)
            {
                UartCloseElt();
                ERR(ex.Message);
            }
        }

        private void UartCloseElt()
        {
            try
            {
                if (null != sPort_Elt)
                {
                    if (sPort_Elt.IsOpen)
                    {
                        sPort_Elt.Close();
                        sPort_Elt.Dispose();
                        LOG(CallerName() + "Serial port has been closed\n");
                    }
                    else
                    {
                        LOG(CallerName() + "Serial port closed\n");
                    }
                    sPort_Elt = null;
                    CheckBoxUpdate(checkBoxElt, false);
                }
            }
            catch (Exception ex)  // CS0168
            {
                ERR(ex.Message);
            }
        }

        private void update_elt_ppm(string str)
        {
            TextBoxSet(textBoxElt, str);
        }
    }
}

