﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		private object lockObject = null;
		UInt32 counter = 0;

		public void PrdTimerStart()
		{
			if(timer1.Enabled)
			{
#if true
                timer1.Stop();
                buttonStop.Text = "Mon Start";
#endif
            }
			else
			{
				timer1.Start();
                buttonStop.Text = "Mon Stop";

                if (lockObject == null)
					lockObject = new object();
			}
		}

		public void PrdTimer()
		{
			try
			{
				lock (lockObject)
				{
                    //if ((sPort_Ref != null) && (checkBoxRef.Checked))
                    if ((sPort_Ref != null) && (checkBoxSenseAir.Checked))
                    {
						request_ref();
					}

					if ((sPort_Tempus1 != null) && (checkBoxModule1.Checked))
					{
						request_tempus1();
					}
					if ((sPort_Tempus2 != null) && (checkBoxModule2.Checked))
					{
						request_tempus2();
					}

					if ((sPort_Tempus3 != null) && (checkBoxModule3.Checked))
					{
						request_tempus3();
					}

					if ((sPort_Tempus4 != null) && (checkBoxModule4.Checked))
					{
						request_tempus4();
					}

					if (sPort_Ext != null)
					{
						request_ext();
					}

                    if( (sPort_Winsen != null) && (checkBoxWinsen.Checked) )
                    {
                        request_winsen();
                    }

                    if ((sPort_Cubic != null) && (checkBoxCubic.Checked))
                    {
                        request_cubic();
                    }

                    if ((sPort_Elt != null) && (checkBoxElt.Checked))
                    {
                        
                    }

                    if ((sPort_Figaro != null) && (checkBoxFigaro.Checked))
                    {
                        request_figaro();
                    }

                    PopulateDataGridView(DbgInfo);
					chart_co2_update(counter, senseair_co2, dbgInfo[0].ppm, dbgInfo[1].ppm, dbgInfo[2].ppm, dbgInfo[3].ppm, winsen_ppm, cm1106_ppm, elt_ppm, figaro_ppm);
                    chart_dcdr_update(counter++, dbgInfo[0].dC, dbgInfo[0].dR, dbgInfo[1].dC, dbgInfo[1].dR, dbgInfo[2].dC, dbgInfo[2].dR, dbgInfo[3].dC, dbgInfo[3].dR);

#if false
                    chart_a0_ih_update(counter, dbgInfo[0].CAL_A0, dbgInfo[0].CAL_Ih, dbgInfo[0].REF_A0, dbgInfo[0].REF_Ih, dbgInfo[1].CAL_A0, dbgInfo[1].CAL_Ih, dbgInfo[1].REF_A0, dbgInfo[1].REF_Ih
                                    , dbgInfo[2].CAL_A0, dbgInfo[2].CAL_Ih, dbgInfo[2].REF_A0, dbgInfo[2].REF_Ih, dbgInfo[3].CAL_A0, dbgInfo[3].CAL_Ih, dbgInfo[3].REF_A0, dbgInfo[3].REF_Ih); 
#endif
                }
			}
			catch(Exception ex)
			{
				ERR(CallerName() + ex.Message);
			}
		}
	}
}
