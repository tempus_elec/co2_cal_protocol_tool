﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
    public partial class Form1 : Form
    {
        SerialPort sPort_Winsen = null;

        const int WINSEN_MSG_LEN = 9;
        double winsen_ppm = 0;
        byte[] winsenBuff = new byte[32];
        int msgOffsetWin = 0;

        private void SerialPort_Winsen_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int intRecSize = sPort_Winsen.BytesToRead;

            mtx.WaitOne();

            try
            {
                if (intRecSize != 0)
                {
                    byte[] buff = new byte[intRecSize];
                    int iii;

                    iii = sPort_Winsen.Read(buff, 0, intRecSize);
                    if ( (intRecSize + msgOffsetWin ) > WINSEN_MSG_LEN)
                    {
                        ERR("Rx got too long..." + intRecSize + "\n");
                        msgOffsetWin = 0;
                    }
                    else
                    {
                        if (msgOffsetWin == 0)
                        {
                            Buffer.BlockCopy(buff, 0, winsenBuff, msgOffsetWin, intRecSize);
                            msgOffsetWin = intRecSize;
                        }
                        else
                        {
                            Buffer.BlockCopy(buff, 0, winsenBuff, msgOffsetWin, intRecSize);
                            msgOffsetWin += intRecSize;
                        }

                        if (msgOffsetWin == WINSEN_MSG_LEN)
                        {
                            if ((winsenBuff[0] == 0xFF) && (winsenBuff[1] == 0x86))
                            {
                                winsen_ppm = (double)((winsenBuff[2] << 8) | winsenBuff[3]);
                                update_winsen_ppm(winsen_ppm.ToString());
                            }
                            msgOffsetWin = 0;
                        }
                        else if (msgOffsetWin > WINSEN_MSG_LEN)
                        {
                            msgOffsetWin = 0;
                        }
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)  // CS0168
            {
                LOG("uart rx error : " + ex.Message + '\n');
            }
            mtx.ReleaseMutex();
        }

        private void UartOpenWinsen(string comName)
        {
            try
            {
                if (null == sPort_Winsen)
                {
                    if (comName == null)
                    {
                        LOG("시리얼 포트를 선택해 주세요!!!");
                    }
                    else
                    {
                        sPort_Winsen = new SerialPort();
                        sPort_Winsen.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Winsen_DataReceived);
                        sPort_Winsen.PortName = comName;
                        sPort_Winsen.BaudRate = 9600;
                        sPort_Winsen.DataBits = (int)8;
                        sPort_Winsen.Parity = Parity.None;
                        sPort_Winsen.StopBits = StopBits.One;
                        sPort_Winsen.ReadTimeout = -1;
                        sPort_Winsen.WriteTimeout = -1;
                        sPort_Winsen.NewLine = "\n";
                        sPort_Winsen.Open();
                    }
                }

                if (sPort_Winsen.IsOpen)
                {
                    buttonWinsenComOpen.Enabled = false;
                    buttonWinsenComClose.Enabled = true;
                    LOG("시리얼 포트를 연결했습니다... : )\n");

                    comboBoxWinsen.SelectedText = comName;
                    CheckBoxUpdate(checkBoxWinsen, true);
                    //PrdTimerStart();
                }
                else
                {
                    if (comboBoxWinsen.SelectedItem != null)
                        LOG("시리얼 포트가 사용중입니다\n");

                    buttonWinsenComOpen.Enabled = false;
                    buttonWinsenComClose.Enabled = true;
                }
            }
            catch (System.Exception ex)
            {
                ERR(CallerName() + " : " + ex.Message);
                if (sPort_Winsen != null)
                    sPort_Winsen = null;
            }
        }

        private void UartCloseWinsen()
        {
            try
            {
                if (null != sPort_Winsen)
                {
                    if (sPort_Winsen.IsOpen)
                    {
                        sPort_Winsen.Close();
                        sPort_Winsen.Dispose();
                        sPort_Winsen = null;
                        buttonWinsenComClose.Enabled = false;
                        LOG("시리얼 포트를 닫았습니다\n");
                    }
                    else
                    {
                        LOG("시리얼 포트가 닫혀있습니다\n");
                    }
                    buttonWinsenComOpen.Enabled = true;
                    CheckBoxUpdate(checkBoxWinsen, false);
                }
            }
            catch (Exception ex)  // CS0168
            {
                ERR(CallerName() + " : " + ex.Message);
            }
        }

        private void update_winsen_ppm(string str)
        {
            if (textBoxWinPpm.InvokeRequired)
            {
                textBoxWinPpm.Invoke(new MethodInvoker(delegate ()
                {
                    textBoxWinPpm.Text = str;

                }));
            }
            else
            {
                textBoxWinPpm.Text = str;
            }
        }

        private void request_winsen()
        {
            byte[] msg = { 0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79 };

            if ((sPort_Winsen == null) || (sPort_Winsen.IsOpen == false))
            {
                LOG("Please select serial port\n");
                return;
            }
            LOG("Request Winsen\n");

            sPort_Winsen.Write(msg, 0, msg.Length);
        }
    }
}
