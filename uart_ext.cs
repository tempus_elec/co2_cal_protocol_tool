﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Ext = null;
		double sht_amb, sht_rh;

		void SerialPort_Ext_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int intRecSize = sPort_Ext.BytesToRead;
			string strRecData;
			string[] spData;

			try
			{
				mtx.WaitOne();
				if (intRecSize != 0)
				{
					byte[] buff = new byte[intRecSize];

					now = DateTime.Now;

					strRecData = sPort_Ext.ReadLine();
					spData = strRecData.Split(',');
					LOG(strRecData + "\n");
					if (spData[0] == "MSG")
					{
						if (spData[1] == "SHT3x")
						{
							if (spData.Length == 6)
							{
								if (textBoxAmb.InvokeRequired)
								{
									textBoxAmb.Invoke(new MethodInvoker(delegate ()
									{
										textBoxAmb.Text = spData[2] + " °C";
										textBoxRh.Text = spData[3] + " %";
										sht_amb = Convert.ToDouble(spData[2]);
										sht_rh = Convert.ToDouble(spData[3]);
									}));
								}
								else
								{
									textBoxAmb.Text = spData[2] + " °C";
									textBoxRh.Text = spData[3] + " %";
									sht_amb = Convert.ToDouble(spData[2]);
									sht_rh = Convert.ToDouble(spData[3]);
								}
							}
						}
					}
				}
				mtx.ReleaseMutex();
			}
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + ex.Message + "\n");
			}
		}
		
		private void UartOpenExt(string comName)
		{
			try
			{
				if (null == sPort_Ext)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Ext = new SerialPort();
						sPort_Ext.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Ext_DataReceived);
						sPort_Ext.PortName = comName;
						sPort_Ext.BaudRate = 115200;
						sPort_Ext.DataBits = (int)8;
						sPort_Ext.Parity = Parity.None;
						sPort_Ext.StopBits = StopBits.One;
						sPort_Ext.ReadTimeout = -1;
						sPort_Ext.WriteTimeout = -1;
						sPort_Ext.NewLine = "\n";
						sPort_Ext.Open();
					}
				}

				if (sPort_Ext.IsOpen)
				{
					buttonExtComOpen.Enabled = false;
					buttonExtComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");

					comboBoxExt.SelectedText = comName;

					//PrdTimerStart();
				}
				else
				{
					if (comboBoxExt.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonExtComOpen.Enabled = false;
					buttonExtComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				UartCloseExt();
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartCloseExt()
		{
			try
			{
				if (null != sPort_Ext)
				{
					if (sPort_Ext.IsOpen)
					{
						sPort_Ext.Close();
						sPort_Ext.Dispose();
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
					comboBoxExt = null;
					buttonExtComOpen.Enabled = true;
					buttonExtComClose.Enabled = false;
				}
			}
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void request_ext()
		{
			//bsp_get_dbgInfo(sPort_Ext);
		}
	}
}
