﻿namespace CO2_CAL_Protocol_Tool
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.buttonZmdiGet = new System.Windows.Forms.Button();
            this.buttonZmdiSet = new System.Windows.Forms.Button();
            this.textBoxZmdiData = new System.Windows.Forms.TextBox();
            this.textBoxZmdiAddr = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.radioButtonRefTpsAvg = new System.Windows.Forms.RadioButton();
            this.radioButtonRefFigaro = new System.Windows.Forms.RadioButton();
            this.radioButtonRefElt = new System.Windows.Forms.RadioButton();
            this.radioButtonRefWinsen = new System.Windows.Forms.RadioButton();
            this.radioButtonRefT360 = new System.Windows.Forms.RadioButton();
            this.radioButtonRefCubic = new System.Windows.Forms.RadioButton();
            this.radioButtonRefSenseAir = new System.Windows.Forms.RadioButton();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxEtcPro = new System.Windows.Forms.TextBox();
            this.radioButtonEtc = new System.Windows.Forms.RadioButton();
            this.radioButton5pro = new System.Windows.Forms.RadioButton();
            this.radioButton3pro = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxRefOffset = new System.Windows.Forms.TextBox();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textBoxAlarm4 = new System.Windows.Forms.TextBox();
            this.textBoxAlarm3 = new System.Windows.Forms.TextBox();
            this.textBoxAlarm2 = new System.Windows.Forms.TextBox();
            this.textBoxAlarm1 = new System.Windows.Forms.TextBox();
            this.buttonAlarmSet = new System.Windows.Forms.Button();
            this.buttonAlarmGet = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.textBoxSn4 = new System.Windows.Forms.TextBox();
            this.textBoxSn3 = new System.Windows.Forms.TextBox();
            this.textBoxSn2 = new System.Windows.Forms.TextBox();
            this.textBoxSn1 = new System.Windows.Forms.TextBox();
            this.buttonSnSet = new System.Windows.Forms.Button();
            this.buttonSn = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBoxVer4 = new System.Windows.Forms.TextBox();
            this.textBoxVer3 = new System.Windows.Forms.TextBox();
            this.textBoxVer2 = new System.Windows.Forms.TextBox();
            this.textBoxVer1 = new System.Windows.Forms.TextBox();
            this.buttonVer = new System.Windows.Forms.Button();
            this.buttonCmdDbgInfo = new System.Windows.Forms.Button();
            this.buttonCmdCalEn = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.checkBoxErr = new System.Windows.Forms.CheckBox();
            this.checkBoxInfo = new System.Windows.Forms.CheckBox();
            this.checkBoxLog = new System.Windows.Forms.CheckBox();
            this.buttonLogClear = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkBoxModule4 = new System.Windows.Forms.CheckBox();
            this.checkBoxModule3 = new System.Windows.Forms.CheckBox();
            this.checkBoxModule2 = new System.Windows.Forms.CheckBox();
            this.checkBoxRef = new System.Windows.Forms.CheckBox();
            this.checkBoxModule1 = new System.Windows.Forms.CheckBox();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.buttonCom = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.buttonFigaroComClose = new System.Windows.Forms.Button();
            this.comboBoxFigaro = new System.Windows.Forms.ComboBox();
            this.buttonFigaroComOpen = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.buttonEltComClose = new System.Windows.Forms.Button();
            this.comboBoxElt = new System.Windows.Forms.ComboBox();
            this.buttonEltComOpen = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.buttonCubicComClose = new System.Windows.Forms.Button();
            this.comboBoxCubic = new System.Windows.Forms.ComboBox();
            this.buttonCubicComOpen = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.buttonWinsenComClose = new System.Windows.Forms.Button();
            this.comboBoxWinsen = new System.Windows.Forms.ComboBox();
            this.buttonWinsenComOpen = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.buttonExtComClose = new System.Windows.Forms.Button();
            this.comboBoxExt = new System.Windows.Forms.ComboBox();
            this.buttonExtComOpen = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.buttonTempus4ComClose = new System.Windows.Forms.Button();
            this.comboBoxTps4 = new System.Windows.Forms.ComboBox();
            this.buttonTempus4ComOpen = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonTempus3ComClose = new System.Windows.Forms.Button();
            this.comboBoxTps3 = new System.Windows.Forms.ComboBox();
            this.buttonTempus3ComOpen = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonTempus2ComClose = new System.Windows.Forms.Button();
            this.comboBoxTps2 = new System.Windows.Forms.ComboBox();
            this.buttonTempus2ComOpen = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonTempus1ComClose = new System.Windows.Forms.Button();
            this.comboBoxTps1 = new System.Windows.Forms.ComboBox();
            this.buttonTempus1ComOpen = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonRefComClose = new System.Windows.Forms.Button();
            this.comboBoxRef = new System.Windows.Forms.ComboBox();
            this.buttonRefComOpen = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxDiff5 = new System.Windows.Forms.TextBox();
            this.textBoxDiff4 = new System.Windows.Forms.TextBox();
            this.textBoxDiff3 = new System.Windows.Forms.TextBox();
            this.textBoxDiff2 = new System.Windows.Forms.TextBox();
            this.textBoxDiff1 = new System.Windows.Forms.TextBox();
            this.checkBoxFigaro = new System.Windows.Forms.CheckBox();
            this.checkBoxElt = new System.Windows.Forms.CheckBox();
            this.checkBoxCubic = new System.Windows.Forms.CheckBox();
            this.checkBoxWinsen = new System.Windows.Forms.CheckBox();
            this.checkBoxSenseAir = new System.Windows.Forms.CheckBox();
            this.checkBoxChart4 = new System.Windows.Forms.CheckBox();
            this.checkBoxChart3 = new System.Windows.Forms.CheckBox();
            this.checkBoxChart2 = new System.Windows.Forms.CheckBox();
            this.checkBoxChart1 = new System.Windows.Forms.CheckBox();
            this.chartCO2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.checkBoxDcDr4 = new System.Windows.Forms.CheckBox();
            this.checkBoxDcDr3 = new System.Windows.Forms.CheckBox();
            this.checkBoxDcDr2 = new System.Windows.Forms.CheckBox();
            this.checkBoxDcDr1 = new System.Windows.Forms.CheckBox();
            this.buttonDcDrClear = new System.Windows.Forms.Button();
            this.chartDcDr = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.checkBoxCurv4 = new System.Windows.Forms.CheckBox();
            this.checkBoxCurv3 = new System.Windows.Forms.CheckBox();
            this.checkBoxCurv2 = new System.Windows.Forms.CheckBox();
            this.checkBoxCurv1 = new System.Windows.Forms.CheckBox();
            this.buttonCurvClear = new System.Windows.Forms.Button();
            this.chartCurv = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.buttonCurv = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chartDispCurv = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBoxDetectorId = new System.Windows.Forms.GroupBox();
            this.buttonDetectorId = new System.Windows.Forms.Button();
            this.textBoxDetectorId4 = new System.Windows.Forms.TextBox();
            this.textBoxDetectorId3 = new System.Windows.Forms.TextBox();
            this.textBoxDetectorId2 = new System.Windows.Forms.TextBox();
            this.textBoxDetectorId1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps4RefCmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps4RefNorMode = new System.Windows.Forms.RadioButton();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps4Co2CmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps4Co2NorMode = new System.Windows.Forms.RadioButton();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps3RefCmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps3RefNorMode = new System.Windows.Forms.RadioButton();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps3Co2CmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps3Co2NorMode = new System.Windows.Forms.RadioButton();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps2RefCmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps2RefNorMode = new System.Windows.Forms.RadioButton();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps2Co2CmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps2Co2NorMode = new System.Windows.Forms.RadioButton();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps1RefCmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps1RefNorMode = new System.Windows.Forms.RadioButton();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.radioButtonTps1Co2CmdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTps1Co2NorMode = new System.Windows.Forms.RadioButton();
            this.buttonPcParamGet = new System.Windows.Forms.Button();
            this.buttonPcParamSet = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxPcParam = new System.Windows.Forms.TextBox();
            this.buttonCapture = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonTcParamGet = new System.Windows.Forms.Button();
            this.buttonTcParamSet = new System.Windows.Forms.Button();
            this.buttonGain = new System.Windows.Forms.Button();
            this.buttonTs = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.buttonSr = new System.Windows.Forms.Button();
            this.buttonSc = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTcParam = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxGain = new System.Windows.Forms.TextBox();
            this.textBoxTs = new System.Windows.Forms.TextBox();
            this.textBoxSr = new System.Windows.Forms.TextBox();
            this.buttonSync = new System.Windows.Forms.Button();
            this.textBoxSc = new System.Windows.Forms.TextBox();
            this.radioButtonTempus4 = new System.Windows.Forms.RadioButton();
            this.radioButtonTempus2 = new System.Windows.Forms.RadioButton();
            this.radioButtonTempus3 = new System.Windows.Forms.RadioButton();
            this.radioButtonTempus1 = new System.Windows.Forms.RadioButton();
            this.textBoxAmb = new System.Windows.Forms.TextBox();
            this.textBoxRh = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBoxRef = new System.Windows.Forms.TextBox();
            this.serialPort2 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort3 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort4 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort5 = new System.IO.Ports.SerialPort(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxWinPpm = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxCubic = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxElt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxFigaro = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.textBoxDcOffset = new System.Windows.Forms.TextBox();
            this.textBoxDrOffset = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCO2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartDcDr)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCurv)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartDispCurv)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBoxDetectorId.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(10, 8);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(964, 132);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(10, 178);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1173, 488);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox18);
            this.tabPage1.Controls.Add(this.groupBox17);
            this.tabPage1.Controls.Add(this.groupBox16);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.textBoxRefOffset);
            this.tabPage1.Controls.Add(this.buttonStop);
            this.tabPage1.Controls.Add(this.buttonDefault);
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.buttonCmdDbgInfo);
            this.tabPage1.Controls.Add(this.buttonCmdCalEn);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.buttonLogClear);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.richTextBoxLog);
            this.tabPage1.Controls.Add(this.buttonCom);
            this.tabPage1.Controls.Add(this.groupBox15);
            this.tabPage1.Controls.Add(this.groupBox14);
            this.tabPage1.Controls.Add(this.groupBox13);
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1165, 462);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Config";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label19);
            this.groupBox18.Controls.Add(this.label20);
            this.groupBox18.Controls.Add(this.label18);
            this.groupBox18.Controls.Add(this.buttonZmdiGet);
            this.groupBox18.Controls.Add(this.buttonZmdiSet);
            this.groupBox18.Controls.Add(this.textBoxZmdiData);
            this.groupBox18.Controls.Add(this.textBoxZmdiAddr);
            this.groupBox18.Location = new System.Drawing.Point(495, 10);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(156, 118);
            this.groupBox18.TabIndex = 19;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Register";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(94, 66);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "data";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(130, 12);
            this.label20.TabIndex = 2;
            this.label20.Text = "Hex value without \"0x\"";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 12);
            this.label18.TabIndex = 2;
            this.label18.Text = "addr";
            // 
            // buttonZmdiGet
            // 
            this.buttonZmdiGet.Location = new System.Drawing.Point(77, 83);
            this.buttonZmdiGet.Name = "buttonZmdiGet";
            this.buttonZmdiGet.Size = new System.Drawing.Size(64, 23);
            this.buttonZmdiGet.TabIndex = 1;
            this.buttonZmdiGet.Text = "Get";
            this.buttonZmdiGet.UseVisualStyleBackColor = true;
            this.buttonZmdiGet.Click += new System.EventHandler(this.buttonZmdiGet_Click);
            // 
            // buttonZmdiSet
            // 
            this.buttonZmdiSet.Location = new System.Drawing.Point(7, 83);
            this.buttonZmdiSet.Name = "buttonZmdiSet";
            this.buttonZmdiSet.Size = new System.Drawing.Size(64, 23);
            this.buttonZmdiSet.TabIndex = 1;
            this.buttonZmdiSet.Text = "Set";
            this.buttonZmdiSet.UseVisualStyleBackColor = true;
            this.buttonZmdiSet.Click += new System.EventHandler(this.buttonZmdiSet_Click);
            // 
            // textBoxZmdiData
            // 
            this.textBoxZmdiData.Location = new System.Drawing.Point(77, 42);
            this.textBoxZmdiData.Name = "textBoxZmdiData";
            this.textBoxZmdiData.Size = new System.Drawing.Size(64, 21);
            this.textBoxZmdiData.TabIndex = 0;
            this.textBoxZmdiData.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxZmdiAddr
            // 
            this.textBoxZmdiAddr.Location = new System.Drawing.Point(7, 42);
            this.textBoxZmdiAddr.Name = "textBoxZmdiAddr";
            this.textBoxZmdiAddr.Size = new System.Drawing.Size(64, 21);
            this.textBoxZmdiAddr.TabIndex = 0;
            this.textBoxZmdiAddr.Text = "29";
            this.textBoxZmdiAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.radioButtonRefTpsAvg);
            this.groupBox17.Controls.Add(this.radioButtonRefFigaro);
            this.groupBox17.Controls.Add(this.radioButtonRefElt);
            this.groupBox17.Controls.Add(this.radioButtonRefWinsen);
            this.groupBox17.Controls.Add(this.radioButtonRefT360);
            this.groupBox17.Controls.Add(this.radioButtonRefCubic);
            this.groupBox17.Controls.Add(this.radioButtonRefSenseAir);
            this.groupBox17.Location = new System.Drawing.Point(692, 10);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(93, 181);
            this.groupBox17.TabIndex = 18;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "REF Sel";
            // 
            // radioButtonRefTpsAvg
            // 
            this.radioButtonRefTpsAvg.AutoSize = true;
            this.radioButtonRefTpsAvg.Location = new System.Drawing.Point(7, 155);
            this.radioButtonRefTpsAvg.Name = "radioButtonRefTpsAvg";
            this.radioButtonRefTpsAvg.Size = new System.Drawing.Size(76, 16);
            this.radioButtonRefTpsAvg.TabIndex = 0;
            this.radioButtonRefTpsAvg.Text = "TPS AVG";
            this.radioButtonRefTpsAvg.UseVisualStyleBackColor = true;
            // 
            // radioButtonRefFigaro
            // 
            this.radioButtonRefFigaro.AutoSize = true;
            this.radioButtonRefFigaro.Location = new System.Drawing.Point(7, 133);
            this.radioButtonRefFigaro.Name = "radioButtonRefFigaro";
            this.radioButtonRefFigaro.Size = new System.Drawing.Size(58, 16);
            this.radioButtonRefFigaro.TabIndex = 0;
            this.radioButtonRefFigaro.Text = "Figaro";
            this.radioButtonRefFigaro.UseVisualStyleBackColor = true;
            // 
            // radioButtonRefElt
            // 
            this.radioButtonRefElt.AutoSize = true;
            this.radioButtonRefElt.Location = new System.Drawing.Point(7, 111);
            this.radioButtonRefElt.Name = "radioButtonRefElt";
            this.radioButtonRefElt.Size = new System.Drawing.Size(37, 16);
            this.radioButtonRefElt.TabIndex = 0;
            this.radioButtonRefElt.Text = "Elt";
            this.radioButtonRefElt.UseVisualStyleBackColor = true;
            // 
            // radioButtonRefWinsen
            // 
            this.radioButtonRefWinsen.AutoSize = true;
            this.radioButtonRefWinsen.Location = new System.Drawing.Point(7, 89);
            this.radioButtonRefWinsen.Name = "radioButtonRefWinsen";
            this.radioButtonRefWinsen.Size = new System.Drawing.Size(64, 16);
            this.radioButtonRefWinsen.TabIndex = 0;
            this.radioButtonRefWinsen.Text = "Winsen";
            this.radioButtonRefWinsen.UseVisualStyleBackColor = true;
            // 
            // radioButtonRefT360
            // 
            this.radioButtonRefT360.AutoSize = true;
            this.radioButtonRefT360.Location = new System.Drawing.Point(7, 67);
            this.radioButtonRefT360.Name = "radioButtonRefT360";
            this.radioButtonRefT360.Size = new System.Drawing.Size(49, 16);
            this.radioButtonRefT360.TabIndex = 0;
            this.radioButtonRefT360.Text = "T360";
            this.radioButtonRefT360.UseVisualStyleBackColor = true;
            // 
            // radioButtonRefCubic
            // 
            this.radioButtonRefCubic.AutoSize = true;
            this.radioButtonRefCubic.Location = new System.Drawing.Point(7, 43);
            this.radioButtonRefCubic.Name = "radioButtonRefCubic";
            this.radioButtonRefCubic.Size = new System.Drawing.Size(56, 16);
            this.radioButtonRefCubic.TabIndex = 0;
            this.radioButtonRefCubic.Text = "Cubic";
            this.radioButtonRefCubic.UseVisualStyleBackColor = true;
            // 
            // radioButtonRefSenseAir
            // 
            this.radioButtonRefSenseAir.AutoSize = true;
            this.radioButtonRefSenseAir.Checked = true;
            this.radioButtonRefSenseAir.Location = new System.Drawing.Point(7, 21);
            this.radioButtonRefSenseAir.Name = "radioButtonRefSenseAir";
            this.radioButtonRefSenseAir.Size = new System.Drawing.Size(74, 16);
            this.radioButtonRefSenseAir.TabIndex = 0;
            this.radioButtonRefSenseAir.TabStop = true;
            this.radioButtonRefSenseAir.Text = "SenseAir";
            this.radioButtonRefSenseAir.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label17);
            this.groupBox16.Controls.Add(this.textBoxEtcPro);
            this.groupBox16.Controls.Add(this.radioButtonEtc);
            this.groupBox16.Controls.Add(this.radioButton5pro);
            this.groupBox16.Controls.Add(this.radioButton3pro);
            this.groupBox16.Location = new System.Drawing.Point(238, 196);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(241, 43);
            this.groupBox16.TabIndex = 17;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Accuracy Range";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(202, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 12);
            this.label17.TabIndex = 3;
            this.label17.Text = "%";
            // 
            // textBoxEtcPro
            // 
            this.textBoxEtcPro.Location = new System.Drawing.Point(159, 15);
            this.textBoxEtcPro.Name = "textBoxEtcPro";
            this.textBoxEtcPro.Size = new System.Drawing.Size(37, 21);
            this.textBoxEtcPro.TabIndex = 2;
            this.textBoxEtcPro.Text = "10";
            this.textBoxEtcPro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radioButtonEtc
            // 
            this.radioButtonEtc.AutoSize = true;
            this.radioButtonEtc.Location = new System.Drawing.Point(112, 19);
            this.radioButtonEtc.Name = "radioButtonEtc";
            this.radioButtonEtc.Size = new System.Drawing.Size(41, 16);
            this.radioButtonEtc.TabIndex = 0;
            this.radioButtonEtc.TabStop = true;
            this.radioButtonEtc.Text = "Etc";
            this.radioButtonEtc.UseVisualStyleBackColor = true;
            // 
            // radioButton5pro
            // 
            this.radioButton5pro.AutoSize = true;
            this.radioButton5pro.Location = new System.Drawing.Point(65, 19);
            this.radioButton5pro.Name = "radioButton5pro";
            this.radioButton5pro.Size = new System.Drawing.Size(39, 16);
            this.radioButton5pro.TabIndex = 1;
            this.radioButton5pro.Text = "5%";
            this.radioButton5pro.UseVisualStyleBackColor = true;
            // 
            // radioButton3pro
            // 
            this.radioButton3pro.AutoSize = true;
            this.radioButton3pro.Checked = true;
            this.radioButton3pro.Location = new System.Drawing.Point(13, 19);
            this.radioButton3pro.Name = "radioButton3pro";
            this.radioButton3pro.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton3pro.Size = new System.Drawing.Size(39, 16);
            this.radioButton3pro.TabIndex = 0;
            this.radioButton3pro.TabStop = true;
            this.radioButton3pro.Text = "3%";
            this.radioButton3pro.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(237, 130);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 12);
            this.label13.TabIndex = 16;
            this.label13.Text = "SENSEAIR OFFSET";
            // 
            // textBoxRefOffset
            // 
            this.textBoxRefOffset.Location = new System.Drawing.Point(245, 144);
            this.textBoxRefOffset.Name = "textBoxRefOffset";
            this.textBoxRefOffset.Size = new System.Drawing.Size(100, 21);
            this.textBoxRefOffset.TabIndex = 15;
            this.textBoxRefOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(238, 423);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 14;
            this.buttonStop.Text = "Mon Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonDefault
            // 
            this.buttonDefault.Location = new System.Drawing.Point(135, 6);
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.Size = new System.Drawing.Size(75, 23);
            this.buttonDefault.TabIndex = 13;
            this.buttonDefault.Text = "Default";
            this.buttonDefault.UseVisualStyleBackColor = true;
            this.buttonDefault.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBoxAlarm4);
            this.groupBox11.Controls.Add(this.textBoxAlarm3);
            this.groupBox11.Controls.Add(this.textBoxAlarm2);
            this.groupBox11.Controls.Add(this.textBoxAlarm1);
            this.groupBox11.Controls.Add(this.buttonAlarmSet);
            this.groupBox11.Controls.Add(this.buttonAlarmGet);
            this.groupBox11.Location = new System.Drawing.Point(495, 255);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(125, 162);
            this.groupBox11.TabIndex = 12;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Alarm";
            // 
            // textBoxAlarm4
            // 
            this.textBoxAlarm4.Location = new System.Drawing.Point(7, 102);
            this.textBoxAlarm4.Name = "textBoxAlarm4";
            this.textBoxAlarm4.Size = new System.Drawing.Size(100, 21);
            this.textBoxAlarm4.TabIndex = 1;
            this.textBoxAlarm4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxAlarm3
            // 
            this.textBoxAlarm3.Location = new System.Drawing.Point(7, 75);
            this.textBoxAlarm3.Name = "textBoxAlarm3";
            this.textBoxAlarm3.Size = new System.Drawing.Size(100, 21);
            this.textBoxAlarm3.TabIndex = 1;
            this.textBoxAlarm3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxAlarm2
            // 
            this.textBoxAlarm2.Location = new System.Drawing.Point(7, 48);
            this.textBoxAlarm2.Name = "textBoxAlarm2";
            this.textBoxAlarm2.Size = new System.Drawing.Size(100, 21);
            this.textBoxAlarm2.TabIndex = 1;
            this.textBoxAlarm2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxAlarm1
            // 
            this.textBoxAlarm1.Location = new System.Drawing.Point(7, 21);
            this.textBoxAlarm1.Name = "textBoxAlarm1";
            this.textBoxAlarm1.Size = new System.Drawing.Size(100, 21);
            this.textBoxAlarm1.TabIndex = 1;
            this.textBoxAlarm1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonAlarmSet
            // 
            this.buttonAlarmSet.Location = new System.Drawing.Point(57, 128);
            this.buttonAlarmSet.Name = "buttonAlarmSet";
            this.buttonAlarmSet.Size = new System.Drawing.Size(50, 23);
            this.buttonAlarmSet.TabIndex = 0;
            this.buttonAlarmSet.Text = "Set";
            this.buttonAlarmSet.UseVisualStyleBackColor = true;
            this.buttonAlarmSet.Click += new System.EventHandler(this.buttonAlarmSet_Click);
            // 
            // buttonAlarmGet
            // 
            this.buttonAlarmGet.Location = new System.Drawing.Point(7, 128);
            this.buttonAlarmGet.Name = "buttonAlarmGet";
            this.buttonAlarmGet.Size = new System.Drawing.Size(50, 23);
            this.buttonAlarmGet.TabIndex = 0;
            this.buttonAlarmGet.Text = "Get";
            this.buttonAlarmGet.UseVisualStyleBackColor = true;
            this.buttonAlarmGet.Click += new System.EventHandler(this.buttonAlarmGet_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.textBoxSn4);
            this.groupBox10.Controls.Add(this.textBoxSn3);
            this.groupBox10.Controls.Add(this.textBoxSn2);
            this.groupBox10.Controls.Add(this.textBoxSn1);
            this.groupBox10.Controls.Add(this.buttonSnSet);
            this.groupBox10.Controls.Add(this.buttonSn);
            this.groupBox10.Location = new System.Drawing.Point(363, 255);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(116, 162);
            this.groupBox10.TabIndex = 12;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "SN";
            // 
            // textBoxSn4
            // 
            this.textBoxSn4.Location = new System.Drawing.Point(7, 102);
            this.textBoxSn4.Name = "textBoxSn4";
            this.textBoxSn4.Size = new System.Drawing.Size(100, 21);
            this.textBoxSn4.TabIndex = 1;
            this.textBoxSn4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSn3
            // 
            this.textBoxSn3.Location = new System.Drawing.Point(7, 75);
            this.textBoxSn3.Name = "textBoxSn3";
            this.textBoxSn3.Size = new System.Drawing.Size(100, 21);
            this.textBoxSn3.TabIndex = 1;
            this.textBoxSn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSn2
            // 
            this.textBoxSn2.Location = new System.Drawing.Point(7, 48);
            this.textBoxSn2.Name = "textBoxSn2";
            this.textBoxSn2.Size = new System.Drawing.Size(100, 21);
            this.textBoxSn2.TabIndex = 1;
            this.textBoxSn2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSn1
            // 
            this.textBoxSn1.Location = new System.Drawing.Point(7, 21);
            this.textBoxSn1.Name = "textBoxSn1";
            this.textBoxSn1.Size = new System.Drawing.Size(100, 21);
            this.textBoxSn1.TabIndex = 1;
            this.textBoxSn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSnSet
            // 
            this.buttonSnSet.Location = new System.Drawing.Point(57, 128);
            this.buttonSnSet.Name = "buttonSnSet";
            this.buttonSnSet.Size = new System.Drawing.Size(50, 23);
            this.buttonSnSet.TabIndex = 0;
            this.buttonSnSet.Text = "Set";
            this.buttonSnSet.UseVisualStyleBackColor = true;
            this.buttonSnSet.Click += new System.EventHandler(this.buttonSnSet_Click);
            // 
            // buttonSn
            // 
            this.buttonSn.Location = new System.Drawing.Point(7, 128);
            this.buttonSn.Name = "buttonSn";
            this.buttonSn.Size = new System.Drawing.Size(50, 23);
            this.buttonSn.TabIndex = 0;
            this.buttonSn.Text = "Get";
            this.buttonSn.UseVisualStyleBackColor = true;
            this.buttonSn.Click += new System.EventHandler(this.buttonSn_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBoxVer4);
            this.groupBox9.Controls.Add(this.textBoxVer3);
            this.groupBox9.Controls.Add(this.textBoxVer2);
            this.groupBox9.Controls.Add(this.textBoxVer1);
            this.groupBox9.Controls.Add(this.buttonVer);
            this.groupBox9.Location = new System.Drawing.Point(238, 255);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(113, 162);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "VER";
            // 
            // textBoxVer4
            // 
            this.textBoxVer4.Location = new System.Drawing.Point(7, 102);
            this.textBoxVer4.Name = "textBoxVer4";
            this.textBoxVer4.Size = new System.Drawing.Size(100, 21);
            this.textBoxVer4.TabIndex = 1;
            this.textBoxVer4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxVer3
            // 
            this.textBoxVer3.Location = new System.Drawing.Point(7, 75);
            this.textBoxVer3.Name = "textBoxVer3";
            this.textBoxVer3.Size = new System.Drawing.Size(100, 21);
            this.textBoxVer3.TabIndex = 1;
            this.textBoxVer3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxVer2
            // 
            this.textBoxVer2.Location = new System.Drawing.Point(7, 48);
            this.textBoxVer2.Name = "textBoxVer2";
            this.textBoxVer2.Size = new System.Drawing.Size(100, 21);
            this.textBoxVer2.TabIndex = 1;
            this.textBoxVer2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxVer1
            // 
            this.textBoxVer1.Location = new System.Drawing.Point(7, 21);
            this.textBoxVer1.Name = "textBoxVer1";
            this.textBoxVer1.Size = new System.Drawing.Size(100, 21);
            this.textBoxVer1.TabIndex = 1;
            this.textBoxVer1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonVer
            // 
            this.buttonVer.Location = new System.Drawing.Point(7, 128);
            this.buttonVer.Name = "buttonVer";
            this.buttonVer.Size = new System.Drawing.Size(75, 23);
            this.buttonVer.TabIndex = 0;
            this.buttonVer.Text = "Version";
            this.buttonVer.UseVisualStyleBackColor = true;
            this.buttonVer.Click += new System.EventHandler(this.buttonVer_Click);
            // 
            // buttonCmdDbgInfo
            // 
            this.buttonCmdDbgInfo.Location = new System.Drawing.Point(362, 423);
            this.buttonCmdDbgInfo.Name = "buttonCmdDbgInfo";
            this.buttonCmdDbgInfo.Size = new System.Drawing.Size(75, 23);
            this.buttonCmdDbgInfo.TabIndex = 10;
            this.buttonCmdDbgInfo.Text = "DbgInfo";
            this.buttonCmdDbgInfo.UseVisualStyleBackColor = true;
            this.buttonCmdDbgInfo.Click += new System.EventHandler(this.buttonCmdDbgInfo_Click);
            // 
            // buttonCmdCalEn
            // 
            this.buttonCmdCalEn.Location = new System.Drawing.Point(495, 423);
            this.buttonCmdCalEn.Name = "buttonCmdCalEn";
            this.buttonCmdCalEn.Size = new System.Drawing.Size(75, 23);
            this.buttonCmdCalEn.TabIndex = 9;
            this.buttonCmdCalEn.Text = "CAL En";
            this.buttonCmdCalEn.UseVisualStyleBackColor = true;
            this.buttonCmdCalEn.Click += new System.EventHandler(this.buttonCmdCalEn_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.checkBoxErr);
            this.groupBox7.Controls.Add(this.checkBoxInfo);
            this.groupBox7.Controls.Add(this.checkBoxLog);
            this.groupBox7.Location = new System.Drawing.Point(245, 18);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(100, 93);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "LOG Flag";
            // 
            // checkBoxErr
            // 
            this.checkBoxErr.AutoSize = true;
            this.checkBoxErr.Checked = true;
            this.checkBoxErr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxErr.Location = new System.Drawing.Point(7, 65);
            this.checkBoxErr.Name = "checkBoxErr";
            this.checkBoxErr.Size = new System.Drawing.Size(48, 16);
            this.checkBoxErr.TabIndex = 0;
            this.checkBoxErr.Text = "ERR";
            this.checkBoxErr.UseVisualStyleBackColor = true;
            // 
            // checkBoxInfo
            // 
            this.checkBoxInfo.AutoSize = true;
            this.checkBoxInfo.Checked = true;
            this.checkBoxInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxInfo.Location = new System.Drawing.Point(7, 43);
            this.checkBoxInfo.Name = "checkBoxInfo";
            this.checkBoxInfo.Size = new System.Drawing.Size(52, 16);
            this.checkBoxInfo.TabIndex = 0;
            this.checkBoxInfo.Text = "INFO";
            this.checkBoxInfo.UseVisualStyleBackColor = true;
            // 
            // checkBoxLog
            // 
            this.checkBoxLog.AutoSize = true;
            this.checkBoxLog.Checked = true;
            this.checkBoxLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLog.Location = new System.Drawing.Point(7, 21);
            this.checkBoxLog.Name = "checkBoxLog";
            this.checkBoxLog.Size = new System.Drawing.Size(49, 16);
            this.checkBoxLog.TabIndex = 0;
            this.checkBoxLog.Text = "LOG";
            this.checkBoxLog.UseVisualStyleBackColor = true;
            // 
            // buttonLogClear
            // 
            this.buttonLogClear.Location = new System.Drawing.Point(1105, 432);
            this.buttonLogClear.Name = "buttonLogClear";
            this.buttonLogClear.Size = new System.Drawing.Size(54, 23);
            this.buttonLogClear.TabIndex = 7;
            this.buttonLogClear.Text = "Clear";
            this.buttonLogClear.UseVisualStyleBackColor = true;
            this.buttonLogClear.Click += new System.EventHandler(this.buttonLogClear_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.checkBoxModule4);
            this.groupBox6.Controls.Add(this.checkBoxModule3);
            this.groupBox6.Controls.Add(this.checkBoxModule2);
            this.groupBox6.Controls.Add(this.checkBoxRef);
            this.groupBox6.Controls.Add(this.checkBoxModule1);
            this.groupBox6.Location = new System.Drawing.Point(363, 18);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(116, 153);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tempus Module";
            // 
            // checkBoxModule4
            // 
            this.checkBoxModule4.AutoSize = true;
            this.checkBoxModule4.ForeColor = System.Drawing.Color.Magenta;
            this.checkBoxModule4.Location = new System.Drawing.Point(6, 111);
            this.checkBoxModule4.Name = "checkBoxModule4";
            this.checkBoxModule4.Size = new System.Drawing.Size(76, 16);
            this.checkBoxModule4.TabIndex = 0;
            this.checkBoxModule4.Text = "Module 4";
            this.checkBoxModule4.UseVisualStyleBackColor = true;
            // 
            // checkBoxModule3
            // 
            this.checkBoxModule3.AutoSize = true;
            this.checkBoxModule3.Location = new System.Drawing.Point(6, 89);
            this.checkBoxModule3.Name = "checkBoxModule3";
            this.checkBoxModule3.Size = new System.Drawing.Size(76, 16);
            this.checkBoxModule3.TabIndex = 0;
            this.checkBoxModule3.Text = "Module 3";
            this.checkBoxModule3.UseVisualStyleBackColor = true;
            // 
            // checkBoxModule2
            // 
            this.checkBoxModule2.AutoSize = true;
            this.checkBoxModule2.ForeColor = System.Drawing.Color.Blue;
            this.checkBoxModule2.Location = new System.Drawing.Point(6, 67);
            this.checkBoxModule2.Name = "checkBoxModule2";
            this.checkBoxModule2.Size = new System.Drawing.Size(76, 16);
            this.checkBoxModule2.TabIndex = 0;
            this.checkBoxModule2.Text = "Module 2";
            this.checkBoxModule2.UseVisualStyleBackColor = true;
            // 
            // checkBoxRef
            // 
            this.checkBoxRef.AutoSize = true;
            this.checkBoxRef.ForeColor = System.Drawing.Color.Green;
            this.checkBoxRef.Location = new System.Drawing.Point(6, 23);
            this.checkBoxRef.Name = "checkBoxRef";
            this.checkBoxRef.Size = new System.Drawing.Size(47, 16);
            this.checkBoxRef.TabIndex = 0;
            this.checkBoxRef.Text = "REF";
            this.checkBoxRef.UseVisualStyleBackColor = true;
            // 
            // checkBoxModule1
            // 
            this.checkBoxModule1.AutoSize = true;
            this.checkBoxModule1.ForeColor = System.Drawing.Color.Red;
            this.checkBoxModule1.Location = new System.Drawing.Point(7, 45);
            this.checkBoxModule1.Name = "checkBoxModule1";
            this.checkBoxModule1.Size = new System.Drawing.Size(76, 16);
            this.checkBoxModule1.TabIndex = 0;
            this.checkBoxModule1.Text = "Module 1";
            this.checkBoxModule1.UseVisualStyleBackColor = true;
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Location = new System.Drawing.Point(791, 10);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(368, 445);
            this.richTextBoxLog.TabIndex = 5;
            this.richTextBoxLog.Text = "";
            // 
            // buttonCom
            // 
            this.buttonCom.Location = new System.Drawing.Point(11, 6);
            this.buttonCom.Name = "buttonCom";
            this.buttonCom.Size = new System.Drawing.Size(78, 23);
            this.buttonCom.TabIndex = 4;
            this.buttonCom.Text = "COM Port";
            this.buttonCom.UseVisualStyleBackColor = true;
            this.buttonCom.Click += new System.EventHandler(this.buttonCom_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.buttonFigaroComClose);
            this.groupBox15.Controls.Add(this.comboBoxFigaro);
            this.groupBox15.Controls.Add(this.buttonFigaroComOpen);
            this.groupBox15.ForeColor = System.Drawing.Color.Indigo;
            this.groupBox15.Location = new System.Drawing.Point(6, 422);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(204, 40);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Figaro";
            // 
            // buttonFigaroComClose
            // 
            this.buttonFigaroComClose.ForeColor = System.Drawing.Color.Indigo;
            this.buttonFigaroComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonFigaroComClose.Name = "buttonFigaroComClose";
            this.buttonFigaroComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonFigaroComClose.TabIndex = 0;
            this.buttonFigaroComClose.Text = "Close";
            this.buttonFigaroComClose.UseVisualStyleBackColor = true;
            this.buttonFigaroComClose.Click += new System.EventHandler(this.buttonFigaroClose_Click);
            // 
            // comboBoxFigaro
            // 
            this.comboBoxFigaro.ForeColor = System.Drawing.Color.Indigo;
            this.comboBoxFigaro.FormattingEnabled = true;
            this.comboBoxFigaro.Location = new System.Drawing.Point(5, 13);
            this.comboBoxFigaro.Name = "comboBoxFigaro";
            this.comboBoxFigaro.Size = new System.Drawing.Size(78, 20);
            this.comboBoxFigaro.TabIndex = 1;
            // 
            // buttonFigaroComOpen
            // 
            this.buttonFigaroComOpen.ForeColor = System.Drawing.Color.Indigo;
            this.buttonFigaroComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonFigaroComOpen.Name = "buttonFigaroComOpen";
            this.buttonFigaroComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonFigaroComOpen.TabIndex = 0;
            this.buttonFigaroComOpen.Text = "Open";
            this.buttonFigaroComOpen.UseVisualStyleBackColor = true;
            this.buttonFigaroComOpen.Click += new System.EventHandler(this.buttonFigaroComOpen_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.buttonEltComClose);
            this.groupBox14.Controls.Add(this.comboBoxElt);
            this.groupBox14.Controls.Add(this.buttonEltComOpen);
            this.groupBox14.ForeColor = System.Drawing.Color.LightSlateGray;
            this.groupBox14.Location = new System.Drawing.Point(6, 382);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(204, 40);
            this.groupBox14.TabIndex = 3;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Elt";
            // 
            // buttonEltComClose
            // 
            this.buttonEltComClose.ForeColor = System.Drawing.Color.LightSlateGray;
            this.buttonEltComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonEltComClose.Name = "buttonEltComClose";
            this.buttonEltComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonEltComClose.TabIndex = 0;
            this.buttonEltComClose.Text = "Close";
            this.buttonEltComClose.UseVisualStyleBackColor = true;
            this.buttonEltComClose.Click += new System.EventHandler(this.buttonEltComClose_Click);
            // 
            // comboBoxElt
            // 
            this.comboBoxElt.ForeColor = System.Drawing.Color.LightSlateGray;
            this.comboBoxElt.FormattingEnabled = true;
            this.comboBoxElt.Location = new System.Drawing.Point(5, 13);
            this.comboBoxElt.Name = "comboBoxElt";
            this.comboBoxElt.Size = new System.Drawing.Size(78, 20);
            this.comboBoxElt.TabIndex = 1;
            // 
            // buttonEltComOpen
            // 
            this.buttonEltComOpen.ForeColor = System.Drawing.Color.LightSlateGray;
            this.buttonEltComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonEltComOpen.Name = "buttonEltComOpen";
            this.buttonEltComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonEltComOpen.TabIndex = 0;
            this.buttonEltComOpen.Text = "Open";
            this.buttonEltComOpen.UseVisualStyleBackColor = true;
            this.buttonEltComOpen.Click += new System.EventHandler(this.buttonEltComOpen_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.buttonCubicComClose);
            this.groupBox13.Controls.Add(this.comboBoxCubic);
            this.groupBox13.Controls.Add(this.buttonCubicComOpen);
            this.groupBox13.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox13.Location = new System.Drawing.Point(6, 338);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(204, 40);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Cubic";
            // 
            // buttonCubicComClose
            // 
            this.buttonCubicComClose.ForeColor = System.Drawing.Color.Maroon;
            this.buttonCubicComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonCubicComClose.Name = "buttonCubicComClose";
            this.buttonCubicComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonCubicComClose.TabIndex = 0;
            this.buttonCubicComClose.Text = "Close";
            this.buttonCubicComClose.UseVisualStyleBackColor = true;
            this.buttonCubicComClose.Click += new System.EventHandler(this.buttonCubicClose_Click);
            // 
            // comboBoxCubic
            // 
            this.comboBoxCubic.ForeColor = System.Drawing.Color.Maroon;
            this.comboBoxCubic.FormattingEnabled = true;
            this.comboBoxCubic.Location = new System.Drawing.Point(5, 13);
            this.comboBoxCubic.Name = "comboBoxCubic";
            this.comboBoxCubic.Size = new System.Drawing.Size(78, 20);
            this.comboBoxCubic.TabIndex = 1;
            // 
            // buttonCubicComOpen
            // 
            this.buttonCubicComOpen.ForeColor = System.Drawing.Color.Maroon;
            this.buttonCubicComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonCubicComOpen.Name = "buttonCubicComOpen";
            this.buttonCubicComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonCubicComOpen.TabIndex = 0;
            this.buttonCubicComOpen.Text = "Open";
            this.buttonCubicComOpen.UseVisualStyleBackColor = true;
            this.buttonCubicComOpen.Click += new System.EventHandler(this.buttonCubicOpen_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.buttonWinsenComClose);
            this.groupBox12.Controls.Add(this.comboBoxWinsen);
            this.groupBox12.Controls.Add(this.buttonWinsenComOpen);
            this.groupBox12.ForeColor = System.Drawing.Color.LightCoral;
            this.groupBox12.Location = new System.Drawing.Point(6, 294);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(204, 40);
            this.groupBox12.TabIndex = 3;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Winsen";
            // 
            // buttonWinsenComClose
            // 
            this.buttonWinsenComClose.ForeColor = System.Drawing.Color.LightCoral;
            this.buttonWinsenComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonWinsenComClose.Name = "buttonWinsenComClose";
            this.buttonWinsenComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonWinsenComClose.TabIndex = 0;
            this.buttonWinsenComClose.Text = "Close";
            this.buttonWinsenComClose.UseVisualStyleBackColor = true;
            this.buttonWinsenComClose.Click += new System.EventHandler(this.buttonWinsenComClose_Click);
            // 
            // comboBoxWinsen
            // 
            this.comboBoxWinsen.ForeColor = System.Drawing.Color.Olive;
            this.comboBoxWinsen.FormattingEnabled = true;
            this.comboBoxWinsen.Location = new System.Drawing.Point(5, 13);
            this.comboBoxWinsen.Name = "comboBoxWinsen";
            this.comboBoxWinsen.Size = new System.Drawing.Size(78, 20);
            this.comboBoxWinsen.TabIndex = 1;
            // 
            // buttonWinsenComOpen
            // 
            this.buttonWinsenComOpen.ForeColor = System.Drawing.Color.LightCoral;
            this.buttonWinsenComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonWinsenComOpen.Name = "buttonWinsenComOpen";
            this.buttonWinsenComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonWinsenComOpen.TabIndex = 0;
            this.buttonWinsenComOpen.Text = "Open";
            this.buttonWinsenComOpen.UseVisualStyleBackColor = true;
            this.buttonWinsenComOpen.Click += new System.EventHandler(this.buttonWinsenComOpen_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.buttonExtComClose);
            this.groupBox8.Controls.Add(this.comboBoxExt);
            this.groupBox8.Controls.Add(this.buttonExtComOpen);
            this.groupBox8.ForeColor = System.Drawing.Color.Olive;
            this.groupBox8.Location = new System.Drawing.Point(6, 250);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(204, 40);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Ext";
            // 
            // buttonExtComClose
            // 
            this.buttonExtComClose.ForeColor = System.Drawing.Color.Olive;
            this.buttonExtComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonExtComClose.Name = "buttonExtComClose";
            this.buttonExtComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonExtComClose.TabIndex = 0;
            this.buttonExtComClose.Text = "Close";
            this.buttonExtComClose.UseVisualStyleBackColor = true;
            this.buttonExtComClose.Click += new System.EventHandler(this.buttonExtComClose_Click);
            // 
            // comboBoxExt
            // 
            this.comboBoxExt.ForeColor = System.Drawing.Color.Olive;
            this.comboBoxExt.FormattingEnabled = true;
            this.comboBoxExt.Location = new System.Drawing.Point(5, 13);
            this.comboBoxExt.Name = "comboBoxExt";
            this.comboBoxExt.Size = new System.Drawing.Size(78, 20);
            this.comboBoxExt.TabIndex = 1;
            // 
            // buttonExtComOpen
            // 
            this.buttonExtComOpen.ForeColor = System.Drawing.Color.Olive;
            this.buttonExtComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonExtComOpen.Name = "buttonExtComOpen";
            this.buttonExtComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonExtComOpen.TabIndex = 0;
            this.buttonExtComOpen.Text = "Open";
            this.buttonExtComOpen.UseVisualStyleBackColor = true;
            this.buttonExtComOpen.Click += new System.EventHandler(this.buttonExtComOpen_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.buttonTempus4ComClose);
            this.groupBox5.Controls.Add(this.comboBoxTps4);
            this.groupBox5.Controls.Add(this.buttonTempus4ComOpen);
            this.groupBox5.ForeColor = System.Drawing.Color.Magenta;
            this.groupBox5.Location = new System.Drawing.Point(6, 206);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(204, 40);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tempus 4";
            // 
            // buttonTempus4ComClose
            // 
            this.buttonTempus4ComClose.ForeColor = System.Drawing.Color.Magenta;
            this.buttonTempus4ComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonTempus4ComClose.Name = "buttonTempus4ComClose";
            this.buttonTempus4ComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus4ComClose.TabIndex = 0;
            this.buttonTempus4ComClose.Text = "Close";
            this.buttonTempus4ComClose.UseVisualStyleBackColor = true;
            this.buttonTempus4ComClose.Click += new System.EventHandler(this.buttonTempus4ComClose_Click);
            // 
            // comboBoxTps4
            // 
            this.comboBoxTps4.ForeColor = System.Drawing.Color.Magenta;
            this.comboBoxTps4.FormattingEnabled = true;
            this.comboBoxTps4.Location = new System.Drawing.Point(5, 13);
            this.comboBoxTps4.Name = "comboBoxTps4";
            this.comboBoxTps4.Size = new System.Drawing.Size(78, 20);
            this.comboBoxTps4.TabIndex = 1;
            // 
            // buttonTempus4ComOpen
            // 
            this.buttonTempus4ComOpen.ForeColor = System.Drawing.Color.Magenta;
            this.buttonTempus4ComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonTempus4ComOpen.Name = "buttonTempus4ComOpen";
            this.buttonTempus4ComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus4ComOpen.TabIndex = 0;
            this.buttonTempus4ComOpen.Text = "Open";
            this.buttonTempus4ComOpen.UseVisualStyleBackColor = true;
            this.buttonTempus4ComOpen.Click += new System.EventHandler(this.buttonTempus4ComOpen_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonTempus3ComClose);
            this.groupBox4.Controls.Add(this.comboBoxTps3);
            this.groupBox4.Controls.Add(this.buttonTempus3ComOpen);
            this.groupBox4.Location = new System.Drawing.Point(6, 163);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(204, 40);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tempus 3";
            // 
            // buttonTempus3ComClose
            // 
            this.buttonTempus3ComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonTempus3ComClose.Name = "buttonTempus3ComClose";
            this.buttonTempus3ComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus3ComClose.TabIndex = 0;
            this.buttonTempus3ComClose.Text = "Close";
            this.buttonTempus3ComClose.UseVisualStyleBackColor = true;
            this.buttonTempus3ComClose.Click += new System.EventHandler(this.buttonTempus3ComClose_Click);
            // 
            // comboBoxTps3
            // 
            this.comboBoxTps3.FormattingEnabled = true;
            this.comboBoxTps3.Location = new System.Drawing.Point(5, 13);
            this.comboBoxTps3.Name = "comboBoxTps3";
            this.comboBoxTps3.Size = new System.Drawing.Size(78, 20);
            this.comboBoxTps3.TabIndex = 1;
            // 
            // buttonTempus3ComOpen
            // 
            this.buttonTempus3ComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonTempus3ComOpen.Name = "buttonTempus3ComOpen";
            this.buttonTempus3ComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus3ComOpen.TabIndex = 0;
            this.buttonTempus3ComOpen.Text = "Open";
            this.buttonTempus3ComOpen.UseVisualStyleBackColor = true;
            this.buttonTempus3ComOpen.Click += new System.EventHandler(this.buttonTempus3ComOpen_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonTempus2ComClose);
            this.groupBox3.Controls.Add(this.comboBoxTps2);
            this.groupBox3.Controls.Add(this.buttonTempus2ComOpen);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            this.groupBox3.Location = new System.Drawing.Point(6, 119);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(204, 40);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tempus 2";
            // 
            // buttonTempus2ComClose
            // 
            this.buttonTempus2ComClose.ForeColor = System.Drawing.Color.Blue;
            this.buttonTempus2ComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonTempus2ComClose.Name = "buttonTempus2ComClose";
            this.buttonTempus2ComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus2ComClose.TabIndex = 0;
            this.buttonTempus2ComClose.Text = "Close";
            this.buttonTempus2ComClose.UseVisualStyleBackColor = true;
            this.buttonTempus2ComClose.Click += new System.EventHandler(this.buttonTempus2ComClose_Click);
            // 
            // comboBoxTps2
            // 
            this.comboBoxTps2.ForeColor = System.Drawing.Color.Blue;
            this.comboBoxTps2.FormattingEnabled = true;
            this.comboBoxTps2.Location = new System.Drawing.Point(5, 13);
            this.comboBoxTps2.Name = "comboBoxTps2";
            this.comboBoxTps2.Size = new System.Drawing.Size(78, 20);
            this.comboBoxTps2.TabIndex = 1;
            // 
            // buttonTempus2ComOpen
            // 
            this.buttonTempus2ComOpen.ForeColor = System.Drawing.Color.Blue;
            this.buttonTempus2ComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonTempus2ComOpen.Name = "buttonTempus2ComOpen";
            this.buttonTempus2ComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus2ComOpen.TabIndex = 0;
            this.buttonTempus2ComOpen.Text = "Open";
            this.buttonTempus2ComOpen.UseVisualStyleBackColor = true;
            this.buttonTempus2ComOpen.Click += new System.EventHandler(this.buttonTempus2ComOpen_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonTempus1ComClose);
            this.groupBox2.Controls.Add(this.comboBoxTps1);
            this.groupBox2.Controls.Add(this.buttonTempus1ComOpen);
            this.groupBox2.ForeColor = System.Drawing.Color.Red;
            this.groupBox2.Location = new System.Drawing.Point(6, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(204, 40);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tempus 1";
            // 
            // buttonTempus1ComClose
            // 
            this.buttonTempus1ComClose.ForeColor = System.Drawing.Color.Red;
            this.buttonTempus1ComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonTempus1ComClose.Name = "buttonTempus1ComClose";
            this.buttonTempus1ComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus1ComClose.TabIndex = 0;
            this.buttonTempus1ComClose.Text = "Close";
            this.buttonTempus1ComClose.UseVisualStyleBackColor = true;
            this.buttonTempus1ComClose.Click += new System.EventHandler(this.buttonTempus1ComClose_Click);
            // 
            // comboBoxTps1
            // 
            this.comboBoxTps1.ForeColor = System.Drawing.Color.Red;
            this.comboBoxTps1.FormattingEnabled = true;
            this.comboBoxTps1.Location = new System.Drawing.Point(5, 13);
            this.comboBoxTps1.Name = "comboBoxTps1";
            this.comboBoxTps1.Size = new System.Drawing.Size(78, 20);
            this.comboBoxTps1.TabIndex = 1;
            // 
            // buttonTempus1ComOpen
            // 
            this.buttonTempus1ComOpen.ForeColor = System.Drawing.Color.Red;
            this.buttonTempus1ComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonTempus1ComOpen.Name = "buttonTempus1ComOpen";
            this.buttonTempus1ComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonTempus1ComOpen.TabIndex = 0;
            this.buttonTempus1ComOpen.Text = "Open";
            this.buttonTempus1ComOpen.UseVisualStyleBackColor = true;
            this.buttonTempus1ComOpen.Click += new System.EventHandler(this.buttonTempus1ComOpen_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonRefComClose);
            this.groupBox1.Controls.Add(this.comboBoxRef);
            this.groupBox1.Controls.Add(this.buttonRefComOpen);
            this.groupBox1.ForeColor = System.Drawing.Color.Green;
            this.groupBox1.Location = new System.Drawing.Point(6, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(204, 40);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SenseAir";
            // 
            // buttonRefComClose
            // 
            this.buttonRefComClose.ForeColor = System.Drawing.Color.Green;
            this.buttonRefComClose.Location = new System.Drawing.Point(146, 11);
            this.buttonRefComClose.Name = "buttonRefComClose";
            this.buttonRefComClose.Size = new System.Drawing.Size(51, 23);
            this.buttonRefComClose.TabIndex = 0;
            this.buttonRefComClose.Text = "Close";
            this.buttonRefComClose.UseVisualStyleBackColor = true;
            this.buttonRefComClose.Click += new System.EventHandler(this.buttonRefComClose_Click);
            // 
            // comboBoxRef
            // 
            this.comboBoxRef.ForeColor = System.Drawing.Color.Green;
            this.comboBoxRef.FormattingEnabled = true;
            this.comboBoxRef.Location = new System.Drawing.Point(5, 13);
            this.comboBoxRef.Name = "comboBoxRef";
            this.comboBoxRef.Size = new System.Drawing.Size(78, 20);
            this.comboBoxRef.TabIndex = 1;
            // 
            // buttonRefComOpen
            // 
            this.buttonRefComOpen.ForeColor = System.Drawing.Color.Green;
            this.buttonRefComOpen.Location = new System.Drawing.Point(89, 11);
            this.buttonRefComOpen.Name = "buttonRefComOpen";
            this.buttonRefComOpen.Size = new System.Drawing.Size(51, 23);
            this.buttonRefComOpen.TabIndex = 0;
            this.buttonRefComOpen.Text = "Open";
            this.buttonRefComOpen.UseVisualStyleBackColor = true;
            this.buttonRefComOpen.Click += new System.EventHandler(this.buttonRefComOpen_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonClear);
            this.tabPage2.Controls.Add(this.textBoxDiff5);
            this.tabPage2.Controls.Add(this.textBoxDiff4);
            this.tabPage2.Controls.Add(this.textBoxDiff3);
            this.tabPage2.Controls.Add(this.textBoxDiff2);
            this.tabPage2.Controls.Add(this.textBoxDiff1);
            this.tabPage2.Controls.Add(this.checkBoxFigaro);
            this.tabPage2.Controls.Add(this.checkBoxElt);
            this.tabPage2.Controls.Add(this.checkBoxCubic);
            this.tabPage2.Controls.Add(this.checkBoxWinsen);
            this.tabPage2.Controls.Add(this.checkBoxSenseAir);
            this.tabPage2.Controls.Add(this.checkBoxChart4);
            this.tabPage2.Controls.Add(this.checkBoxChart3);
            this.tabPage2.Controls.Add(this.checkBoxChart2);
            this.tabPage2.Controls.Add(this.checkBoxChart1);
            this.tabPage2.Controls.Add(this.chartCO2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1165, 462);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Graph (ppm)";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(1082, 433);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(64, 23);
            this.buttonClear.TabIndex = 3;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxDiff5
            // 
            this.textBoxDiff5.ForeColor = System.Drawing.Color.LightCoral;
            this.textBoxDiff5.Location = new System.Drawing.Point(1082, 334);
            this.textBoxDiff5.Name = "textBoxDiff5";
            this.textBoxDiff5.Size = new System.Drawing.Size(64, 21);
            this.textBoxDiff5.TabIndex = 2;
            this.textBoxDiff5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff4
            // 
            this.textBoxDiff4.ForeColor = System.Drawing.Color.Black;
            this.textBoxDiff4.Location = new System.Drawing.Point(1082, 290);
            this.textBoxDiff4.Name = "textBoxDiff4";
            this.textBoxDiff4.Size = new System.Drawing.Size(64, 21);
            this.textBoxDiff4.TabIndex = 2;
            this.textBoxDiff4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff3
            // 
            this.textBoxDiff3.ForeColor = System.Drawing.Color.Black;
            this.textBoxDiff3.Location = new System.Drawing.Point(1082, 268);
            this.textBoxDiff3.Name = "textBoxDiff3";
            this.textBoxDiff3.Size = new System.Drawing.Size(64, 21);
            this.textBoxDiff3.TabIndex = 2;
            this.textBoxDiff3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff2
            // 
            this.textBoxDiff2.ForeColor = System.Drawing.Color.Black;
            this.textBoxDiff2.Location = new System.Drawing.Point(1082, 246);
            this.textBoxDiff2.Name = "textBoxDiff2";
            this.textBoxDiff2.Size = new System.Drawing.Size(64, 21);
            this.textBoxDiff2.TabIndex = 2;
            this.textBoxDiff2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff1
            // 
            this.textBoxDiff1.ForeColor = System.Drawing.Color.Black;
            this.textBoxDiff1.Location = new System.Drawing.Point(1082, 224);
            this.textBoxDiff1.Name = "textBoxDiff1";
            this.textBoxDiff1.Size = new System.Drawing.Size(64, 21);
            this.textBoxDiff1.TabIndex = 2;
            this.textBoxDiff1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxFigaro
            // 
            this.checkBoxFigaro.AutoSize = true;
            this.checkBoxFigaro.ForeColor = System.Drawing.Color.Indigo;
            this.checkBoxFigaro.Location = new System.Drawing.Point(1011, 402);
            this.checkBoxFigaro.Name = "checkBoxFigaro";
            this.checkBoxFigaro.Size = new System.Drawing.Size(59, 16);
            this.checkBoxFigaro.TabIndex = 1;
            this.checkBoxFigaro.Text = "Figaro";
            this.checkBoxFigaro.UseVisualStyleBackColor = true;
            // 
            // checkBoxElt
            // 
            this.checkBoxElt.AutoSize = true;
            this.checkBoxElt.ForeColor = System.Drawing.Color.LightSlateGray;
            this.checkBoxElt.Location = new System.Drawing.Point(1011, 380);
            this.checkBoxElt.Name = "checkBoxElt";
            this.checkBoxElt.Size = new System.Drawing.Size(38, 16);
            this.checkBoxElt.TabIndex = 1;
            this.checkBoxElt.Text = "Elt";
            this.checkBoxElt.UseVisualStyleBackColor = true;
            // 
            // checkBoxCubic
            // 
            this.checkBoxCubic.AutoSize = true;
            this.checkBoxCubic.ForeColor = System.Drawing.Color.Maroon;
            this.checkBoxCubic.Location = new System.Drawing.Point(1011, 358);
            this.checkBoxCubic.Name = "checkBoxCubic";
            this.checkBoxCubic.Size = new System.Drawing.Size(57, 16);
            this.checkBoxCubic.TabIndex = 1;
            this.checkBoxCubic.Text = "Cubic";
            this.checkBoxCubic.UseVisualStyleBackColor = true;
            // 
            // checkBoxWinsen
            // 
            this.checkBoxWinsen.AutoSize = true;
            this.checkBoxWinsen.ForeColor = System.Drawing.Color.LightCoral;
            this.checkBoxWinsen.Location = new System.Drawing.Point(1011, 336);
            this.checkBoxWinsen.Name = "checkBoxWinsen";
            this.checkBoxWinsen.Size = new System.Drawing.Size(65, 16);
            this.checkBoxWinsen.TabIndex = 1;
            this.checkBoxWinsen.Text = "Winsen";
            this.checkBoxWinsen.UseVisualStyleBackColor = true;
            // 
            // checkBoxSenseAir
            // 
            this.checkBoxSenseAir.AutoSize = true;
            this.checkBoxSenseAir.ForeColor = System.Drawing.Color.Green;
            this.checkBoxSenseAir.Location = new System.Drawing.Point(1011, 314);
            this.checkBoxSenseAir.Name = "checkBoxSenseAir";
            this.checkBoxSenseAir.Size = new System.Drawing.Size(75, 16);
            this.checkBoxSenseAir.TabIndex = 1;
            this.checkBoxSenseAir.Text = "SenseAir";
            this.checkBoxSenseAir.UseVisualStyleBackColor = true;
            // 
            // checkBoxChart4
            // 
            this.checkBoxChart4.AutoSize = true;
            this.checkBoxChart4.ForeColor = System.Drawing.Color.Magenta;
            this.checkBoxChart4.Location = new System.Drawing.Point(1011, 292);
            this.checkBoxChart4.Name = "checkBoxChart4";
            this.checkBoxChart4.Size = new System.Drawing.Size(54, 16);
            this.checkBoxChart4.TabIndex = 1;
            this.checkBoxChart4.Text = "TPS4";
            this.checkBoxChart4.UseVisualStyleBackColor = true;
            // 
            // checkBoxChart3
            // 
            this.checkBoxChart3.AutoSize = true;
            this.checkBoxChart3.Location = new System.Drawing.Point(1011, 270);
            this.checkBoxChart3.Name = "checkBoxChart3";
            this.checkBoxChart3.Size = new System.Drawing.Size(54, 16);
            this.checkBoxChart3.TabIndex = 1;
            this.checkBoxChart3.Text = "TPS3";
            this.checkBoxChart3.UseVisualStyleBackColor = true;
            // 
            // checkBoxChart2
            // 
            this.checkBoxChart2.AutoSize = true;
            this.checkBoxChart2.ForeColor = System.Drawing.Color.Blue;
            this.checkBoxChart2.Location = new System.Drawing.Point(1011, 248);
            this.checkBoxChart2.Name = "checkBoxChart2";
            this.checkBoxChart2.Size = new System.Drawing.Size(54, 16);
            this.checkBoxChart2.TabIndex = 1;
            this.checkBoxChart2.Text = "TPS2";
            this.checkBoxChart2.UseVisualStyleBackColor = true;
            // 
            // checkBoxChart1
            // 
            this.checkBoxChart1.AutoSize = true;
            this.checkBoxChart1.ForeColor = System.Drawing.Color.Red;
            this.checkBoxChart1.Location = new System.Drawing.Point(1011, 226);
            this.checkBoxChart1.Name = "checkBoxChart1";
            this.checkBoxChart1.Size = new System.Drawing.Size(54, 16);
            this.checkBoxChart1.TabIndex = 1;
            this.checkBoxChart1.Text = "TPS1";
            this.checkBoxChart1.UseVisualStyleBackColor = true;
            // 
            // chartCO2
            // 
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.Name = "ChartArea1";
            this.chartCO2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartCO2.Legends.Add(legend1);
            this.chartCO2.Location = new System.Drawing.Point(3, 3);
            this.chartCO2.Name = "chartCO2";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartCO2.Series.Add(series1);
            this.chartCO2.Size = new System.Drawing.Size(1158, 455);
            this.chartCO2.TabIndex = 0;
            this.chartCO2.Text = "chart1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.checkBoxDcDr4);
            this.tabPage3.Controls.Add(this.checkBoxDcDr3);
            this.tabPage3.Controls.Add(this.checkBoxDcDr2);
            this.tabPage3.Controls.Add(this.checkBoxDcDr1);
            this.tabPage3.Controls.Add(this.buttonDcDrClear);
            this.tabPage3.Controls.Add(this.chartDcDr);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1165, 462);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Graph (dC/dR)";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // checkBoxDcDr4
            // 
            this.checkBoxDcDr4.AutoSize = true;
            this.checkBoxDcDr4.ForeColor = System.Drawing.Color.Magenta;
            this.checkBoxDcDr4.Location = new System.Drawing.Point(1087, 333);
            this.checkBoxDcDr4.Name = "checkBoxDcDr4";
            this.checkBoxDcDr4.Size = new System.Drawing.Size(54, 16);
            this.checkBoxDcDr4.TabIndex = 2;
            this.checkBoxDcDr4.Text = "TPS4";
            this.checkBoxDcDr4.UseVisualStyleBackColor = true;
            // 
            // checkBoxDcDr3
            // 
            this.checkBoxDcDr3.AutoSize = true;
            this.checkBoxDcDr3.Location = new System.Drawing.Point(1087, 311);
            this.checkBoxDcDr3.Name = "checkBoxDcDr3";
            this.checkBoxDcDr3.Size = new System.Drawing.Size(54, 16);
            this.checkBoxDcDr3.TabIndex = 2;
            this.checkBoxDcDr3.Text = "TPS3";
            this.checkBoxDcDr3.UseVisualStyleBackColor = true;
            // 
            // checkBoxDcDr2
            // 
            this.checkBoxDcDr2.AutoSize = true;
            this.checkBoxDcDr2.ForeColor = System.Drawing.Color.Blue;
            this.checkBoxDcDr2.Location = new System.Drawing.Point(1087, 289);
            this.checkBoxDcDr2.Name = "checkBoxDcDr2";
            this.checkBoxDcDr2.Size = new System.Drawing.Size(54, 16);
            this.checkBoxDcDr2.TabIndex = 2;
            this.checkBoxDcDr2.Text = "TPS2";
            this.checkBoxDcDr2.UseVisualStyleBackColor = true;
            // 
            // checkBoxDcDr1
            // 
            this.checkBoxDcDr1.AutoSize = true;
            this.checkBoxDcDr1.ForeColor = System.Drawing.Color.Red;
            this.checkBoxDcDr1.Location = new System.Drawing.Point(1087, 267);
            this.checkBoxDcDr1.Name = "checkBoxDcDr1";
            this.checkBoxDcDr1.Size = new System.Drawing.Size(54, 16);
            this.checkBoxDcDr1.TabIndex = 2;
            this.checkBoxDcDr1.Text = "TPS1";
            this.checkBoxDcDr1.UseVisualStyleBackColor = true;
            // 
            // buttonDcDrClear
            // 
            this.buttonDcDrClear.Location = new System.Drawing.Point(1087, 436);
            this.buttonDcDrClear.Name = "buttonDcDrClear";
            this.buttonDcDrClear.Size = new System.Drawing.Size(75, 23);
            this.buttonDcDrClear.TabIndex = 1;
            this.buttonDcDrClear.Text = "Clear";
            this.buttonDcDrClear.UseVisualStyleBackColor = true;
            this.buttonDcDrClear.Click += new System.EventHandler(this.buttonDcDrClear_Click);
            // 
            // chartDcDr
            // 
            chartArea2.AxisY.IsStartedFromZero = false;
            chartArea2.Name = "ChartArea1";
            this.chartDcDr.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartDcDr.Legends.Add(legend2);
            this.chartDcDr.Location = new System.Drawing.Point(4, 4);
            this.chartDcDr.Name = "chartDcDr";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartDcDr.Series.Add(series2);
            this.chartDcDr.Size = new System.Drawing.Size(1158, 455);
            this.chartDcDr.TabIndex = 0;
            this.chartDcDr.Text = "chart1";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.checkBoxCurv4);
            this.tabPage4.Controls.Add(this.checkBoxCurv3);
            this.tabPage4.Controls.Add(this.checkBoxCurv2);
            this.tabPage4.Controls.Add(this.checkBoxCurv1);
            this.tabPage4.Controls.Add(this.buttonCurvClear);
            this.tabPage4.Controls.Add(this.chartCurv);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1165, 462);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Graph (A0/Pk)";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // checkBoxCurv4
            // 
            this.checkBoxCurv4.AutoSize = true;
            this.checkBoxCurv4.Location = new System.Drawing.Point(1087, 354);
            this.checkBoxCurv4.Name = "checkBoxCurv4";
            this.checkBoxCurv4.Size = new System.Drawing.Size(54, 16);
            this.checkBoxCurv4.TabIndex = 13;
            this.checkBoxCurv4.Text = "TPS4";
            this.checkBoxCurv4.UseVisualStyleBackColor = true;
            // 
            // checkBoxCurv3
            // 
            this.checkBoxCurv3.AutoSize = true;
            this.checkBoxCurv3.Location = new System.Drawing.Point(1087, 332);
            this.checkBoxCurv3.Name = "checkBoxCurv3";
            this.checkBoxCurv3.Size = new System.Drawing.Size(54, 16);
            this.checkBoxCurv3.TabIndex = 13;
            this.checkBoxCurv3.Text = "TPS3";
            this.checkBoxCurv3.UseVisualStyleBackColor = true;
            // 
            // checkBoxCurv2
            // 
            this.checkBoxCurv2.AutoSize = true;
            this.checkBoxCurv2.Location = new System.Drawing.Point(1087, 310);
            this.checkBoxCurv2.Name = "checkBoxCurv2";
            this.checkBoxCurv2.Size = new System.Drawing.Size(54, 16);
            this.checkBoxCurv2.TabIndex = 13;
            this.checkBoxCurv2.Text = "TPS2";
            this.checkBoxCurv2.UseVisualStyleBackColor = true;
            // 
            // checkBoxCurv1
            // 
            this.checkBoxCurv1.AutoSize = true;
            this.checkBoxCurv1.Location = new System.Drawing.Point(1087, 288);
            this.checkBoxCurv1.Name = "checkBoxCurv1";
            this.checkBoxCurv1.Size = new System.Drawing.Size(54, 16);
            this.checkBoxCurv1.TabIndex = 13;
            this.checkBoxCurv1.Text = "TPS1";
            this.checkBoxCurv1.UseVisualStyleBackColor = true;
            // 
            // buttonCurvClear
            // 
            this.buttonCurvClear.Location = new System.Drawing.Point(1087, 436);
            this.buttonCurvClear.Name = "buttonCurvClear";
            this.buttonCurvClear.Size = new System.Drawing.Size(75, 23);
            this.buttonCurvClear.TabIndex = 12;
            this.buttonCurvClear.Text = "Clear";
            this.buttonCurvClear.UseVisualStyleBackColor = true;
            this.buttonCurvClear.Click += new System.EventHandler(this.buttonCurvClear_Click);
            // 
            // chartCurv
            // 
            chartArea3.AxisY.IsStartedFromZero = false;
            chartArea3.Name = "ChartArea1";
            this.chartCurv.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartCurv.Legends.Add(legend3);
            this.chartCurv.Location = new System.Drawing.Point(4, 4);
            this.chartCurv.Name = "chartCurv";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chartCurv.Series.Add(series3);
            this.chartCurv.Size = new System.Drawing.Size(1158, 455);
            this.chartCurv.TabIndex = 0;
            this.chartCurv.Text = "chartCurv";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.buttonCurv);
            this.tabPage5.Controls.Add(this.button1);
            this.tabPage5.Controls.Add(this.chartDispCurv);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1165, 462);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Graph(Curv)";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // buttonCurv
            // 
            this.buttonCurv.Location = new System.Drawing.Point(1108, 391);
            this.buttonCurv.Name = "buttonCurv";
            this.buttonCurv.Size = new System.Drawing.Size(48, 23);
            this.buttonCurv.TabIndex = 11;
            this.buttonCurv.Text = "Curv";
            this.buttonCurv.UseVisualStyleBackColor = true;
            this.buttonCurv.Click += new System.EventHandler(this.buttonCurv_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1108, 436);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chartDispCurv
            // 
            chartArea4.AxisY.IsStartedFromZero = false;
            chartArea4.Name = "ChartArea1";
            this.chartDispCurv.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chartDispCurv.Legends.Add(legend4);
            this.chartDispCurv.Location = new System.Drawing.Point(4, 4);
            this.chartDispCurv.Name = "chartDispCurv";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chartDispCurv.Series.Add(series4);
            this.chartDispCurv.Size = new System.Drawing.Size(1158, 455);
            this.chartDispCurv.TabIndex = 0;
            this.chartDispCurv.Text = "chart1";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBoxDetectorId);
            this.tabPage6.Controls.Add(this.groupBox21);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1165, 462);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Status Flag";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBoxDetectorId
            // 
            this.groupBoxDetectorId.Controls.Add(this.buttonDetectorId);
            this.groupBoxDetectorId.Controls.Add(this.textBoxDetectorId4);
            this.groupBoxDetectorId.Controls.Add(this.textBoxDetectorId3);
            this.groupBoxDetectorId.Controls.Add(this.textBoxDetectorId2);
            this.groupBoxDetectorId.Controls.Add(this.textBoxDetectorId1);
            this.groupBoxDetectorId.Controls.Add(this.label11);
            this.groupBoxDetectorId.Controls.Add(this.label10);
            this.groupBoxDetectorId.Controls.Add(this.label9);
            this.groupBoxDetectorId.Controls.Add(this.label8);
            this.groupBoxDetectorId.Location = new System.Drawing.Point(523, 15);
            this.groupBoxDetectorId.Name = "groupBoxDetectorId";
            this.groupBoxDetectorId.Size = new System.Drawing.Size(125, 162);
            this.groupBoxDetectorId.TabIndex = 12;
            this.groupBoxDetectorId.TabStop = false;
            this.groupBoxDetectorId.Text = "Detector ID";
            // 
            // buttonDetectorId
            // 
            this.buttonDetectorId.Location = new System.Drawing.Point(7, 130);
            this.buttonDetectorId.Name = "buttonDetectorId";
            this.buttonDetectorId.Size = new System.Drawing.Size(75, 23);
            this.buttonDetectorId.TabIndex = 6;
            this.buttonDetectorId.Text = "ID";
            this.buttonDetectorId.UseVisualStyleBackColor = true;
            // 
            // textBoxDetectorId4
            // 
            this.textBoxDetectorId4.Location = new System.Drawing.Point(7, 102);
            this.textBoxDetectorId4.Name = "textBoxDetectorId4";
            this.textBoxDetectorId4.Size = new System.Drawing.Size(76, 21);
            this.textBoxDetectorId4.TabIndex = 0;
            this.textBoxDetectorId4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDetectorId3
            // 
            this.textBoxDetectorId3.Location = new System.Drawing.Point(6, 75);
            this.textBoxDetectorId3.Name = "textBoxDetectorId3";
            this.textBoxDetectorId3.Size = new System.Drawing.Size(76, 21);
            this.textBoxDetectorId3.TabIndex = 0;
            this.textBoxDetectorId3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDetectorId2
            // 
            this.textBoxDetectorId2.Location = new System.Drawing.Point(6, 48);
            this.textBoxDetectorId2.Name = "textBoxDetectorId2";
            this.textBoxDetectorId2.Size = new System.Drawing.Size(76, 21);
            this.textBoxDetectorId2.TabIndex = 0;
            this.textBoxDetectorId2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDetectorId1
            // 
            this.textBoxDetectorId1.Location = new System.Drawing.Point(7, 21);
            this.textBoxDetectorId1.Name = "textBoxDetectorId1";
            this.textBoxDetectorId1.Size = new System.Drawing.Size(76, 21);
            this.textBoxDetectorId1.TabIndex = 0;
            this.textBoxDetectorId1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(88, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 12);
            this.label11.TabIndex = 5;
            this.label11.Text = "ID 4";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(88, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 12);
            this.label10.TabIndex = 5;
            this.label10.Text = "ID 3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(88, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 12);
            this.label9.TabIndex = 5;
            this.label9.Text = "ID 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(89, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 12);
            this.label8.TabIndex = 5;
            this.label8.Text = "ID 1";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.groupBox29);
            this.groupBox21.Controls.Add(this.groupBox26);
            this.groupBox21.Controls.Add(this.groupBox23);
            this.groupBox21.Controls.Add(this.groupBox22);
            this.groupBox21.Location = new System.Drawing.Point(17, 15);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(464, 220);
            this.groupBox21.TabIndex = 1;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Mode";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.groupBox30);
            this.groupBox29.Controls.Add(this.groupBox31);
            this.groupBox29.Location = new System.Drawing.Point(234, 118);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(221, 95);
            this.groupBox29.TabIndex = 0;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "TPS4";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.radioButtonTps4RefCmdMode);
            this.groupBox30.Controls.Add(this.radioButtonTps4RefNorMode);
            this.groupBox30.Location = new System.Drawing.Point(111, 20);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(99, 63);
            this.groupBox30.TabIndex = 2;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "REF";
            // 
            // radioButtonTps4RefCmdMode
            // 
            this.radioButtonTps4RefCmdMode.AutoSize = true;
            this.radioButtonTps4RefCmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps4RefCmdMode.Name = "radioButtonTps4RefCmdMode";
            this.radioButtonTps4RefCmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps4RefCmdMode.TabIndex = 0;
            this.radioButtonTps4RefCmdMode.TabStop = true;
            this.radioButtonTps4RefCmdMode.Text = "CMD Mode";
            this.radioButtonTps4RefCmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps4RefNorMode
            // 
            this.radioButtonTps4RefNorMode.AutoSize = true;
            this.radioButtonTps4RefNorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps4RefNorMode.Name = "radioButtonTps4RefNorMode";
            this.radioButtonTps4RefNorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps4RefNorMode.TabIndex = 0;
            this.radioButtonTps4RefNorMode.TabStop = true;
            this.radioButtonTps4RefNorMode.Text = "NOR Mode";
            this.radioButtonTps4RefNorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.radioButtonTps4Co2CmdMode);
            this.groupBox31.Controls.Add(this.radioButtonTps4Co2NorMode);
            this.groupBox31.Location = new System.Drawing.Point(6, 20);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(99, 63);
            this.groupBox31.TabIndex = 2;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "CO2";
            // 
            // radioButtonTps4Co2CmdMode
            // 
            this.radioButtonTps4Co2CmdMode.AutoSize = true;
            this.radioButtonTps4Co2CmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps4Co2CmdMode.Name = "radioButtonTps4Co2CmdMode";
            this.radioButtonTps4Co2CmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps4Co2CmdMode.TabIndex = 0;
            this.radioButtonTps4Co2CmdMode.TabStop = true;
            this.radioButtonTps4Co2CmdMode.Text = "CMD Mode";
            this.radioButtonTps4Co2CmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps4Co2NorMode
            // 
            this.radioButtonTps4Co2NorMode.AutoSize = true;
            this.radioButtonTps4Co2NorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps4Co2NorMode.Name = "radioButtonTps4Co2NorMode";
            this.radioButtonTps4Co2NorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps4Co2NorMode.TabIndex = 0;
            this.radioButtonTps4Co2NorMode.TabStop = true;
            this.radioButtonTps4Co2NorMode.Text = "NOR Mode";
            this.radioButtonTps4Co2NorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.groupBox27);
            this.groupBox26.Controls.Add(this.groupBox28);
            this.groupBox26.Location = new System.Drawing.Point(7, 118);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(221, 95);
            this.groupBox26.TabIndex = 0;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "TPS3";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.radioButtonTps3RefCmdMode);
            this.groupBox27.Controls.Add(this.radioButtonTps3RefNorMode);
            this.groupBox27.Location = new System.Drawing.Point(111, 20);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(99, 63);
            this.groupBox27.TabIndex = 2;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "REF";
            // 
            // radioButtonTps3RefCmdMode
            // 
            this.radioButtonTps3RefCmdMode.AutoSize = true;
            this.radioButtonTps3RefCmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps3RefCmdMode.Name = "radioButtonTps3RefCmdMode";
            this.radioButtonTps3RefCmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps3RefCmdMode.TabIndex = 0;
            this.radioButtonTps3RefCmdMode.TabStop = true;
            this.radioButtonTps3RefCmdMode.Text = "CMD Mode";
            this.radioButtonTps3RefCmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps3RefNorMode
            // 
            this.radioButtonTps3RefNorMode.AutoSize = true;
            this.radioButtonTps3RefNorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps3RefNorMode.Name = "radioButtonTps3RefNorMode";
            this.radioButtonTps3RefNorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps3RefNorMode.TabIndex = 0;
            this.radioButtonTps3RefNorMode.TabStop = true;
            this.radioButtonTps3RefNorMode.Text = "NOR Mode";
            this.radioButtonTps3RefNorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.radioButtonTps3Co2CmdMode);
            this.groupBox28.Controls.Add(this.radioButtonTps3Co2NorMode);
            this.groupBox28.Location = new System.Drawing.Point(6, 20);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(99, 63);
            this.groupBox28.TabIndex = 2;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "CO2";
            // 
            // radioButtonTps3Co2CmdMode
            // 
            this.radioButtonTps3Co2CmdMode.AutoSize = true;
            this.radioButtonTps3Co2CmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps3Co2CmdMode.Name = "radioButtonTps3Co2CmdMode";
            this.radioButtonTps3Co2CmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps3Co2CmdMode.TabIndex = 0;
            this.radioButtonTps3Co2CmdMode.TabStop = true;
            this.radioButtonTps3Co2CmdMode.Text = "CMD Mode";
            this.radioButtonTps3Co2CmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps3Co2NorMode
            // 
            this.radioButtonTps3Co2NorMode.AutoSize = true;
            this.radioButtonTps3Co2NorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps3Co2NorMode.Name = "radioButtonTps3Co2NorMode";
            this.radioButtonTps3Co2NorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps3Co2NorMode.TabIndex = 0;
            this.radioButtonTps3Co2NorMode.TabStop = true;
            this.radioButtonTps3Co2NorMode.Text = "NOR Mode";
            this.radioButtonTps3Co2NorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.groupBox24);
            this.groupBox23.Controls.Add(this.groupBox25);
            this.groupBox23.Location = new System.Drawing.Point(234, 17);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(221, 95);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "TPS2";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.radioButtonTps2RefCmdMode);
            this.groupBox24.Controls.Add(this.radioButtonTps2RefNorMode);
            this.groupBox24.Location = new System.Drawing.Point(111, 20);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(99, 63);
            this.groupBox24.TabIndex = 2;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "REF";
            // 
            // radioButtonTps2RefCmdMode
            // 
            this.radioButtonTps2RefCmdMode.AutoSize = true;
            this.radioButtonTps2RefCmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps2RefCmdMode.Name = "radioButtonTps2RefCmdMode";
            this.radioButtonTps2RefCmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps2RefCmdMode.TabIndex = 0;
            this.radioButtonTps2RefCmdMode.TabStop = true;
            this.radioButtonTps2RefCmdMode.Text = "CMD Mode";
            this.radioButtonTps2RefCmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps2RefNorMode
            // 
            this.radioButtonTps2RefNorMode.AutoSize = true;
            this.radioButtonTps2RefNorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps2RefNorMode.Name = "radioButtonTps2RefNorMode";
            this.radioButtonTps2RefNorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps2RefNorMode.TabIndex = 0;
            this.radioButtonTps2RefNorMode.TabStop = true;
            this.radioButtonTps2RefNorMode.Text = "NOR Mode";
            this.radioButtonTps2RefNorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.radioButtonTps2Co2CmdMode);
            this.groupBox25.Controls.Add(this.radioButtonTps2Co2NorMode);
            this.groupBox25.Location = new System.Drawing.Point(6, 20);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(99, 63);
            this.groupBox25.TabIndex = 2;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "CO2";
            // 
            // radioButtonTps2Co2CmdMode
            // 
            this.radioButtonTps2Co2CmdMode.AutoSize = true;
            this.radioButtonTps2Co2CmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps2Co2CmdMode.Name = "radioButtonTps2Co2CmdMode";
            this.radioButtonTps2Co2CmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps2Co2CmdMode.TabIndex = 0;
            this.radioButtonTps2Co2CmdMode.TabStop = true;
            this.radioButtonTps2Co2CmdMode.Text = "CMD Mode";
            this.radioButtonTps2Co2CmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps2Co2NorMode
            // 
            this.radioButtonTps2Co2NorMode.AutoSize = true;
            this.radioButtonTps2Co2NorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps2Co2NorMode.Name = "radioButtonTps2Co2NorMode";
            this.radioButtonTps2Co2NorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps2Co2NorMode.TabIndex = 0;
            this.radioButtonTps2Co2NorMode.TabStop = true;
            this.radioButtonTps2Co2NorMode.Text = "NOR Mode";
            this.radioButtonTps2Co2NorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox20);
            this.groupBox22.Controls.Add(this.groupBox19);
            this.groupBox22.Location = new System.Drawing.Point(7, 17);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(221, 95);
            this.groupBox22.TabIndex = 0;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "TPS1";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.radioButtonTps1RefCmdMode);
            this.groupBox20.Controls.Add(this.radioButtonTps1RefNorMode);
            this.groupBox20.Location = new System.Drawing.Point(111, 20);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(99, 63);
            this.groupBox20.TabIndex = 2;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "REF";
            // 
            // radioButtonTps1RefCmdMode
            // 
            this.radioButtonTps1RefCmdMode.AutoSize = true;
            this.radioButtonTps1RefCmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps1RefCmdMode.Name = "radioButtonTps1RefCmdMode";
            this.radioButtonTps1RefCmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps1RefCmdMode.TabIndex = 0;
            this.radioButtonTps1RefCmdMode.TabStop = true;
            this.radioButtonTps1RefCmdMode.Text = "CMD Mode";
            this.radioButtonTps1RefCmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps1RefNorMode
            // 
            this.radioButtonTps1RefNorMode.AutoSize = true;
            this.radioButtonTps1RefNorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps1RefNorMode.Name = "radioButtonTps1RefNorMode";
            this.radioButtonTps1RefNorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps1RefNorMode.TabIndex = 0;
            this.radioButtonTps1RefNorMode.TabStop = true;
            this.radioButtonTps1RefNorMode.Text = "NOR Mode";
            this.radioButtonTps1RefNorMode.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.radioButtonTps1Co2CmdMode);
            this.groupBox19.Controls.Add(this.radioButtonTps1Co2NorMode);
            this.groupBox19.Location = new System.Drawing.Point(6, 20);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(99, 63);
            this.groupBox19.TabIndex = 2;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "CO2";
            // 
            // radioButtonTps1Co2CmdMode
            // 
            this.radioButtonTps1Co2CmdMode.AutoSize = true;
            this.radioButtonTps1Co2CmdMode.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTps1Co2CmdMode.Name = "radioButtonTps1Co2CmdMode";
            this.radioButtonTps1Co2CmdMode.Size = new System.Drawing.Size(87, 16);
            this.radioButtonTps1Co2CmdMode.TabIndex = 0;
            this.radioButtonTps1Co2CmdMode.TabStop = true;
            this.radioButtonTps1Co2CmdMode.Text = "CMD Mode";
            this.radioButtonTps1Co2CmdMode.UseVisualStyleBackColor = true;
            // 
            // radioButtonTps1Co2NorMode
            // 
            this.radioButtonTps1Co2NorMode.AutoSize = true;
            this.radioButtonTps1Co2NorMode.Location = new System.Drawing.Point(6, 20);
            this.radioButtonTps1Co2NorMode.Name = "radioButtonTps1Co2NorMode";
            this.radioButtonTps1Co2NorMode.Size = new System.Drawing.Size(85, 16);
            this.radioButtonTps1Co2NorMode.TabIndex = 0;
            this.radioButtonTps1Co2NorMode.TabStop = true;
            this.radioButtonTps1Co2NorMode.Text = "NOR Mode";
            this.radioButtonTps1Co2NorMode.UseVisualStyleBackColor = true;
            // 
            // buttonPcParamGet
            // 
            this.buttonPcParamGet.Location = new System.Drawing.Point(158, 134);
            this.buttonPcParamGet.Name = "buttonPcParamGet";
            this.buttonPcParamGet.Size = new System.Drawing.Size(35, 22);
            this.buttonPcParamGet.TabIndex = 7;
            this.buttonPcParamGet.Text = "Get";
            this.buttonPcParamGet.UseVisualStyleBackColor = true;
            this.buttonPcParamGet.Click += new System.EventHandler(this.buttonPcParamGet_Click);
            // 
            // buttonPcParamSet
            // 
            this.buttonPcParamSet.Location = new System.Drawing.Point(115, 134);
            this.buttonPcParamSet.Name = "buttonPcParamSet";
            this.buttonPcParamSet.Size = new System.Drawing.Size(35, 22);
            this.buttonPcParamSet.TabIndex = 7;
            this.buttonPcParamSet.Text = "Set";
            this.buttonPcParamSet.UseVisualStyleBackColor = true;
            this.buttonPcParamSet.Click += new System.EventHandler(this.buttonPcParamSet_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 139);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 12);
            this.label23.TabIndex = 2;
            this.label23.Text = "PC";
            // 
            // textBoxPcParam
            // 
            this.textBoxPcParam.Location = new System.Drawing.Point(35, 135);
            this.textBoxPcParam.Name = "textBoxPcParam";
            this.textBoxPcParam.Size = new System.Drawing.Size(74, 21);
            this.textBoxPcParam.TabIndex = 0;
            this.textBoxPcParam.Text = "9600";
            this.textBoxPcParam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.textBoxPcParam, "Value / 10,000");
            // 
            // buttonCapture
            // 
            this.buttonCapture.Location = new System.Drawing.Point(652, 149);
            this.buttonCapture.Name = "buttonCapture";
            this.buttonCapture.Size = new System.Drawing.Size(64, 23);
            this.buttonCapture.TabIndex = 4;
            this.buttonCapture.Text = "Capture";
            this.buttonCapture.UseVisualStyleBackColor = true;
            this.buttonCapture.Click += new System.EventHandler(this.buttonCapture_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buttonTcParamGet);
            this.panel1.Controls.Add(this.buttonTcParamSet);
            this.panel1.Controls.Add(this.buttonPcParamGet);
            this.panel1.Controls.Add(this.buttonGain);
            this.panel1.Controls.Add(this.buttonPcParamSet);
            this.panel1.Controls.Add(this.buttonTs);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.buttonSr);
            this.panel1.Controls.Add(this.buttonSc);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxTcParam);
            this.panel1.Controls.Add(this.textBoxPcParam);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxGain);
            this.panel1.Controls.Add(this.textBoxTs);
            this.panel1.Controls.Add(this.textBoxSr);
            this.panel1.Controls.Add(this.buttonSync);
            this.panel1.Controls.Add(this.textBoxSc);
            this.panel1.Controls.Add(this.radioButtonTempus4);
            this.panel1.Controls.Add(this.radioButtonTempus2);
            this.panel1.Controls.Add(this.radioButtonTempus3);
            this.panel1.Controls.Add(this.radioButtonTempus1);
            this.panel1.Location = new System.Drawing.Point(980, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 186);
            this.panel1.TabIndex = 7;
            // 
            // buttonTcParamGet
            // 
            this.buttonTcParamGet.Location = new System.Drawing.Point(158, 157);
            this.buttonTcParamGet.Name = "buttonTcParamGet";
            this.buttonTcParamGet.Size = new System.Drawing.Size(35, 22);
            this.buttonTcParamGet.TabIndex = 18;
            this.buttonTcParamGet.Text = "Get";
            this.buttonTcParamGet.UseVisualStyleBackColor = true;
            this.buttonTcParamGet.Click += new System.EventHandler(this.buttonTcParamGet_Click);
            // 
            // buttonTcParamSet
            // 
            this.buttonTcParamSet.Location = new System.Drawing.Point(115, 157);
            this.buttonTcParamSet.Name = "buttonTcParamSet";
            this.buttonTcParamSet.Size = new System.Drawing.Size(35, 22);
            this.buttonTcParamSet.TabIndex = 18;
            this.buttonTcParamSet.Text = "Set";
            this.buttonTcParamSet.UseVisualStyleBackColor = true;
            this.buttonTcParamSet.Click += new System.EventHandler(this.buttonTcParamSet_Click);
            // 
            // buttonGain
            // 
            this.buttonGain.Location = new System.Drawing.Point(115, 112);
            this.buttonGain.Name = "buttonGain";
            this.buttonGain.Size = new System.Drawing.Size(35, 22);
            this.buttonGain.TabIndex = 15;
            this.buttonGain.Text = "Set";
            this.buttonGain.UseVisualStyleBackColor = true;
            this.buttonGain.Click += new System.EventHandler(this.buttonGain_Click);
            // 
            // buttonTs
            // 
            this.buttonTs.Location = new System.Drawing.Point(115, 91);
            this.buttonTs.Name = "buttonTs";
            this.buttonTs.Size = new System.Drawing.Size(35, 22);
            this.buttonTs.TabIndex = 16;
            this.buttonTs.Text = "Set";
            this.buttonTs.UseVisualStyleBackColor = true;
            this.buttonTs.Click += new System.EventHandler(this.buttonTs_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 162);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(22, 12);
            this.label21.TabIndex = 2;
            this.label21.Text = "TC";
            // 
            // buttonSr
            // 
            this.buttonSr.Location = new System.Drawing.Point(115, 69);
            this.buttonSr.Name = "buttonSr";
            this.buttonSr.Size = new System.Drawing.Size(35, 22);
            this.buttonSr.TabIndex = 17;
            this.buttonSr.Text = "Set";
            this.buttonSr.UseVisualStyleBackColor = true;
            this.buttonSr.Click += new System.EventHandler(this.buttonSr_Click);
            // 
            // buttonSc
            // 
            this.buttonSc.Location = new System.Drawing.Point(115, 47);
            this.buttonSc.Name = "buttonSc";
            this.buttonSc.Size = new System.Drawing.Size(35, 22);
            this.buttonSc.TabIndex = 18;
            this.buttonSc.Text = "Set";
            this.buttonSc.UseVisualStyleBackColor = true;
            this.buttonSc.Click += new System.EventHandler(this.buttonSc_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "G";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "TS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "sR";
            // 
            // textBoxTcParam
            // 
            this.textBoxTcParam.Location = new System.Drawing.Point(35, 158);
            this.textBoxTcParam.Name = "textBoxTcParam";
            this.textBoxTcParam.Size = new System.Drawing.Size(74, 21);
            this.textBoxTcParam.TabIndex = 0;
            this.textBoxTcParam.Text = "25";
            this.textBoxTcParam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.textBoxTcParam, "Value / 100,000");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "sC";
            // 
            // textBoxGain
            // 
            this.textBoxGain.Location = new System.Drawing.Point(35, 113);
            this.textBoxGain.Name = "textBoxGain";
            this.textBoxGain.Size = new System.Drawing.Size(74, 21);
            this.textBoxGain.TabIndex = 7;
            this.textBoxGain.Text = "50000";
            this.textBoxGain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxTs
            // 
            this.textBoxTs.Location = new System.Drawing.Point(35, 91);
            this.textBoxTs.Name = "textBoxTs";
            this.textBoxTs.Size = new System.Drawing.Size(74, 21);
            this.textBoxTs.TabIndex = 10;
            this.textBoxTs.Text = "10";
            this.textBoxTs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSr
            // 
            this.textBoxSr.Location = new System.Drawing.Point(35, 69);
            this.textBoxSr.Name = "textBoxSr";
            this.textBoxSr.Size = new System.Drawing.Size(74, 21);
            this.textBoxSr.TabIndex = 9;
            this.textBoxSr.Text = "2800000";
            this.textBoxSr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSync
            // 
            this.buttonSync.Location = new System.Drawing.Point(12, 9);
            this.buttonSync.Name = "buttonSync";
            this.buttonSync.Size = new System.Drawing.Size(42, 23);
            this.buttonSync.TabIndex = 8;
            this.buttonSync.Text = "Sync";
            this.buttonSync.UseVisualStyleBackColor = true;
            this.buttonSync.Click += new System.EventHandler(this.buttonSync_Click);
            // 
            // textBoxSc
            // 
            this.textBoxSc.Location = new System.Drawing.Point(35, 47);
            this.textBoxSc.Name = "textBoxSc";
            this.textBoxSc.Size = new System.Drawing.Size(74, 21);
            this.textBoxSc.TabIndex = 10;
            this.textBoxSc.Text = "2800000";
            this.textBoxSc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radioButtonTempus4
            // 
            this.radioButtonTempus4.AutoSize = true;
            this.radioButtonTempus4.ForeColor = System.Drawing.Color.Magenta;
            this.radioButtonTempus4.Location = new System.Drawing.Point(125, 25);
            this.radioButtonTempus4.Name = "radioButtonTempus4";
            this.radioButtonTempus4.Size = new System.Drawing.Size(57, 16);
            this.radioButtonTempus4.TabIndex = 0;
            this.radioButtonTempus4.Text = "TPS 4";
            this.radioButtonTempus4.UseVisualStyleBackColor = true;
            // 
            // radioButtonTempus2
            // 
            this.radioButtonTempus2.AutoSize = true;
            this.radioButtonTempus2.ForeColor = System.Drawing.Color.Blue;
            this.radioButtonTempus2.Location = new System.Drawing.Point(125, 3);
            this.radioButtonTempus2.Name = "radioButtonTempus2";
            this.radioButtonTempus2.Size = new System.Drawing.Size(57, 16);
            this.radioButtonTempus2.TabIndex = 0;
            this.radioButtonTempus2.Text = "TPS 2";
            this.radioButtonTempus2.UseVisualStyleBackColor = true;
            // 
            // radioButtonTempus3
            // 
            this.radioButtonTempus3.AutoSize = true;
            this.radioButtonTempus3.Location = new System.Drawing.Point(62, 25);
            this.radioButtonTempus3.Name = "radioButtonTempus3";
            this.radioButtonTempus3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonTempus3.Size = new System.Drawing.Size(57, 16);
            this.radioButtonTempus3.TabIndex = 0;
            this.radioButtonTempus3.Text = "TPS 3";
            this.radioButtonTempus3.UseVisualStyleBackColor = true;
            // 
            // radioButtonTempus1
            // 
            this.radioButtonTempus1.AutoSize = true;
            this.radioButtonTempus1.Checked = true;
            this.radioButtonTempus1.ForeColor = System.Drawing.Color.Red;
            this.radioButtonTempus1.Location = new System.Drawing.Point(62, 3);
            this.radioButtonTempus1.Name = "radioButtonTempus1";
            this.radioButtonTempus1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonTempus1.Size = new System.Drawing.Size(57, 16);
            this.radioButtonTempus1.TabIndex = 0;
            this.radioButtonTempus1.TabStop = true;
            this.radioButtonTempus1.Text = "TPS 1";
            this.radioButtonTempus1.UseVisualStyleBackColor = true;
            // 
            // textBoxAmb
            // 
            this.textBoxAmb.Location = new System.Drawing.Point(476, 150);
            this.textBoxAmb.Name = "textBoxAmb";
            this.textBoxAmb.Size = new System.Drawing.Size(46, 21);
            this.textBoxAmb.TabIndex = 9;
            this.textBoxAmb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxRh
            // 
            this.textBoxRh.Location = new System.Drawing.Point(561, 150);
            this.textBoxRh.Name = "textBoxRh";
            this.textBoxRh.Size = new System.Drawing.Size(48, 21);
            this.textBoxRh.TabIndex = 9;
            this.textBoxRh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(524, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "Amb";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(611, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "RH";
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // textBoxRef
            // 
            this.textBoxRef.Location = new System.Drawing.Point(10, 151);
            this.textBoxRef.Name = "textBoxRef";
            this.textBoxRef.Size = new System.Drawing.Size(58, 21);
            this.textBoxRef.TabIndex = 10;
            this.textBoxRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "K30";
            // 
            // textBoxWinPpm
            // 
            this.textBoxWinPpm.Location = new System.Drawing.Point(101, 152);
            this.textBoxWinPpm.Name = "textBoxWinPpm";
            this.textBoxWinPpm.Size = new System.Drawing.Size(58, 21);
            this.textBoxWinPpm.TabIndex = 11;
            this.textBoxWinPpm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(161, 156);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 12);
            this.label12.TabIndex = 5;
            this.label12.Text = "Win";
            // 
            // textBoxCubic
            // 
            this.textBoxCubic.Location = new System.Drawing.Point(189, 151);
            this.textBoxCubic.Name = "textBoxCubic";
            this.textBoxCubic.Size = new System.Drawing.Size(58, 21);
            this.textBoxCubic.TabIndex = 13;
            this.textBoxCubic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(248, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 12);
            this.label14.TabIndex = 12;
            this.label14.Text = "Cubic";
            // 
            // textBoxElt
            // 
            this.textBoxElt.Location = new System.Drawing.Point(288, 151);
            this.textBoxElt.Name = "textBoxElt";
            this.textBoxElt.Size = new System.Drawing.Size(58, 21);
            this.textBoxElt.TabIndex = 15;
            this.textBoxElt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(348, 156);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 12);
            this.label15.TabIndex = 14;
            this.label15.Text = "Elt";
            // 
            // textBoxFigaro
            // 
            this.textBoxFigaro.Location = new System.Drawing.Point(371, 151);
            this.textBoxFigaro.Name = "textBoxFigaro";
            this.textBoxFigaro.Size = new System.Drawing.Size(58, 21);
            this.textBoxFigaro.TabIndex = 17;
            this.textBoxFigaro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(432, 156);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 12);
            this.label16.TabIndex = 16;
            this.label16.Text = "Figaro";
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.button2);
            this.groupBox32.Controls.Add(this.textBoxDrOffset);
            this.groupBox32.Controls.Add(this.textBoxDcOffset);
            this.groupBox32.Controls.Add(this.label24);
            this.groupBox32.Controls.Add(this.label22);
            this.groupBox32.Location = new System.Drawing.Point(736, 144);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(238, 56);
            this.groupBox32.TabIndex = 18;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "dC/dR Default Setting";
            this.toolTip1.SetToolTip(this.groupBox32, "dC/dR offset value. Set current sC/sR + offset");
            // 
            // textBoxDcOffset
            // 
            this.textBoxDcOffset.Location = new System.Drawing.Point(7, 32);
            this.textBoxDcOffset.Name = "textBoxDcOffset";
            this.textBoxDcOffset.Size = new System.Drawing.Size(80, 21);
            this.textBoxDcOffset.TabIndex = 0;
            this.textBoxDcOffset.Text = "30000";
            this.textBoxDcOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDrOffset
            // 
            this.textBoxDrOffset.Location = new System.Drawing.Point(101, 32);
            this.textBoxDrOffset.Name = "textBoxDrOffset";
            this.textBoxDrOffset.Size = new System.Drawing.Size(80, 21);
            this.textBoxDrOffset.TabIndex = 0;
            this.textBoxDrOffset.Text = "0";
            this.textBoxDrOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(190, 30);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(42, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Sync";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(22, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 12);
            this.label22.TabIndex = 14;
            this.label22.Text = "dC offset";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(114, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 12);
            this.label24.TabIndex = 14;
            this.label24.Text = "dR offset";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 678);
            this.Controls.Add(this.groupBox32);
            this.Controls.Add(this.buttonCapture);
            this.Controls.Add(this.textBoxFigaro);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBoxElt);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxCubic);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxWinPpm);
            this.Controls.Add(this.textBoxRef);
            this.Controls.Add(this.textBoxRh);
            this.Controls.Add(this.textBoxAmb);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "CO2 CAL Protocol Tool V1.8.4";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCO2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartDcDr)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCurv)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartDispCurv)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBoxDetectorId.ResumeLayout(false);
            this.groupBoxDetectorId.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartCO2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton radioButtonTempus4;
		private System.Windows.Forms.RadioButton radioButtonTempus2;
		private System.Windows.Forms.RadioButton radioButtonTempus3;
		private System.Windows.Forms.RadioButton radioButtonTempus1;
		private System.Windows.Forms.Button buttonSync;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.TextBox textBoxDiff4;
		private System.Windows.Forms.TextBox textBoxDiff3;
		private System.Windows.Forms.TextBox textBoxDiff2;
		private System.Windows.Forms.TextBox textBoxDiff1;
		private System.Windows.Forms.CheckBox checkBoxChart4;
		private System.Windows.Forms.CheckBox checkBoxChart3;
		private System.Windows.Forms.CheckBox checkBoxChart2;
		private System.Windows.Forms.CheckBox checkBoxChart1;
		private System.Windows.Forms.TextBox textBoxAmb;
		private System.Windows.Forms.TextBox textBoxRh;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.CheckBox checkBoxModule4;
		private System.Windows.Forms.CheckBox checkBoxModule3;
		private System.Windows.Forms.CheckBox checkBoxModule2;
		private System.Windows.Forms.CheckBox checkBoxModule1;
		private System.Windows.Forms.RichTextBox richTextBoxLog;
		private System.Windows.Forms.Button buttonCom;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button buttonTempus4ComClose;
		private System.Windows.Forms.ComboBox comboBoxTps4;
		private System.Windows.Forms.Button buttonTempus4ComOpen;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button buttonTempus3ComClose;
		private System.Windows.Forms.ComboBox comboBoxTps3;
		private System.Windows.Forms.Button buttonTempus3ComOpen;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button buttonTempus2ComClose;
		private System.Windows.Forms.ComboBox comboBoxTps2;
		private System.Windows.Forms.Button buttonTempus2ComOpen;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button buttonTempus1ComClose;
		private System.Windows.Forms.ComboBox comboBoxTps1;
		private System.Windows.Forms.Button buttonTempus1ComOpen;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonRefComClose;
		private System.Windows.Forms.ComboBox comboBoxRef;
		private System.Windows.Forms.Button buttonRefComOpen;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.IO.Ports.SerialPort serialPort1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonLogClear;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.CheckBox checkBoxErr;
		private System.Windows.Forms.CheckBox checkBoxInfo;
		private System.Windows.Forms.CheckBox checkBoxLog;
		private System.Windows.Forms.TextBox textBoxRef;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Button buttonExtComClose;
		private System.Windows.Forms.ComboBox comboBoxExt;
		private System.Windows.Forms.Button buttonExtComOpen;
		private System.Windows.Forms.CheckBox checkBoxRef;
		private System.IO.Ports.SerialPort serialPort2;
		private System.IO.Ports.SerialPort serialPort3;
		private System.IO.Ports.SerialPort serialPort4;
		private System.IO.Ports.SerialPort serialPort5;
		private System.Windows.Forms.Button buttonCmdDbgInfo;
		private System.Windows.Forms.Button buttonCmdCalEn;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.Button buttonDcDrClear;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartDcDr;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Button buttonCurvClear;
		private System.Windows.Forms.Button buttonCurv;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartCurv;
		private System.Windows.Forms.CheckBox checkBoxDcDr1;
		private System.Windows.Forms.CheckBox checkBoxDcDr4;
		private System.Windows.Forms.CheckBox checkBoxDcDr3;
		private System.Windows.Forms.CheckBox checkBoxDcDr2;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.TextBox textBoxVer4;
		private System.Windows.Forms.TextBox textBoxVer3;
		private System.Windows.Forms.TextBox textBoxVer2;
		private System.Windows.Forms.TextBox textBoxVer1;
		private System.Windows.Forms.Button buttonVer;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox textBoxSn4;
        private System.Windows.Forms.TextBox textBoxSn3;
        private System.Windows.Forms.TextBox textBoxSn2;
        private System.Windows.Forms.TextBox textBoxSn1;
        private System.Windows.Forms.Button buttonSn;
        private System.Windows.Forms.Button buttonSnSet;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox textBoxAlarm4;
        private System.Windows.Forms.TextBox textBoxAlarm3;
        private System.Windows.Forms.TextBox textBoxAlarm2;
        private System.Windows.Forms.TextBox textBoxAlarm1;
        private System.Windows.Forms.Button buttonAlarmSet;
        private System.Windows.Forms.Button buttonAlarmGet;
        private System.Windows.Forms.Button buttonDefault;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button buttonWinsenComClose;
        private System.Windows.Forms.ComboBox comboBoxWinsen;
        private System.Windows.Forms.Button buttonWinsenComOpen;
        private System.Windows.Forms.TextBox textBoxWinPpm;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxDiff5;
        private System.Windows.Forms.CheckBox checkBoxWinsen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxRefOffset;
        private System.Windows.Forms.CheckBox checkBoxFigaro;
        private System.Windows.Forms.CheckBox checkBoxElt;
        private System.Windows.Forms.CheckBox checkBoxCubic;
        private System.Windows.Forms.CheckBox checkBoxSenseAir;
        private System.Windows.Forms.TextBox textBoxCubic;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxElt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxFigaro;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button buttonFigaroComClose;
        private System.Windows.Forms.ComboBox comboBoxFigaro;
        private System.Windows.Forms.Button buttonFigaroComOpen;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button buttonEltComClose;
        private System.Windows.Forms.ComboBox comboBoxElt;
        private System.Windows.Forms.Button buttonEltComOpen;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button buttonCubicComClose;
        private System.Windows.Forms.ComboBox comboBoxCubic;
        private System.Windows.Forms.Button buttonCubicComOpen;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton radioButton5pro;
        private System.Windows.Forms.RadioButton radioButton3pro;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton radioButtonRefTpsAvg;
        private System.Windows.Forms.RadioButton radioButtonRefFigaro;
        private System.Windows.Forms.RadioButton radioButtonRefElt;
        private System.Windows.Forms.RadioButton radioButtonRefWinsen;
        private System.Windows.Forms.RadioButton radioButtonRefT360;
        private System.Windows.Forms.RadioButton radioButtonRefCubic;
        private System.Windows.Forms.RadioButton radioButtonRefSenseAir;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxEtcPro;
        private System.Windows.Forms.RadioButton radioButtonEtc;
        private System.Windows.Forms.Button buttonCapture;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Button buttonZmdiGet;
        private System.Windows.Forms.Button buttonZmdiSet;
        private System.Windows.Forms.TextBox textBoxZmdiData;
        private System.Windows.Forms.TextBox textBoxZmdiAddr;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox checkBoxCurv4;
        private System.Windows.Forms.CheckBox checkBoxCurv3;
        private System.Windows.Forms.CheckBox checkBoxCurv2;
        private System.Windows.Forms.CheckBox checkBoxCurv1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartDispCurv;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.RadioButton radioButtonTps1Co2CmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps1Co2NorMode;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.RadioButton radioButtonTps4RefCmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps4RefNorMode;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.RadioButton radioButtonTps4Co2CmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps4Co2NorMode;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.RadioButton radioButtonTps3RefCmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps3RefNorMode;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.RadioButton radioButtonTps3Co2CmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps3Co2NorMode;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.RadioButton radioButtonTps2RefCmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps2RefNorMode;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.RadioButton radioButtonTps2Co2CmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps2Co2NorMode;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.RadioButton radioButtonTps1RefCmdMode;
        private System.Windows.Forms.RadioButton radioButtonTps1RefNorMode;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxPcParam;
        private System.Windows.Forms.GroupBox groupBoxDetectorId;
        private System.Windows.Forms.Button buttonDetectorId;
        private System.Windows.Forms.TextBox textBoxDetectorId4;
        private System.Windows.Forms.TextBox textBoxDetectorId3;
        private System.Windows.Forms.TextBox textBoxDetectorId2;
        private System.Windows.Forms.TextBox textBoxDetectorId1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonPcParamGet;
        private System.Windows.Forms.Button buttonPcParamSet;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonGain;
        private System.Windows.Forms.Button buttonTs;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button buttonSr;
        private System.Windows.Forms.Button buttonSc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTcParam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxGain;
        private System.Windows.Forms.TextBox textBoxTs;
        private System.Windows.Forms.TextBox textBoxSr;
        private System.Windows.Forms.TextBox textBoxSc;
        private System.Windows.Forms.Button buttonTcParamGet;
        private System.Windows.Forms.Button buttonTcParamSet;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxDrOffset;
        private System.Windows.Forms.TextBox textBoxDcOffset;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
    }
}

