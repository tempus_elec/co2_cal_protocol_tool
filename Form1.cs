﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			FormClosing += new FormClosingEventHandler(Form1_FormClosing);
			chart_init();
			LogInit();
			//ConfigFileOpen();
			SetupDataGridView();
            PrdTimerStart();
        }

		private void Form1_FormClosing(Object sender, FormClosingEventArgs e)
		{
			CloseLogFiles();
			ConfigFileSave(null);
		}

		private void buttonCom_Click(object sender, EventArgs e)
		{
			UartCom(null);
		}

		private void buttonRefComOpen_Click(object sender, EventArgs e)
		{
			if (comboBoxRef.SelectedItem == null || comboBoxRef.SelectedIndex == -1)
			{
				ERR("Please specify port name\n");
			}
			else
			{
				UartOpenRef(comboBoxRef.SelectedItem.ToString());
			}
		}

		private void buttonTempus1ComOpen_Click(object sender, EventArgs e)
		{
			if (comboBoxTps1.SelectedItem == null || comboBoxTps1.SelectedIndex == -1)
			{
				ERR("Please specify port name\n"); 
			}
			else
			{
				UartOpenTps1(comboBoxTps1.SelectedItem.ToString());
			}
		}

		private void buttonRefComClose_Click(object sender, EventArgs e)
		{
			UartCloseRef();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			PrdTimer();
		}

		private void buttonLogClear_Click(object sender, EventArgs e)
		{
			LOG_Clear();
		}

		private void buttonTempus1ComClose_Click(object sender, EventArgs e)
		{
			UartCloseTps1();
		}

		private void buttonCmdCalEn_Click(object sender, EventArgs e)
		{
			SerialPort port = null;

			if (radioButtonTempus1.Checked) port = sPort_Tempus1;
			else if (radioButtonTempus2.Checked) port = sPort_Tempus2;
			else if (radioButtonTempus3.Checked) port = sPort_Tempus3;
			else if (radioButtonTempus4.Checked) port = sPort_Tempus4;

			if (calMode)
			{
				calMode = false;
				bsp_calibration_ctrl(port, false);
				buttonCmdCalEn.Text = "CAL Dis";
			}
			else
			{
				bsp_calibration_ctrl(port, true);
				calMode = true;
				buttonCmdCalEn.Text = "CAL En";
			}
		}

		private void buttonCmdDbgInfo_Click(object sender, EventArgs e)
		{
			SerialPort port = null;

			if (radioButtonTempus1.Checked)			port = sPort_Tempus1;
			else if (radioButtonTempus2.Checked)	port = sPort_Tempus2;
			else if (radioButtonTempus3.Checked)	port = sPort_Tempus3;
			else if (radioButtonTempus4.Checked)	port = sPort_Tempus4;

			bsp_get_dbgInfo(port);
		}

		private void buttonSc_Click(object sender, EventArgs e)
		{
			SerialPort port = null;

			if (radioButtonTempus1.Checked) port = sPort_Tempus1;
			else if (radioButtonTempus2.Checked) port = sPort_Tempus2;
			else if (radioButtonTempus3.Checked) port = sPort_Tempus3;
			else if (radioButtonTempus4.Checked) port = sPort_Tempus4;

			bsp_set_sc(port);
		}

		private void buttonTs_Click(object sender, EventArgs e)
		{
			SerialPort port = null;

			if (radioButtonTempus1.Checked) port = sPort_Tempus1;
			else if (radioButtonTempus2.Checked) port = sPort_Tempus2;
			else if (radioButtonTempus3.Checked) port = sPort_Tempus3;
			else if (radioButtonTempus4.Checked) port = sPort_Tempus4;

			bsp_set_ts(port);
		}

		private void buttonGain_Click(object sender, EventArgs e)
		{
			SerialPort port = null;

			if (radioButtonTempus1.Checked) port = sPort_Tempus1;
			else if (radioButtonTempus2.Checked) port = sPort_Tempus2;
			else if (radioButtonTempus3.Checked) port = sPort_Tempus3;
			else if (radioButtonTempus4.Checked) port = sPort_Tempus4;

			bsp_set_gain(port);
		}

		private void buttonSr_Click(object sender, EventArgs e)
		{
			SerialPort port = null;

			if (radioButtonTempus1.Checked) port = sPort_Tempus1;
			else if (radioButtonTempus2.Checked) port = sPort_Tempus2;
			else if (radioButtonTempus3.Checked) port = sPort_Tempus3;
			else if (radioButtonTempus4.Checked) port = sPort_Tempus4;

			bsp_set_sr(port);
		}

		private void buttonSync_Click(object sender, EventArgs e)
		{
			bsp_update_value();
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			chart_clear();
		}

		private void buttonDcDrClear_Click(object sender, EventArgs e)
		{
			chart_dcdr_clear();
		}

		private void buttonTempus2ComOpen_Click(object sender, EventArgs e)
		{
			if (comboBoxTps2.SelectedItem == null || comboBoxTps2.SelectedIndex == -1)
			{
				ERR("Please specify port name\n");
			}
			else
			{
				UartOpenTps2(comboBoxTps2.SelectedItem.ToString());
			}
		}

		private void buttonTempus2ComClose_Click(object sender, EventArgs e)
		{
			UartCloseTps2();
		}

		private void buttonTempus3ComOpen_Click(object sender, EventArgs e)
		{
			if (comboBoxTps3.SelectedItem == null || comboBoxTps3.SelectedIndex == -1)
			{
				ERR("Please specify port name\n");
			}
			else
			{
				UartOpenTps3(comboBoxTps3.SelectedItem.ToString());
			}
		}

		private void buttonTempus3ComClose_Click(object sender, EventArgs e)
		{
			UartCloseTps3();
		}

		private void buttonTempus4ComOpen_Click(object sender, EventArgs e)
		{
			if (comboBoxTps4.SelectedItem == null || comboBoxTps4.SelectedIndex == -1)
			{
				ERR("Please specify port name\n");
			}
			else
			{
				UartOpenTps4(comboBoxTps4.SelectedItem.ToString());
			}
		}

		private void buttonTempus4ComClose_Click(object sender, EventArgs e)
		{
			UartCloseTps4();
		}

		private void buttonExtComOpen_Click(object sender, EventArgs e)
		{
			if (comboBoxExt.SelectedItem == null || comboBoxExt.SelectedIndex == -1)
			{
				ERR("Please specify port name\n");
			}
			else
			{
				UartOpenExt(comboBoxExt.SelectedItem.ToString());
			}
		}

		private void buttonExtComClose_Click(object sender, EventArgs e)
		{
			UartCloseExt();
		}

		private void buttonDetectorId_Click(object sender, EventArgs e)
		{
			if (checkBoxModule1.Checked) bsp_get_detector_id(sPort_Tempus1);
			if (checkBoxModule2.Checked) bsp_get_detector_id(sPort_Tempus2);
			if (checkBoxModule3.Checked) bsp_get_detector_id(sPort_Tempus3);
			if (checkBoxModule4.Checked) bsp_get_detector_id(sPort_Tempus4);
		}

		private void buttonVer_Click(object sender, EventArgs e)
		{
			if (checkBoxModule1.Checked) bsp_get_version(sPort_Tempus1);
			if (checkBoxModule2.Checked) bsp_get_version(sPort_Tempus2);
			if (checkBoxModule3.Checked) bsp_get_version(sPort_Tempus3);
			if (checkBoxModule4.Checked) bsp_get_version(sPort_Tempus4);
		}

        private void buttonSn_Click(object sender, EventArgs e)
        {
            if (checkBoxModule1.Checked) bsp_get_serial(sPort_Tempus1);
            if (checkBoxModule2.Checked) bsp_get_serial(sPort_Tempus2);
            if (checkBoxModule3.Checked) bsp_get_serial(sPort_Tempus3);
            if (checkBoxModule4.Checked) bsp_get_serial(sPort_Tempus4);
        }

        private void buttonSnSet_Click(object sender, EventArgs e)
        {
            if (checkBoxModule1.Checked) bsp_set_serial(sPort_Tempus1, textBoxSn1.Text);
            if (checkBoxModule2.Checked) bsp_set_serial(sPort_Tempus2, textBoxSn2.Text);
            if (checkBoxModule3.Checked) bsp_set_serial(sPort_Tempus3, textBoxSn3.Text);
            if (checkBoxModule4.Checked) bsp_set_serial(sPort_Tempus4, textBoxSn4.Text);
        }

        private void buttonAlarmGet_Click(object sender, EventArgs e)
        {
            if (checkBoxModule1.Checked) bsp_get_alarm(sPort_Tempus1);
            if (checkBoxModule2.Checked) bsp_get_alarm(sPort_Tempus2);
            if (checkBoxModule3.Checked) bsp_get_alarm(sPort_Tempus3);
            if (checkBoxModule4.Checked) bsp_get_alarm(sPort_Tempus4);
        }

        private void buttonAlarmSet_Click(object sender, EventArgs e)
        {
            if (checkBoxModule1.Checked) bsp_set_alarm(sPort_Tempus1, textBoxAlarm1.Text);
            if (checkBoxModule2.Checked) bsp_set_alarm(sPort_Tempus2, textBoxAlarm2.Text);
            if (checkBoxModule3.Checked) bsp_set_alarm(sPort_Tempus3, textBoxAlarm3.Text);
            if (checkBoxModule4.Checked) bsp_set_alarm(sPort_Tempus4, textBoxAlarm4.Text);
        }

        private void buttonDefault_Click(object sender, EventArgs e)
        {
            ConfigFileOpen();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            PrdTimerStart();
        }

        private void buttonWinsenComOpen_Click(object sender, EventArgs e)
        {
            if (comboBoxWinsen.SelectedItem == null || comboBoxWinsen.SelectedIndex == -1)
            {
                ERR("Please specify port name\n");
            }
            else
            {
                UartOpenWinsen(comboBoxWinsen.SelectedItem.ToString());
            }
        }

        private void buttonWinsenComClose_Click(object sender, EventArgs e)
        {
            UartCloseWinsen();
        }

        private void buttonCubicOpen_Click(object sender, EventArgs e)
        {
            UartOpenCubic(comboBoxCubic.SelectedItem.ToString());
        }

        private void buttonCubicClose_Click(object sender, EventArgs e)
        {
            UartCloseCubic();
        }

        private void buttonEltComOpen_Click(object sender, EventArgs e)
        {
            UartOpenElt(comboBoxElt.SelectedItem.ToString());
        }

        private void buttonEltComClose_Click(object sender, EventArgs e)
        {
            UartCloseElt();
        }

        private void buttonFigaroComOpen_Click(object sender, EventArgs e)
        {
            UartOpenFigaro(comboBoxFigaro.SelectedItem.ToString());
        }

        private void buttonFigaroClose_Click(object sender, EventArgs e)
        {
            UartCloseFigaro();
        }

        private void buttonZmdiSet_Click(object sender, EventArgs e)
        {
            if (checkBoxModule1.Checked) bsp_set_zmdi_addr(sPort_Tempus1);
            if (checkBoxModule2.Checked) bsp_set_zmdi_addr(sPort_Tempus2);
            if (checkBoxModule3.Checked) bsp_set_zmdi_addr(sPort_Tempus3);
            if (checkBoxModule4.Checked) bsp_set_zmdi_addr(sPort_Tempus4);
        }

        private void buttonZmdiGet_Click(object sender, EventArgs e)
        {
            if (checkBoxModule1.Checked) bsp_get_zmdi_addr(sPort_Tempus1);
            if (checkBoxModule2.Checked) bsp_get_zmdi_addr(sPort_Tempus2);
            if (checkBoxModule3.Checked) bsp_get_zmdi_addr(sPort_Tempus3);
            if (checkBoxModule4.Checked) bsp_get_zmdi_addr(sPort_Tempus4);
        }

        private void buttonCapture_Click(object sender, EventArgs e)
        {
            ScreenCapture();
        }

        private void buttonCurvClear_Click(object sender, EventArgs e)
        {
            chart_a0_ih_clear();
        }

        private void buttonCurv_Click(object sender, EventArgs e)
        {
#if false
            if (checkBoxModule1.Checked) bsp_get_curv(sPort_Tempus1);
            if (checkBoxModule2.Checked) bsp_get_curv(sPort_Tempus2);
            if (checkBoxModule3.Checked) bsp_get_curv(sPort_Tempus3);
            if (checkBoxModule4.Checked) bsp_get_curv(sPort_Tempus4);
#endif
        }

        private void button1_Click(object sender, EventArgs e)
        {
            chart_curv_clear();
        }

        private void buttonPcParamSet_Click(object sender, EventArgs e)
        {
            if (radioButtonTempus1.Checked) bsp_set_pwrComp_param(sPort_Tempus1);
            if (radioButtonTempus2.Checked) bsp_set_pwrComp_param(sPort_Tempus2);
            if (radioButtonTempus3.Checked) bsp_set_pwrComp_param(sPort_Tempus3);
            if (radioButtonTempus4.Checked) bsp_set_pwrComp_param(sPort_Tempus4);
        }

        private void buttonPcParamGet_Click(object sender, EventArgs e)
        {
            if (radioButtonTempus1.Checked) bsp_get_pwrComp_param(sPort_Tempus1);
            if (radioButtonTempus2.Checked) bsp_get_pwrComp_param(sPort_Tempus2);
            if (radioButtonTempus3.Checked) bsp_get_pwrComp_param(sPort_Tempus3);
            if (radioButtonTempus4.Checked) bsp_get_pwrComp_param(sPort_Tempus4);
        }

        private void buttonTcParamSet_Click(object sender, EventArgs e)
        {
            if (radioButtonTempus1.Checked) bsp_set_tempComp_param(sPort_Tempus1);
            if (radioButtonTempus2.Checked) bsp_set_tempComp_param(sPort_Tempus2);
            if (radioButtonTempus3.Checked) bsp_set_tempComp_param(sPort_Tempus3);
            if (radioButtonTempus4.Checked) bsp_set_tempComp_param(sPort_Tempus4);
        }

        private void buttonTcParamGet_Click(object sender, EventArgs e)
        {
            if (radioButtonTempus1.Checked) bsp_get_tempComp_param(sPort_Tempus1);
            if (radioButtonTempus2.Checked) bsp_get_tempComp_param(sPort_Tempus2);
            if (radioButtonTempus3.Checked) bsp_get_tempComp_param(sPort_Tempus3);
            if (radioButtonTempus4.Checked) bsp_get_tempComp_param(sPort_Tempus4);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bsp_update_dCdR_value();
        }
    }
}
