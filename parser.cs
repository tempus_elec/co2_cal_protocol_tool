﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		const int MAX_MODULE_NUM = 4;

		public struct DbgInfo_ST
		{
			public byte st0;
			public byte nt0;
			public byte st1;
			public byte nt1;
			public UInt32 dC;
			public UInt32 sC;
			public UInt32 dR;
			public UInt32 sR;
			public UInt16 adc;
			public Int32 ratio;
			public UInt16 ppm;
			public int temp;
			public int ts;
			public UInt32 gain;
#if false
            public double CAL_A0;
            public double CAL_Ih;
            public double REF_A0;
            public double REF_Ih;
#endif
            public byte status_cal;
            public byte status_ref;

            //public DbgInfo_ST(byte st0, byte nt0, byte st1, byte nt1, UInt32 dC, UInt32 sC, UInt32 dR, UInt32 sR, UInt16 adc, Int32 ratio, UInt16 ppm, int temp, int ts, UInt32 gain, double CAL_A0, double CAL_Ih, double REF_A0, double REF_Ih, byte status_cal, byte status_ref)
            public DbgInfo_ST(byte st0, byte nt0, byte st1, byte nt1, UInt32 dC, UInt32 sC, UInt32 dR, UInt32 sR, UInt16 adc, Int32 ratio, UInt16 ppm, int temp, int ts, UInt32 gain, byte status_cal, byte status_ref)
            {
				this.st0 = st0;
				this.nt0 = nt0;
				this.st1 = st1;
				this.nt1 = nt1;
				this.dC = dC;
				this.sC = sC;
				this.dR = dR;
				this.sR = sR;
				this.adc = adc;
				this.ratio = ratio;
				this.ppm = ppm;
				this.temp = temp;
				this.ts = ts;
				this.gain = gain;

#if false
                this.CAL_A0 = CAL_A0;
                this.CAL_Ih = CAL_Ih;
                this.REF_A0 = REF_A0;
                this.REF_Ih = REF_Ih;
#endif
                this.status_cal = status_cal;
                this.status_ref = status_ref;
            }
		}

		DbgInfo_ST[] dbgInfo = new DbgInfo_ST[]
		{
			new DbgInfo_ST(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			new DbgInfo_ST(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			new DbgInfo_ST(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			new DbgInfo_ST(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		};

		string[][] DbgInfo = new string[][] {   new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
												new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
												new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
												new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""}
		};

		private void DbgInfoParser(byte[] buff, byte moduleNum)
		{
			byte offset = 4;
			UInt32 ratioH, ratioL;
            byte[] temp_buff = new byte[4];

			try
			{
			    // parse dbginfo
			    dbgInfo[moduleNum].st0 = buff[offset++];
			    dbgInfo[moduleNum].nt0 = buff[offset++];
			    dbgInfo[moduleNum].st1 = buff[offset++];
			    dbgInfo[moduleNum].nt1 = buff[offset++];

				dbgInfo[moduleNum].dC = (UInt32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
			    offset += 4;
			    dbgInfo[moduleNum].sC = (UInt32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
			    offset += 4;
			    dbgInfo[moduleNum].dR = (UInt32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
			    offset += 4;
			    dbgInfo[moduleNum].sR = (UInt32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
			    offset += 4;
			    dbgInfo[moduleNum].adc = (UInt16)(buff[offset] | (buff[offset + 1] << 8));
			    offset += 2;
			    dbgInfo[moduleNum].ratio = (Int32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
			    ratioH = buff[offset + 3];
			    ratioL = (UInt32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16));
			    offset += 4;
			    dbgInfo[moduleNum].ppm = (UInt16)(buff[offset] | (buff[offset + 1] << 8));
			    offset += 2;

			    dbgInfo[moduleNum].temp = buff[offset++];
			    dbgInfo[moduleNum].ts = buff[offset++];

			    dbgInfo[moduleNum].gain = (UInt32)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
                offset += 4;
#if false
                dbgInfo[moduleNum].CAL_A0 = (double)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
                offset += 4;

                dbgInfo[moduleNum].CAL_Ih = (double)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
                offset += 4;

                dbgInfo[moduleNum].REF_A0 = (double)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
                offset += 4;

                dbgInfo[moduleNum].REF_Ih = (double)(buff[offset] | (buff[offset + 1] << 8) | (buff[offset + 2] << 16) | (buff[offset + 3] << 24));
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                offset = 0;
#endif
                dbgInfo[moduleNum].status_cal = buff[offset++];
                dbgInfo[moduleNum].status_ref = buff[offset++];
#if false
                DbgInfo[moduleNum][offset++] = now.ToString("yyyy-MM-dd-H-mm-ss");
#else
                offset = 0;
                DbgInfo[moduleNum][offset++] = now.ToString("H:mm:ss");
#endif
                DbgInfo[moduleNum][offset++] = (moduleNum + 1).ToString();
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].st0);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].nt0);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].st1);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].nt1);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].dC);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].sC);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].dR);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].sR);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].adc);
			    //DbgInfo[moduleNum][offset++] = Convert.ToString(ratioH) + "." + Convert.ToString(ratioL);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(ratioH) + "." + ratioL.ToString("D5");
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].ppm);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].temp);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].ts);
			    DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].gain);
                DbgInfo[moduleNum][offset++] = Convert.ToString(winsen_ppm);
#if false
                DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].CAL_A0);
                DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].CAL_Ih);
                DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].REF_A0);
                DbgInfo[moduleNum][offset++] = Convert.ToString(dbgInfo[moduleNum].REF_Ih);
#else
                //DbgInfo[moduleNum][offset++] = "0x" + Convert.ToString(dbgInfo[moduleNum].status_cal.ToString("X2"));
                //DbgInfo[moduleNum][offset++] = "0x" + Convert.ToString(dbgInfo[moduleNum].status_ref.ToString("X2"));
                //StatusFlagUpdate(dbgInfo[moduleNum].status_cal, dbgInfo[moduleNum].status_ref, moduleNum);
#endif
            }
			catch(Exception ex)
			{
				ERR(ex.Message);
			}
		}

		private void DetectorIdParser(byte[] buff, byte moduleNum)
		{
			byte offset = 4;
			UInt16 id = 0;

			//id = (UInt16)(buff[offset] | (buff[offset + 1] << 8));
			id = (UInt16)(buff[offset + 1] | (buff[offset] << 8));

			switch (moduleNum)
			{
				case 0:
					if (textBoxDetectorId1.InvokeRequired)
					{
						textBoxDetectorId1.Invoke(new MethodInvoker(delegate ()
						{
							textBoxDetectorId1.Text = id.ToString();
						}));
					}
					else
					{
						textBoxDetectorId1.Text = id.ToString();
					}
					break;

				case 1:
					if (textBoxDetectorId2.InvokeRequired)
					{
						textBoxDetectorId2.Invoke(new MethodInvoker(delegate ()
						{
							textBoxDetectorId2.Text = id.ToString();
						}));
					}
					else
					{
						textBoxDetectorId2.Text = id.ToString();
					}
					break;

				case 2:
					if (textBoxDetectorId3.InvokeRequired)
					{
						textBoxDetectorId3.Invoke(new MethodInvoker(delegate ()
						{
							textBoxDetectorId3.Text = id.ToString();
						}));
					}
					else
					{
						textBoxDetectorId3.Text = id.ToString();
					}
					break;

				case 3:
					if (textBoxDetectorId4.InvokeRequired)
					{
						textBoxDetectorId4.Invoke(new MethodInvoker(delegate ()
						{
							textBoxDetectorId4.Text = id.ToString();
						}));
					}
					else
					{
						textBoxDetectorId4.Text = id.ToString();
					}
					break;

				default:
					break;
			}

		}

		private void FwVerParser(byte[] buff, byte moduleNum)
		{
			byte offset = 4;
			string ver = null;

			//id = (UInt16)(buff[offset] | (buff[offset + 1] << 8));
			ver = buff[offset].ToString() + "." + buff[offset + 1].ToString() + "." + buff[offset + 2].ToString();

			switch (moduleNum)
			{
				case 0:
					if (textBoxVer1.InvokeRequired)
					{
						textBoxVer1.Invoke(new MethodInvoker(delegate ()
						{
							textBoxVer1.Text = ver;
						}));
					}
					else
					{
						textBoxVer1.Text = ver;
					}
					break;

				case 1:
					if (textBoxVer2.InvokeRequired)
					{
						textBoxVer2.Invoke(new MethodInvoker(delegate ()
						{
							textBoxVer2.Text = ver;
						}));
					}
					else
					{
						textBoxVer2.Text = ver;
					}
					break;

				case 2:
					if (textBoxVer3.InvokeRequired)
					{
						textBoxVer3.Invoke(new MethodInvoker(delegate ()
						{
							textBoxVer3.Text = ver;
						}));
					}
					else
					{
						textBoxVer3.Text = ver;
					}
					break;

				case 3:
					if (textBoxVer4.InvokeRequired)
					{
						textBoxVer4.Invoke(new MethodInvoker(delegate ()
						{
							textBoxVer4.Text = ver;
						}));
					}
					else
					{
						textBoxVer4.Text = ver;
					}
					break;

				default:
					break;
			}

		}

        private void SerialNumberParser(byte[] buff, byte moduleNum)
        {
            byte offset = 4;
            string sn = null;

            sn = buff[offset].ToString();

            switch (moduleNum)
            {
                case 0:
                    if (textBoxSn1.InvokeRequired)
                    {
                        textBoxSn1.Invoke(new MethodInvoker(delegate (){ textBoxSn1.Text = sn; }));
                    }
                    else
                    {
                        textBoxSn1.Text = sn;
                    }
                    break;

                case 1:
                    if (textBoxSn2.InvokeRequired)
                    {
                        textBoxSn2.Invoke(new MethodInvoker(delegate () { textBoxSn2.Text = sn; }));
                    }
                    else
                    {
                        textBoxSn2.Text = sn;
                    }
                    break;

                case 2:
                    if (textBoxSn3.InvokeRequired)
                    {
                        textBoxSn3.Invoke(new MethodInvoker(delegate () { textBoxSn3.Text = sn; }));
                    }
                    else
                    {
                        textBoxSn3.Text = sn;
                    }
                    break;

                case 3:
                    if (textBoxSn4.InvokeRequired)
                    {
                        textBoxSn4.Invoke(new MethodInvoker(delegate () { textBoxSn4.Text = sn; }));
                    }
                    else
                    {
                        textBoxSn4.Text = sn;
                    }
                    break;

                default:
                    break;
            }
        }

        private void AlarmParser(byte[] buff, byte moduleNum)
        {
            byte offset = 4;
            //string alarm = buff[offset].ToString();
            string alarm = (buff[offset] | buff[offset + 1]<<8).ToString();

            switch (moduleNum)
            {
                case 0:
                    if (textBoxAlarm1.InvokeRequired)
                    {
                        textBoxAlarm1.Invoke(new MethodInvoker(delegate () { textBoxAlarm1.Text = alarm; }));
                    }
                    else
                    {
                        textBoxAlarm1.Text = alarm;
                    }
                    break;

                case 1:
                    if (textBoxAlarm2.InvokeRequired)
                    {
                        textBoxAlarm2.Invoke(new MethodInvoker(delegate () { textBoxAlarm2.Text = alarm; }));
                    }
                    else
                    {
                        textBoxAlarm2.Text = alarm;
                    }
                    break;

                case 2:
                    if (textBoxAlarm3.InvokeRequired)
                    {
                        textBoxAlarm3.Invoke(new MethodInvoker(delegate () { textBoxAlarm3.Text = alarm; }));
                    }
                    else
                    {
                        textBoxAlarm3.Text = alarm;
                    }
                    break;

                case 3:
                    if (textBoxAlarm4.InvokeRequired)
                    {
                        textBoxAlarm4.Invoke(new MethodInvoker(delegate () { textBoxAlarm4.Text = alarm; }));
                    }
                    else
                    {
                        textBoxAlarm4.Text = alarm;
                    }
                    break;

                default:
                    break;
            }
        }

        private void ZmdiParser(byte[] buff, byte moduleNum)
        {
            byte offset = 4;
            //string alarm = buff[offset].ToString();
            //string dat = (buff[offset] | buff[offset + 1] << 8).ToString();
            string dat = buff[offset].ToString("x") + buff[offset + 1].ToString("x");

            switch (moduleNum)
            {
                case 0: TextBoxSet(textBoxZmdiData, dat);
                    break;

                case 1:
                    TextBoxSet(textBoxZmdiData, dat);
                    break;

                case 2:
                    TextBoxSet(textBoxZmdiData, dat);
                    break;

                case 3:
                    TextBoxSet(textBoxZmdiData, dat);
                    break;

                default:
                    break;
            }
        }

        private void PwrCompParserParser(byte[] buff, byte moduleNum)
        {
            byte offset = 4;
            UInt16 value = 0;

            value = (UInt16)(buff[offset] | buff[offset + 1] << 8);

            TextBoxSet(textBoxPcParam, value.ToString());
        }

        private void MsgParser(byte[] buff, byte portNum)
		{
			byte cmd = buff[2];
			
			LOG((portNum+1).ToString() + " ==> CMD : " + cmd + ", Len : " + buff[3] + "\n");

			try
			{
			    switch ((TpsMsg)cmd)
			    {
				    case TpsMsg.BSP_RES_GET_PPM:
					    UInt16 ppm = (UInt16)(buff[5] << 8 | buff[4]);
					    LOG("BSP_RES_GET_PPM : " + ppm + "\n");
					    break;

				    case TpsMsg.BSP_RES_SET_CALIBRATION_MODE:
					    LOG("BSP_RES_SET_CALIBRATION_MODE\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_GET_INFO:
					    LOG("BSP_RES_DBG_GET_INFO\n");
					    DbgInfoParser(buff, portNum);
					    break;

				    case TpsMsg.BSP_RES_GET_VER:
					    LOG("BSP_RES_GET_VER\n");
					    FwVerParser(buff, portNum);
					    break;

				    case TpsMsg.BSP_RES_GET_SER:
					    LOG("BSP_RES_GET_SER\n");
                        SerialNumberParser(buff, portNum);
                        break;

				    case TpsMsg.BSP_RES_GET_ALARM:
					    LOG("BSP_RES_GET_ALARM\n");
                        AlarmParser(buff, portNum);
                        break;

				    case TpsMsg.BSP_RES_SET_ALARM:
					    LOG("BSP_RES_SET_ALARM\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_GET_I2C_ADDR:
					    LOG("BSP_RES_DBG_GET_I2C_ADDR\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_SER:
					    LOG("BSP_RES_DBG_SET_SER\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_SC:
					    LOG("BSP_RES_DBG_SET_SC\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_SR:
					    LOG("BSP_RES_DBG_SET_SR\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_GAIN:
					    LOG("BSP_RES_DBG_SET_GAIN\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_I2C_ADDR:
					    LOG("BSP_RES_DBG_SET_I2C_ADDR\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_TS:
					    LOG("BSP_RES_DBG_SET_TS\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_DBG:
					    LOG("BSP_RES_DBG_SET_DBG\n");
					    break;

				    case TpsMsg.BSP_RES_DBG_SET_CURV:
					    LOG("BSP_RES_DBG_SET_CURV\n");
					    break;

				    case TpsMsg.BSP_RES_GET_DETECTOR_ID:
					    LOG("BSP_RES_GET_DETECTOR_ID\n");
					    DetectorIdParser(buff, portNum);
					    break;

                    case TpsMsg.BSP_RES_DBG_SET_ZMDI:
                        LOG("BSP_RES_DBG_SET_ZMDI\n");
                        break;

                    case TpsMsg.BSP_RES_DBG_GET_ZMDI:
                        LOG("BSP_RES_DBG_GET_ZMDI\n");
                        ZmdiParser(buff, portNum);
                        break;

                    case TpsMsg.BSP_RES_DBG_GET_PC_PARAM:
                        LOG("BSP_RES_DBG_GET_PC_PARAM\n");
                        PwrCompParserParser(buff, portNum);
                        break;

                    case TpsMsg.BSP_RES_DBG_SET_TC_PARAM:
                        LOG("BSP_RES_DBG_SET_TC_PARAM\n");
                        break;

                    case TpsMsg.BSP_RES_DBG_GET_TC_PARAM:
                        LOG("BSP_RES_DBG_GET_TC_PARAM\n");
                        TextBoxSet(textBoxTcParam, (buff[4] | buff[5] << 8).ToString());
                        break;

                    default:
				    break;
			    }
		    }
			catch(Exception ex)
			{
				ERR(ex.Message);
			}
		}

        private void StatusFlagUpdate(byte cal_mode, byte ref_mode, byte moduleNum)
        {
            byte mode = 0;

            if (moduleNum == 0)
            {
                mode = (byte)(cal_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps1Co2NorMode, true);
                    ERR(moduleNum + " CO2 Status Flag Mode..0x" + cal_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps1Co2CmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " CO2 Status Flag Error..0x" + cal_mode.ToString("X2") + "\n");
                }

                mode = (byte)(ref_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps1RefNorMode, true);
                    ERR(moduleNum + " Ref Status Flag Mode..0x" + ref_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps1RefCmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " Ref Status Flag Error..0x" + ref_mode.ToString("X2") + "\n");
                }
            }
            else if (moduleNum == 1)
            {
                mode = (byte)(cal_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps2Co2NorMode, true);
                    ERR(moduleNum + " CO2 Status Flag Mode..0x" + cal_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps2Co2CmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " CO2 Status Flag Error..0x" + cal_mode.ToString("X2") + "\n");
                }

                mode = (byte)(ref_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps2RefNorMode, true);
                    ERR(moduleNum + " Ref Status Flag Mode..0x" + ref_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps2RefCmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " Ref Status Flag Error..0x" + ref_mode.ToString("X2") + "\n");
                }
            }
            else if (moduleNum == 2)
            {
                mode = (byte)(cal_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps3Co2NorMode, true);
                    ERR(moduleNum + " CO2 Status Flag Mode..0x" + cal_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps3Co2CmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " CO2 Status Flag Error..0x" + cal_mode.ToString("X2") + "\n");
                }

                mode = (byte)(ref_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps3RefNorMode, true);
                    ERR(moduleNum + " Ref Status Flag Mode..0x" + ref_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps3RefCmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " Ref Status Flag Error..0x" + ref_mode.ToString("X2") + "\n");
                }
            }
            else if (moduleNum == 3)
            {
                mode = (byte)(cal_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps4Co2NorMode, true);
                    ERR(moduleNum + " CO2 Status Flag Mode..0x" + cal_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps4Co2CmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " CO2 Status Flag Error..0x" + cal_mode.ToString("X2") + "\n");
                }

                mode = (byte)(ref_mode & (byte)0x18);
                if (mode == 0x00)   // normal mode
                {
                    RadioCheckUpdate(radioButtonTps4RefNorMode, true);
                    ERR(moduleNum + " Ref Status Flag Mode..0x" + ref_mode.ToString("X2") + "\n");
                }
                else if (mode == 0x08)     // command mode
                {
                    RadioCheckUpdate(radioButtonTps4RefCmdMode, true);
                }
                else
                {
                    ERR(moduleNum + " Ref Status Flag Error..0x" + ref_mode.ToString("X2") + "\n");
                }
            }
            else
            {
                ERR(CallerName() + " : moduleNum error\n");
            }
        }
	}
}
