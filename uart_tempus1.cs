﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		Mutex mtx = new Mutex();

		const int TEMPUS_MAX_RX_LEN = 60;

		SerialPort sPort_Tempus1 = null;
		const int MAX_UART_RX_BUFF_LEN = 256;

		bool msgFoundSync1 = false;
		byte msgOffset1 = 0;
		byte msgLen1 = 0;
		byte[] msgBuff1 = new byte[MAX_UART_RX_BUFF_LEN];

		void SerialPort_Tempus1_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int intRecSize = sPort_Tempus1.BytesToRead;

			try
			{
				mtx.WaitOne();

                if (intRecSize != 0)
                {
                    byte[] buff = new byte[intRecSize];
                    int iii;

                    iii = sPort_Tempus1.Read(buff, 0, intRecSize);

                    LOG("Got " + intRecSize + "\n");

                    if (intRecSize > TEMPUS_MAX_RX_LEN)
                    {
                        ERR("Rx got too long..." + intRecSize + "\n");
                        msgFoundSync1 = false;
                        msgOffset1 = 0;
                        msgLen1 = 0;
                    }
                    else
                    {
                        try
                        {
                            if (msgFoundSync1)
                            {
                                byte len = (byte)(msgOffset1 + intRecSize);
                                Buffer.BlockCopy(buff, 0, msgBuff1, msgOffset1, intRecSize);
                                msgOffset1 += (byte)intRecSize;

                                if ((msgLen1 == 0) && (len > 4))
                                {
                                    msgLen1 = (byte)(msgBuff1[3] + 6);
                                }

                                if (len == msgLen1)
                                {
                                    MsgParser(msgBuff1, 0);
                                    msgFoundSync1 = false;
                                    msgOffset1 = 0;
                                    msgLen1 = 0;
                                }
                                else if (len > msgLen1)
                                {
                                    LOG("Msg parser error\n");
                                    msgFoundSync1 = false;
                                    msgOffset1 = 0;
                                    msgLen1 = 0;
                                }
                                else if (len < msgLen1)
                                {

                                }
                            }
                            else
                            {
                                if (msgOffset1 == 0)
                                {
                                    if (intRecSize > 1)
                                    {
                                        for (int i = 0; i < intRecSize; i++)
                                        {
                                            if (buff[i] == 0xbb)
                                            {
                                                if (i > intRecSize)
                                                {
                                                    msgOffset1 = (byte)intRecSize;
                                                }
                                                else
                                                {
                                                    if (buff[i + 1] == 0x66)
                                                    {
                                                        msgFoundSync1 = true;
                                                        Buffer.BlockCopy(buff, 0, msgBuff1, 0, intRecSize);
                                                        msgOffset1 += (byte)intRecSize;

                                                        if (msgLen1 == 0)
                                                        {
                                                            if (msgOffset1 >= 4)
                                                            {
                                                                msgLen1 = (byte)(msgBuff1[3] + 6);
                                                            }

                                                            if (msgLen1 == intRecSize)
                                                            {
                                                                MsgParser(msgBuff1, 0);
                                                                msgFoundSync1 = false;
                                                                msgOffset1 = 0;
                                                                msgLen1 = 0;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff1, msgOffset1, intRecSize);
                                        msgOffset1 = (byte)intRecSize;
                                    }
                                }
                                else
                                {
                                    if ((msgOffset1 + intRecSize) > MAX_UART_RX_BUFF_LEN)
                                    {
                                        ERR("UART Offset is too big\n");
                                        msgFoundSync1 = false;
                                        msgOffset1 = 0;
                                        msgLen1 = 0;
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff1, msgOffset1, intRecSize);

                                        if ((msgBuff1[0] == 0xbb) && (msgBuff1[1] == 0x66))
                                        {
                                            msgFoundSync1 = true;
                                        }
                                        msgOffset1 += (byte)intRecSize;

                                        if (msgLen1 == 0)
                                        {
                                            if (msgOffset1 >= 4)
                                            {
                                                msgLen1 = (byte)(msgBuff1[3] + 6);
                                            }

                                            if (msgLen1 == msgOffset1)
                                            {
                                                MsgParser(msgBuff1, 0);
                                                msgFoundSync1 = false;
                                                msgOffset1 = 0;
                                                msgLen1 = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ERR(CallerName() + " : " + ex.Message);
                            msgFoundSync1 = false;
                            msgOffset1 = 0;
                            msgLen1 = 0;
                        }
                    }
                }
                mtx.ReleaseMutex();
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				LOG("uart rx error : " + intRecSize + ", " + ex.Message + '\n');
			}
		}

		private void UartOpenTps1(string comName)
		{
			try
			{
				if (null == sPort_Tempus1)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus1 = new SerialPort();
						sPort_Tempus1.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Tempus1_DataReceived);
						sPort_Tempus1.PortName = comName;
						sPort_Tempus1.BaudRate = 9600;
						sPort_Tempus1.DataBits = (int)8;
						sPort_Tempus1.Parity = Parity.None;
						sPort_Tempus1.StopBits = StopBits.One;
						sPort_Tempus1.ReadTimeout = -1;
						sPort_Tempus1.WriteTimeout = -1;
						sPort_Tempus1.NewLine = "\n";
                        try
                        {
                            sPort_Tempus1.Open();
                        }
                        catch (Exception ex)
                        {
                            UartCloseTps1();
                            ERR(CallerName() + " : " + ex.Message);
                        }
                    }
				}

				if (sPort_Tempus1.IsOpen)
				{
					buttonTempus1ComOpen.Enabled = false;
					buttonTempus1ComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");

					comboBoxTps1.SelectedText = comName;

                    bsp_calibration_ctrl(sPort_Tempus1, true);  // port를 열면 바로 CAL cmd를 보내도록 수정
                    CheckBoxUpdate(checkBoxModule1, true);
                    CheckBoxUpdate(checkBoxChart1, true);
                    //PrdTimerStart();
                    if(timer1.Enabled == false)
                    {
                        timer1.Start();
                        buttonStop.Text = "Mon Stop";
                    }
                }
				else
				{
					if (comboBoxTps1.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonTempus1ComOpen.Enabled = true;
					buttonTempus1ComClose.Enabled = false;
				}
			}
			catch (System.Exception ex)
			{
				UartCloseTps1();
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartCloseTps1()
		{
			try
			{
				if (null != sPort_Tempus1)
				{
					if (sPort_Tempus1.IsOpen)
					{
						sPort_Tempus1.Close();
						sPort_Tempus1.Dispose();
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
					sPort_Tempus1 = null;
				}

                buttonTempus1ComOpen.Enabled = true;
                buttonTempus1ComClose.Enabled = false;
                CheckBoxUpdate(checkBoxChart1, false);
            }
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void request_tempus1()
		{
            LOG("Request TPS1\n");
            bsp_get_dbgInfo(sPort_Tempus1);
		}
	}
}
