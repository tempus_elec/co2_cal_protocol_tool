﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
    public partial class Form1 : Form
    {
        SerialPort sPort_Figaro = null;
        double figaro_ppm = 0;
        int figaroMsgLen = 0;
        byte[] figaroBuf = new byte[9];

        void SerialPort_Figaro_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int intRecSize = sPort_Figaro.BytesToRead;

            mtx.WaitOne();

            try
            {
                if (intRecSize != 0)
                {
                    byte[] buff = new byte[intRecSize];
                    int iii;

                    iii = sPort_Figaro.Read(buff, 0, intRecSize);

                    if ((intRecSize + figaroMsgLen) > 9)
                    {
                        ERR(CallerName() + " : Got wrong size, " + intRecSize);
                        figaroMsgLen = 0;
                        return;
                    }

                    try
                    {
                        Buffer.BlockCopy(buff, 0, figaroBuf, figaroMsgLen, intRecSize);
                        figaroMsgLen += intRecSize;
                        if (figaroMsgLen >= 7)
                        {
                            if ((figaroBuf[0] == 0xFE) && (figaroBuf[1] == 0x44))
                            {
                                figaroMsgLen = 0;
                                figaro_ppm = (figaroBuf[3] << 8) | figaroBuf[4];
                                LOG("Figaro : " + figaro_ppm + " ppm\n");

                                update_figaro_ppm(figaro_ppm.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ERR(CallerName() + " : " + ex.Message);
                        figaroMsgLen = 0;
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)  // CS0168
            {
                LOG("uart rx error : " + ex.Message + '\n');
            }
            mtx.ReleaseMutex();
        }

        private void UartOpenFigaro(string comName)
        {
            try
            {
                if (null == sPort_Figaro)
                {
                    if (comName == null)
                    {
                        LOG("Please select serial port!!!");
                    }
                    else
                    {
                        sPort_Figaro = new SerialPort();
                        sPort_Figaro.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Figaro_DataReceived);
                        sPort_Figaro.PortName = comName;
                        sPort_Figaro.BaudRate = 9600;
                        sPort_Figaro.DataBits = (int)8;
                        sPort_Figaro.Parity = Parity.None;
                        sPort_Figaro.StopBits = StopBits.One;
                        sPort_Figaro.ReadTimeout = -1;
                        sPort_Figaro.WriteTimeout = -1;
                        //sPort_Figaro.NewLine = "\n";
                        sPort_Figaro.Open();
                    }
                }

                if (sPort_Figaro.IsOpen)
                {
                    LOG(CallerName() + "Serial port connected... : )\n");
                    CheckBoxSet(checkBoxFigaro, true);
                    //PrdTimerStart();
                }
                else
                {
                    if (comboBoxFigaro.SelectedItem != null)
                        LOG(CallerName() + "Serial port is in use\n");
                }
            }
            catch (System.Exception ex)
            {
                UartCloseFigaro();
                ERR(ex.Message);
            }
        }

        private void UartCloseFigaro()
        {
            try
            {
                if (null != sPort_Figaro)
                {
                    if (sPort_Figaro.IsOpen)
                    {
                        sPort_Figaro.Close();
                        sPort_Figaro.Dispose();
                        LOG(CallerName() + "Serial port has been closed\n");
                    }
                    else
                    {
                        LOG(CallerName() + "Serial port closed\n");
                    }
                    sPort_Figaro = null;
                    CheckBoxUpdate(checkBoxFigaro, false);
                }
            }
            catch (Exception ex)  // CS0168
            {
                ERR(ex.Message);
            }
        }

        private void update_figaro_ppm(string str)
        {
            TextBoxSet(textBoxFigaro, str);
        }

        private void request_figaro()
        {
            byte[] msg = { 0xFE, 0x44, 0x00, 0x08, 0x02, 0x9F, 0x25 };

            figaroMsgLen = 0;

            if ((sPort_Figaro == null) || (sPort_Figaro.IsOpen == false))
            {
                LOG("Please select serial port\n");
                return;
            }
            LOG("Request figaro\n");

            sPort_Figaro.Write(msg, 0, msg.Length);
        }
    }
}

