﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Ref = null;
		double senseair_co2;
		byte[] senseAir_buff = new byte[20];
		int senseAir_buff_count = 0;
		const int senseAir_response_len = 13;
		bool foundRefSync = false;

        double ref_offset = 0;

		void SerialPort_Ref_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int intRecSize = sPort_Ref.BytesToRead;
			int rxCounter = 0;

			try
			{
				if (intRecSize != 0)
				{
					byte[] buff = new byte[intRecSize];
					int iii;

					iii = sPort_Ref.Read(buff, rxCounter, intRecSize);

					if (foundRefSync)
					{
						// make full packet
						if ((senseAir_buff_count + intRecSize) == senseAir_response_len)    // full packet
						{
							Buffer.BlockCopy(buff, 0, senseAir_buff, senseAir_buff_count, intRecSize);
							senseair_co2 = (UInt16)((senseAir_buff[9] << 8) | senseAir_buff[10]);
							LOG("SenseAir : " + senseair_co2 + "ppm\n");

                            senseair_co2 += ref_offset;
                            update_ref_text(Convert.ToString(senseair_co2));
							foundRefSync = false;
							senseAir_buff_count = 0;
						}
						else
						{
							if ((intRecSize + senseAir_buff_count) < 20)
							{
								Buffer.BlockCopy(buff, 0, senseAir_buff, senseAir_buff_count, intRecSize);
								senseAir_buff_count += intRecSize;
							}
							else
							{
								foundRefSync = false;
								senseAir_buff_count = 0;
							}
						}
					}
					else
					{
						// found sync
						if (buff[0] == 0xFE)
						{
							if (senseAir_response_len < intRecSize)
								return;

							if (senseAir_response_len == intRecSize)        // in case of full packets
							{
								senseair_co2 = (UInt16)((buff[9] << 8) | buff[10]);
								LOG("SenseAir : " + senseair_co2 + "ppm\n");

                                senseair_co2 += ref_offset;
                                update_ref_text(Convert.ToString(senseair_co2));
							}
							else
							{
								Buffer.BlockCopy(buff, 0, senseAir_buff, 0, intRecSize);
								foundRefSync = true;
								senseAir_buff_count = intRecSize;
							}
						}
					}
				}
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				LOG("uart rx error : " + ex.Message + '\n');
			}
		}

		private void UartOpenRef(string comName)
		{
			try
			{
                if(string.IsNullOrWhiteSpace(textBoxRefOffset.Text))
                {
                    ERR("Please input REF OFFSET Value\n");
                    MessageBox.Show("Please input REF OFFSET Value");
                    return;
                }
                else
                {
                    ref_offset = Convert.ToDouble(textBoxRefOffset.Text);
                }

				if (null == sPort_Ref)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Ref = new SerialPort();
						sPort_Ref.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Ref_DataReceived);
						sPort_Ref.PortName = comName;
						sPort_Ref.BaudRate = 9600;
						sPort_Ref.DataBits = (int)8;
						sPort_Ref.Parity = Parity.None;
						sPort_Ref.StopBits = StopBits.One;
						sPort_Ref.ReadTimeout = 0;
						sPort_Ref.WriteTimeout = (int)5000;
						sPort_Ref.NewLine = "\n";
						sPort_Ref.Open();
					}
				}

				if (sPort_Ref.IsOpen)
				{
					buttonRefComOpen.Enabled = false;
					buttonRefComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");

					comboBoxRef.SelectedText = comName;
                    CheckBoxUpdate(checkBoxRef, true);
                    CheckBoxSet(checkBoxSenseAir, true);
                    //PrdTimerStart();
				}
				else
				{
					if (comboBoxRef.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonRefComOpen.Enabled = false;
					buttonRefComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
                if (sPort_Ref != null)
                    sPort_Ref = null;
            }
		}

		private void UartCloseRef()
		{
			try
			{
				if (null != sPort_Ref)
				{
					if (sPort_Ref.IsOpen)
					{
						sPort_Ref.Close();
						sPort_Ref.Dispose();
						sPort_Ref = null;
						buttonRefComClose.Enabled = false;
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
					buttonRefComOpen.Enabled = true;
                    CheckBoxUpdate(checkBoxSenseAir, false);
                }
			}
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void request_ref()
		{
			byte[] msg = { 0xfe, 0x04, 0x00, 0x00, 0x00, 0x04, 0xe5, 0xc6 };

			if ((sPort_Ref == null) || (sPort_Ref.IsOpen == false))
			{
				LOG("시리얼 포트를 연결해 주세요\n");
				return;
			}
			LOG("Request SenseAir\n");

			sPort_Ref.Write(msg, 0, msg.Length);
		}

		private void update_ref_text(string ref_co2)
		{
			if(textBoxRef.InvokeRequired)
			{
				textBoxRef.Invoke(new MethodInvoker(delegate ()
				{
					textBoxRef.Text = ref_co2;
				}));
			}
			else
			{
				textBoxRef.Text = ref_co2;
			}
		}
	}
}
