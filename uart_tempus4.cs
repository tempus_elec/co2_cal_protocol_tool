﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Tempus4 = null;

		bool msgFoundSync4 = false;
		byte msgOffset4 = 0;
		byte msgLen4 = 0;
		byte[] msgBuff4 = new byte[MAX_UART_RX_BUFF_LEN];

		void SerialPort_Tempus4_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int intRecSize = sPort_Tempus4.BytesToRead;

			try
			{
				mtx.WaitOne();

				if (intRecSize != 0)
				{
					byte[] buff = new byte[intRecSize];
					int iii;

					iii = sPort_Tempus4.Read(buff, 0, intRecSize);
					
					LOG("Got " + intRecSize + "\n");

					if (intRecSize > TEMPUS_MAX_RX_LEN)
					{
						ERR("Rx got too long..." + intRecSize + "\n");
                        msgFoundSync4 = false;
                        msgOffset4 = 0;
                        msgLen4 = 0;
                    }
                    else
                    {
                        try
                        {
                            if (msgFoundSync4)
                            {
                                byte len = (byte)(msgOffset4 + intRecSize);
                                Buffer.BlockCopy(buff, 0, msgBuff4, msgOffset4, intRecSize);
                                msgOffset4 += (byte)intRecSize;

                                if ((msgLen4 == 0) && (len > 4))
                                {
                                    msgLen4 = (byte)(msgBuff4[3] + 6);
                                }

                                if (len == msgLen4)
                                {
                                    MsgParser(msgBuff4, 3);
                                    msgFoundSync4 = false;
                                    msgOffset4 = 0;
                                    msgLen4 = 0;
                                }
                                else if (len > msgLen4)
                                {
                                    LOG("Msg parser error\n");
                                    msgFoundSync4 = false;
                                    msgOffset4 = 0;
                                    msgLen4 = 0;
                                }
                                else if (len < msgLen4)
                                {

                                }
                            }
                            else
                            {
                                if (msgOffset4 == 0)
                                {
                                    if (intRecSize > 1)
                                    {
                                        for (int i = 0; i < intRecSize; i++)
                                        {
                                            if (buff[i] == 0xbb)
                                            {
                                                if (i > intRecSize)
                                                {
                                                    msgOffset4 = (byte)intRecSize;
                                                }
                                                else
                                                {
                                                    if (buff[i + 1] == 0x66)
                                                    {
                                                        msgFoundSync4 = true;
                                                        Buffer.BlockCopy(buff, 0, msgBuff4, 0, intRecSize);
                                                        msgOffset4 += (byte)intRecSize;

                                                        if (msgLen4 == 0)
                                                        {
                                                            if (msgOffset4 >= 4)
                                                            {
                                                                msgLen4 = (byte)(msgBuff4[3] + 6);
                                                            }

                                                            if (msgLen4 == intRecSize)
                                                            {
                                                                MsgParser(msgBuff4, 3);
                                                                msgFoundSync4 = false;
                                                                msgOffset4 = 0;
                                                                msgLen4 = 0;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff4, msgOffset4, intRecSize);
                                        msgOffset4 = (byte)intRecSize;
                                    }
                                }
                                else
                                {
                                    if ((msgOffset4 + intRecSize) > MAX_UART_RX_BUFF_LEN)
                                    {
                                        ERR("UART Offset is too big\n");
                                        msgFoundSync4 = false;
                                        msgOffset4 = 0;
                                        msgLen4 = 0;
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff4, msgOffset4, intRecSize);

                                        if ((msgBuff4[0] == 0xbb) && (msgBuff4[1] == 0x66))
                                        {
                                            msgFoundSync4 = true;
                                        }
                                        msgOffset4 += (byte)intRecSize;

                                        if (msgLen4 == 0)
                                        {
                                            if (msgOffset4 >= 4)
                                            {
                                                msgLen4 = (byte)(msgBuff4[3] + 6);
                                            }

                                            if (msgLen4 == msgOffset4)
                                            {
                                                MsgParser(msgBuff4, 3);
                                                msgFoundSync4 = false;
                                                msgOffset4 = 0;
                                                msgLen4 = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            ERR(CallerName() + " : " + ex.Message);
                            msgFoundSync4 = false;
                            msgOffset4 = 0;
                            msgLen4 = 0;
                        }
                    }
				}
				mtx.ReleaseMutex();
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				LOG("uart rx error : " + intRecSize + ", " + ex.Message + '\n');
			}
		}
		
		private void UartOpenTps4(string comName)
		{
			try
			{
				if (null == sPort_Tempus4)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus4 = new SerialPort();
						sPort_Tempus4.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Tempus4_DataReceived);
						sPort_Tempus4.PortName = comName;
						sPort_Tempus4.BaudRate = 9600;
						sPort_Tempus4.DataBits = (int)8;
						sPort_Tempus4.Parity = Parity.None;
						sPort_Tempus4.StopBits = StopBits.One;
						sPort_Tempus4.ReadTimeout = -1;
						sPort_Tempus4.WriteTimeout = -1;
						sPort_Tempus4.NewLine = "\n";
                        try
                        {
                            sPort_Tempus4.Open();
                        }
                        catch (Exception ex)
                        {
                            UartCloseTps4();
                            ERR(CallerName() + " : " + ex.Message);
                        }
                    }
				}

				if (sPort_Tempus4.IsOpen)
				{
					buttonTempus4ComOpen.Enabled = false;
					buttonTempus4ComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");

					comboBoxTps4.SelectedText = comName;
                    bsp_calibration_ctrl(sPort_Tempus4, true);  // port를 열면 바로 CAL cmd를 보내도록 수정
                    CheckBoxUpdate(checkBoxModule4, true);
                    CheckBoxUpdate(checkBoxChart4, true);
                    //PrdTimerStart();
                    if (timer1.Enabled == false)
                    {
                        timer1.Start();
                        buttonStop.Text = "Mon Stop";
                    }
                }
				else
				{
					if (comboBoxTps4.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonTempus4ComOpen.Enabled = false;
					buttonTempus4ComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				UartCloseTps4();
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartCloseTps4()
		{
			try
			{
				if (null != sPort_Tempus4)
				{
					if (sPort_Tempus4.IsOpen)
					{
						sPort_Tempus4.Close();
						sPort_Tempus4.Dispose();
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
					sPort_Tempus4 = null;
				}

                buttonTempus4ComOpen.Enabled = true;
                buttonTempus4ComClose.Enabled = false;
            }
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void request_tempus4()
		{
			bsp_get_dbgInfo(sPort_Tempus4);
            LOG("Request TPS4\n");
        }
	}
}
