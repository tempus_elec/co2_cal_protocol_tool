﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		public void chart_init()
		{
			//////////////////////////////////////////////////////////////
			//	CO2 Chart Init
			//////////////////////////////////////////////////////////////
			Series chartSenseair = chartCO2.Series.Add("SENSEAIR");
			Series chartTempus1 = chartCO2.Series.Add("TEMPUS1");
			Series chartTempus2 = chartCO2.Series.Add("TEMPUS2");
			Series chartTempus3 = chartCO2.Series.Add("TEMPUS3");
			Series chartTempus4 = chartCO2.Series.Add("TEMPUS4");
            Series chartWinsen = chartCO2.Series.Add("WINSEN");
            Series chartCubic = chartCO2.Series.Add("CUBIC");
            Series chartElt = chartCO2.Series.Add("ELT");
            Series chartFigaro = chartCO2.Series.Add("FIGARO");
            Series chartTemp = chartCO2.Series.Add("TEMP");

            chartSenseair.ChartType = SeriesChartType.Line;
			chartCO2.Series["SENSEAIR"].Color = System.Drawing.Color.Green;

			chartTempus1.ChartType = SeriesChartType.Line;
			chartCO2.Series["TEMPUS1"].Color = System.Drawing.Color.Red;

			chartTempus2.ChartType = SeriesChartType.Line;
			chartCO2.Series["TEMPUS2"].Color = System.Drawing.Color.Blue;

			chartTempus3.ChartType = SeriesChartType.Line;
			chartCO2.Series["TEMPUS3"].Color = System.Drawing.Color.Black;

			chartTempus4.ChartType = SeriesChartType.Line;
			chartCO2.Series["TEMPUS4"].Color = System.Drawing.Color.Magenta;

            chartWinsen.ChartType = SeriesChartType.Line;
            chartCO2.Series["WINSEN"].Color = System.Drawing.Color.LightCoral;

            chartCubic.ChartType = SeriesChartType.Line;
            chartCO2.Series["CUBIC"].Color = System.Drawing.Color.Maroon;

            chartElt.ChartType = SeriesChartType.Line;
            chartCO2.Series["ELT"].Color = System.Drawing.Color.LightSlateGray;

            chartFigaro.ChartType = SeriesChartType.Line;
            chartCO2.Series["FIGARO"].Color = System.Drawing.Color.Indigo;

            chartTemp.ChartType = SeriesChartType.Point;
            chartTemp.YAxisType = AxisType.Secondary;
            chartCO2.Series["TEMP"].Color = System.Drawing.Color.DarkGray;
            chartCO2.Series["TEMP"].MarkerStyle = MarkerStyle.Triangle;
            chartCO2.Series["TEMP"].MarkerSize = 5;

            chartCO2.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            chartCO2.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = ChartDashStyle.NotSet;

            chartCO2.ChartAreas[0].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            chartCO2.ChartAreas[0].AxisX2.MajorGrid.LineDashStyle = ChartDashStyle.NotSet;
            //////////////////////////////////////////////////////////////
            //	CO2 dC/dR Init
            //////////////////////////////////////////////////////////////
            Series chartDc1 = chartDcDr.Series.Add("dC1");
			Series chartDr1 = chartDcDr.Series.Add("dR1");
			Series chartDc2 = chartDcDr.Series.Add("dC2");
			Series chartDr2 = chartDcDr.Series.Add("dR2");
			Series chartDc3 = chartDcDr.Series.Add("dC3");
			Series chartDr3 = chartDcDr.Series.Add("dR3");
			Series chartDc4 = chartDcDr.Series.Add("dC4");
			Series chartDr4 = chartDcDr.Series.Add("dR4");

			chartDc1.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dC1"].Color = System.Drawing.Color.Green;
			chartDr1.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dR1"].Color = System.Drawing.Color.Red;

			chartDc2.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dC2"].Color = System.Drawing.Color.Blue;
			chartDr2.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dR2"].Color = System.Drawing.Color.Magenta;

			chartDc3.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dC3"].Color = System.Drawing.Color.Green;
			chartDr3.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dR3"].Color = System.Drawing.Color.Red;

			chartDc4.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dC4"].Color = System.Drawing.Color.Blue;
			chartDr4.ChartType = SeriesChartType.Point;
			chartDcDr.Series["dR4"].Color = System.Drawing.Color.Magenta;

            /*
             * ChartCurv
             */
            Series chartCalA00 = chartCurv.Series.Add("CAL_A00");
            Series chartCalIh0 = chartCurv.Series.Add("CAL_Pk0");
            Series chartRefA00 = chartCurv.Series.Add("REF_A00");
            Series chartRefIh0 = chartCurv.Series.Add("REF_Pk0");

            Series chartCalA01 = chartCurv.Series.Add("CAL_A01");
            Series chartCalIh1 = chartCurv.Series.Add("CAL_Pk1");
            Series chartRefA01 = chartCurv.Series.Add("REF_A01");
            Series chartRefIh1 = chartCurv.Series.Add("REF_Pk1");

            Series chartCalA02 = chartCurv.Series.Add("CAL_A02");
            Series chartCalIh2 = chartCurv.Series.Add("CAL_Pk2");
            Series chartRefA02 = chartCurv.Series.Add("REF_A02");
            Series chartRefIh2 = chartCurv.Series.Add("REF_Pk2");

            Series chartCalA03 = chartCurv.Series.Add("CAL_A03");
            Series chartCalIh3 = chartCurv.Series.Add("CAL_Pk3");
            Series chartRefA03 = chartCurv.Series.Add("REF_A03");
            Series chartRefIh3 = chartCurv.Series.Add("REF_Pk3");

            chartCalA00.ChartType = SeriesChartType.Point;
            chartCurv.Series["CAL_A00"].Color = System.Drawing.Color.Green;
            chartCurv.Series["CAL_A00"].MarkerStyle = MarkerStyle.Square;
            chartCurv.Series["CAL_A00"].MarkerSize = 5;
            chartCalIh0.ChartType = SeriesChartType.Point;
            //chartCalIh0.YAxisType = AxisType.Secondary;
            chartCurv.Series["CAL_Pk0"].Color = System.Drawing.Color.Red;
            chartCurv.Series["CAL_Pk0"].MarkerStyle = MarkerStyle.Square;
            chartCurv.Series["CAL_Pk0"].MarkerSize = 5;
            chartRefA00.ChartType = SeriesChartType.Point;
            chartCurv.Series["REF_A00"].Color = System.Drawing.Color.Blue;
            chartCurv.Series["REF_A00"].MarkerStyle = MarkerStyle.Square;
            chartCurv.Series["REF_A00"].MarkerSize = 5;
            chartRefIh0.ChartType = SeriesChartType.Point;
            //chartRefIh0.YAxisType = AxisType.Secondary;
            chartCurv.Series["REF_Pk0"].Color = System.Drawing.Color.Magenta;
            chartCurv.Series["REF_Pk0"].MarkerStyle = MarkerStyle.Square;
            chartCurv.Series["REF_Pk0"].MarkerSize = 5;
            

            chartCalA01.ChartType = SeriesChartType.Point;
            chartCurv.Series["CAL_A01"].Color = System.Drawing.Color.Green;
            chartCurv.Series["CAL_A01"].MarkerStyle = MarkerStyle.Star10;
            chartCurv.Series["CAL_A01"].MarkerSize = 5;
            chartCalIh1.ChartType = SeriesChartType.Point;
            //chartCalIh1.YAxisType = AxisType.Secondary;
            chartCurv.Series["CAL_Pk1"].Color = System.Drawing.Color.Red;
            chartCurv.Series["CAL_Pk1"].MarkerStyle = MarkerStyle.Star10;
            chartCurv.Series["CAL_Pk1"].MarkerSize = 5;
            chartRefA01.ChartType = SeriesChartType.Point;
            chartCurv.Series["REF_A01"].Color = System.Drawing.Color.Blue;
            chartCurv.Series["REF_A01"].MarkerStyle = MarkerStyle.Star10;
            chartCurv.Series["REF_A01"].MarkerSize = 5;
            chartRefIh1.ChartType = SeriesChartType.Point;
            //chartRefIh1.YAxisType = AxisType.Secondary;
            chartCurv.Series["REF_Pk1"].Color = System.Drawing.Color.Magenta;
            chartCurv.Series["REF_Pk1"].MarkerStyle = MarkerStyle.Star10;
            chartCurv.Series["REF_Pk1"].MarkerSize = 5;

            chartCalA02.ChartType = SeriesChartType.Point;
            chartCurv.Series["CAL_A02"].Color = System.Drawing.Color.Green;
            chartCurv.Series["CAL_A02"].MarkerStyle = MarkerStyle.Cross;
            chartCurv.Series["CAL_A02"].MarkerSize = 5;
            chartCalIh2.ChartType = SeriesChartType.Point;
            //chartCalIh2.YAxisType = AxisType.Secondary;
            chartCurv.Series["CAL_Pk2"].Color = System.Drawing.Color.Red;
            chartCurv.Series["CAL_Pk2"].MarkerStyle = MarkerStyle.Cross;
            chartCurv.Series["CAL_Pk2"].MarkerSize = 5;
            chartRefA02.ChartType = SeriesChartType.Point;
            chartCurv.Series["REF_A02"].Color = System.Drawing.Color.Blue;
            chartCurv.Series["REF_A02"].MarkerStyle = MarkerStyle.Cross;
            chartCurv.Series["REF_A02"].MarkerSize = 5;
            chartRefIh2.ChartType = SeriesChartType.Point;
            //chartRefIh2.YAxisType = AxisType.Secondary;
            chartCurv.Series["REF_Pk2"].Color = System.Drawing.Color.Magenta;
            chartCurv.Series["REF_Pk2"].MarkerStyle = MarkerStyle.Cross;
            chartCurv.Series["REF_Pk2"].MarkerSize = 5;

            chartCalA03.ChartType = SeriesChartType.Point;
            chartCurv.Series["CAL_A03"].Color = System.Drawing.Color.Green;
            chartCurv.Series["CAL_A03"].MarkerStyle = MarkerStyle.Diamond;
            chartCurv.Series["CAL_A03"].MarkerSize = 5;
            chartCalIh3.ChartType = SeriesChartType.Point;
            //chartCalIh3.YAxisType = AxisType.Secondary;
            chartCurv.Series["CAL_Pk3"].Color = System.Drawing.Color.Red;
            chartCurv.Series["CAL_Pk3"].MarkerStyle = MarkerStyle.Diamond;
            chartCurv.Series["CAL_Pk3"].MarkerSize = 5;
            chartRefA03.ChartType = SeriesChartType.Point;
            chartCurv.Series["REF_A03"].Color = System.Drawing.Color.Blue;
            chartCurv.Series["REF_A03"].MarkerStyle = MarkerStyle.Diamond;
            chartCurv.Series["REF_A03"].MarkerSize = 5;
            chartRefIh3.ChartType = SeriesChartType.Point;
            //chartRefIh3.YAxisType = AxisType.Secondary;
            chartCurv.Series["REF_Pk3"].Color = System.Drawing.Color.Magenta;
            chartCurv.Series["REF_Pk3"].MarkerStyle = MarkerStyle.Diamond;
            chartCurv.Series["REF_Pk3"].MarkerSize = 5;

            /*
             * 
             */
            Series chartCurvCal = chartDispCurv.Series.Add("CAL");
            Series chartCurvRef = chartDispCurv.Series.Add("REF");
            Series chartCurvAdc = chartDispCurv.Series.Add("ADC");
            chartCurvCal.ChartType = SeriesChartType.Point;
            chartDispCurv.Series["CAL"].Color = System.Drawing.Color.Red;
            chartCurvRef.ChartType = SeriesChartType.Point;
            chartDispCurv.Series["REF"].Color = System.Drawing.Color.Blue;
            chartCurvAdc.ChartType = SeriesChartType.Line;
            chartDispCurv.Series["ADC"].Color = System.Drawing.Color.Green;
        }

		/*
		 * Chart CO2 Update
		 */
		void chart_co2_update(UInt64 count, double co20, double co21, double co22, double co23, double co24, double co2_winsen, double cubic, double elt, double figaro)
		{
			if (chartCO2.InvokeRequired)
			{
				chartCO2.Invoke(new MethodInvoker(delegate ()
				{
					if(checkBoxChart1.Checked)
					{
						chartCO2.Series["TEMPUS1"].Points.AddXY(count, co21);
                        //textBoxDiff1.Text = Convert.ToString(co20 - co21);
                        GetErrRate(co21, textBoxDiff1);
                    }
					else
					{
						textBoxDiff1.Text = "-";
					}						

					if (checkBoxChart2.Checked)
					{
						chartCO2.Series["TEMPUS2"].Points.AddXY(count, co22);
                        //textBoxDiff2.Text = Convert.ToString(co20 - co22);
                        GetErrRate(co22, textBoxDiff2);
                    }
					else
					{
						textBoxDiff2.Text = "-";
					}

					if (checkBoxChart3.Checked)
					{
						chartCO2.Series["TEMPUS3"].Points.AddXY(count, co23);
                        //textBoxDiff3.Text = Convert.ToString(co20 - co23);
                        GetErrRate(co23, textBoxDiff3);
                    }
					else
					{
						textBoxDiff3.Text = "-";
					}

					if (checkBoxChart4.Checked)
					{
						chartCO2.Series["TEMPUS4"].Points.AddXY(count, co24);
                        //textBoxDiff4.Text = Convert.ToString(co20 - co24);
                        GetErrRate(co24, textBoxDiff4);
                    }
					else
					{
						textBoxDiff4.Text = "-";
					}

                    if (checkBoxWinsen.Checked)
                    {
                        chartCO2.Series["WINSEN"].Points.AddXY(count, co2_winsen);
                        textBoxDiff5.Text = Convert.ToString(co20 - co2_winsen);
                    }
                    else
                    {
                        textBoxDiff5.Text = "-";
                    }
                    //chartCO2.Series["SENSEAIR"].Points.AddXY(count, co20);
                    ChartCheckBox(chartCO2, checkBoxSenseAir, (UInt32)count, "SENSEAIR", co20);
                    ChartCheckBox(chartCO2, checkBoxCubic, (UInt32)count, "CUBIC", cubic);
                    ChartCheckBox(chartCO2, checkBoxElt, (UInt32)count, "ELT", elt);
                    ChartCheckBox(chartCO2, checkBoxFigaro, (UInt32)count, "FIGARO", figaro);
                    //ChartSet(chartCO2, (UInt32)count, "TEMP", temp);
                }));
			}
			else
			{
                //chartCO2.Series["SENSEAIR"].Points.AddXY(count, co20);

                if (checkBoxChart1.Checked)
                {
                    chartCO2.Series["TEMPUS1"].Points.AddXY(count, co21);
                    //textBoxDiff1.Text = Convert.ToString(co20 - co21);
                    GetErrRate(co21, textBoxDiff1);
                }
                else
                {
                    textBoxDiff1.Text = "-";
                }

                if (checkBoxChart2.Checked)
                {
                    chartCO2.Series["TEMPUS2"].Points.AddXY(count, co22);
                    //textBoxDiff2.Text = Convert.ToString(co20 - co22);
                    GetErrRate(co22, textBoxDiff2);
                }
                else
                {
                    textBoxDiff2.Text = "-";
                }

                if (checkBoxChart3.Checked)
                {
                    chartCO2.Series["TEMPUS3"].Points.AddXY(count, co23);
                    //textBoxDiff3.Text = Convert.ToString(co20 - co23);
                    GetErrRate(co23, textBoxDiff3);
                }
                else
                {
                    textBoxDiff3.Text = "-";
                }

                if (checkBoxChart4.Checked)
                {
                    chartCO2.Series["TEMPUS4"].Points.AddXY(count, co24);
                    //textBoxDiff4.Text = Convert.ToString(co20 - co24);
                    GetErrRate(co24, textBoxDiff4);
                }
                else
                {
                    textBoxDiff4.Text = "-";
                }

                if (checkBoxWinsen.Checked)
                {
                    chartCO2.Series["WINSEN"].Points.AddXY(count, co2_winsen);
                    textBoxDiff5.Text = Convert.ToString(co20 - co2_winsen);
                }
                else
                {
                    textBoxDiff5.Text = "-";
                }
                ChartCheckBox(chartCO2, checkBoxSenseAir, (UInt32)count, "SENSEAIR", co20);
                ChartCheckBox(chartCO2, checkBoxCubic, (UInt32)count, "CUBIC", cubic);
                ChartCheckBox(chartCO2, checkBoxElt, (UInt32)count, "ELT", elt);
                ChartCheckBox(chartCO2, checkBoxFigaro, (UInt32)count, "FIGARO", figaro);
                //ChartSet(chartCO2, (UInt32)count, "TEMP", temp);
            }
			
			chartCO2.Invalidate();
		}

		void chart_clear()
		{
			foreach (var series in chartCO2.Series)
			{
				series.Points.Clear();
			}
		}

		void chart_dcdr_update(UInt64 count, double dc1, double dr1, double dc2, double dr2, double dc3, double dr3, double dc4, double dr4)
		{
			if (chartDcDr.InvokeRequired)
			{
				chartDcDr.Invoke(new MethodInvoker(delegate ()
				{
					if (checkBoxDcDr1.Checked)
					{
						chartDcDr.Series["dC1"].Points.AddXY(count, dc1);
						chartDcDr.Series["dR1"].Points.AddXY(count, dr1);
					}

					if (checkBoxDcDr2.Checked)
					{
						chartDcDr.Series["dC2"].Points.AddXY(count, dc2);
						chartDcDr.Series["dR2"].Points.AddXY(count, dr2);
					}

					if (checkBoxDcDr3.Checked)
					{
						chartDcDr.Series["dC3"].Points.AddXY(count, dc3);
						chartDcDr.Series["dR3"].Points.AddXY(count, dr3);
					}

					if (checkBoxDcDr4.Checked)
					{
						chartDcDr.Series["dC4"].Points.AddXY(count, dc4);
						chartDcDr.Series["dR4"].Points.AddXY(count, dr4);
					}
				}));
			}
			else
			{
				if (checkBoxDcDr1.Checked)
				{
					chartDcDr.Series["dC1"].Points.AddXY(count, dc1);
					chartDcDr.Series["dR1"].Points.AddXY(count, dr1);
				}

				if (checkBoxDcDr2.Checked)
				{
					chartDcDr.Series["dC2"].Points.AddXY(count, dc2);
					chartDcDr.Series["dR2"].Points.AddXY(count, dr2);
				}

				if (checkBoxDcDr3.Checked)
				{
					chartDcDr.Series["dC3"].Points.AddXY(count, dc3);
					chartDcDr.Series["dR3"].Points.AddXY(count, dr3);
				}

				if (checkBoxDcDr4.Checked)
				{
					chartDcDr.Series["dC4"].Points.AddXY(count, dc4);
					chartDcDr.Series["dR4"].Points.AddXY(count, dr4);
				}
			}

			chartDcDr.Invalidate();
		}

		void chart_dcdr_clear()
		{
			foreach (var series in chartDcDr.Series)
			{
				series.Points.Clear();
			}
		}

        bool GetErrRate(double ppm, TextBox tb)
        {
            double errH = 0, errL = 0;
            double ppm_diff = 0;
            double errRate = 0.0;
            double refPpm = 0;

            /*
             * REF sensor value check
             */
            if (radioButtonRefSenseAir.Checked)
                refPpm = senseair_co2;
            else if (radioButtonRefCubic.Checked)
                refPpm = cm1106_ppm;
            else if (radioButtonRefT360.Checked)
                refPpm = senseair_co2;
            else if (radioButtonRefWinsen.Checked)
                refPpm = winsen_ppm;
            else if (radioButtonRefElt.Checked)
                refPpm = elt_ppm;
            else if (radioButtonRefFigaro.Checked)
                refPpm = figaro_ppm;
            /*******************************************/

            ppm_diff = refPpm - ppm;

            if (radioButton3pro.Checked)
            {
                errRate = 0.03;
            }
            else if(radioButton5pro.Checked)
            {
                errRate = 0.05;
            }
            else if (radioButtonEtc.Checked)
            {
                if (string.IsNullOrWhiteSpace(textBoxEtcPro.Text))
                {
                    ERR("Please input ETC % Value\n");
                    MessageBox.Show("Please input ETC % Value");
                    return false;
                }
                else
                    errRate = Convert.ToDouble(textBoxEtcPro.Text);
            }

            errH = refPpm + (errRate * 0.05) + 50;
            errL = refPpm - (errRate * 0.05) - 50;

            if ((ppm > errH) || (ppm < errL))
            {
                tb.ForeColor = System.Drawing.Color.Red;
                tb.Text = ppm_diff.ToString();

                return false;
            }                
            else
            {
                tb.ForeColor = System.Drawing.Color.Black;
                tb.Text = ppm_diff.ToString();

                return true;
            }
        }

        public void chart_a0_ih_update(UInt64 count, double cal_a00, double CAL_Pk0, double ref_a00, double REF_Pk0, double cal_a01, double CAL_Pk1, double ref_a01, double REF_Pk1, double cal_a02, double CAL_Pk2, double ref_a02, double REF_Pk2, double cal_a03, double CAL_Pk3, double ref_a03, double REF_Pk3)
        {
            if (chartCurv.InvokeRequired)
            {
                chartCurv.Invoke(new MethodInvoker(delegate ()
                {
                    if(checkBoxCurv1.Checked)
                    {
                        chartCurv.Series["CAL_A00"].Points.AddXY(count, cal_a00);
                        chartCurv.Series["CAL_Pk0"].Points.AddXY(count, CAL_Pk0);
                        chartCurv.Series["REF_A00"].Points.AddXY(count, ref_a00);
                        chartCurv.Series["REF_Pk0"].Points.AddXY(count, REF_Pk0);
                    }

                    if (checkBoxCurv2.Checked)
                    {
                        chartCurv.Series["CAL_A01"].Points.AddXY(count, cal_a01);
                        chartCurv.Series["CAL_Pk1"].Points.AddXY(count, CAL_Pk1);
                        chartCurv.Series["REF_A01"].Points.AddXY(count, ref_a01);
                        chartCurv.Series["REF_Pk1"].Points.AddXY(count, REF_Pk1);
                    }

                    if (checkBoxCurv3.Checked)
                    {
                        chartCurv.Series["CAL_A02"].Points.AddXY(count, cal_a02);
                        chartCurv.Series["CAL_Pk2"].Points.AddXY(count, CAL_Pk2);
                        chartCurv.Series["REF_A02"].Points.AddXY(count, ref_a02);
                        chartCurv.Series["REF_Pk2"].Points.AddXY(count, REF_Pk2);
                    }

                    if (checkBoxCurv4.Checked)
                    {
                        chartCurv.Series["CAL_A03"].Points.AddXY(count, cal_a03);
                        chartCurv.Series["CAL_Pk3"].Points.AddXY(count, CAL_Pk3);
                        chartCurv.Series["REF_A03"].Points.AddXY(count, ref_a03);
                        chartCurv.Series["REF_Pk3"].Points.AddXY(count, REF_Pk3);
                    }                    
                }));
            }
            else
            {
                if (checkBoxCurv1.Checked)
                {
                    chartCurv.Series["CAL_A00"].Points.AddXY(count, cal_a00);
                    chartCurv.Series["CAL_Pk0"].Points.AddXY(count, CAL_Pk0);
                    chartCurv.Series["REF_A00"].Points.AddXY(count, ref_a00);
                    chartCurv.Series["REF_Pk0"].Points.AddXY(count, REF_Pk0);
                }

                if (checkBoxCurv2.Checked)
                {
                    chartCurv.Series["CAL_A01"].Points.AddXY(count, cal_a01);
                    chartCurv.Series["CAL_Pk1"].Points.AddXY(count, CAL_Pk1);
                    chartCurv.Series["REF_A01"].Points.AddXY(count, ref_a01);
                    chartCurv.Series["REF_Pk1"].Points.AddXY(count, REF_Pk1);
                }

                if (checkBoxCurv3.Checked)
                {
                    chartCurv.Series["CAL_A02"].Points.AddXY(count, cal_a02);
                    chartCurv.Series["CAL_Pk2"].Points.AddXY(count, CAL_Pk2);
                    chartCurv.Series["REF_A02"].Points.AddXY(count, ref_a02);
                    chartCurv.Series["REF_Pk2"].Points.AddXY(count, REF_Pk2);
                }

                if (checkBoxCurv4.Checked)
                {
                    chartCurv.Series["CAL_A03"].Points.AddXY(count, cal_a03);
                    chartCurv.Series["CAL_Pk3"].Points.AddXY(count, CAL_Pk3);
                    chartCurv.Series["REF_A03"].Points.AddXY(count, ref_a03);
                    chartCurv.Series["REF_Pk3"].Points.AddXY(count, REF_Pk3);
                }
            }

            chartCurv.Invalidate();
        }

        void chart_a0_ih_clear()
        {
            foreach (var series in chartCurv.Series)
            {
                series.Points.Clear();
            }
        }

        public void chart_curv_update(string[] spData)
        {
            try
            {
                ushort curvCount = 0;
                double adc = 0;
                
                for (UInt32 i = 1; i < spData.Length - 1; curvCount++)
                {
                    if (chartCurv.InvokeRequired)
                    {
                        chartCurv.Invoke(new MethodInvoker(delegate ()
                        {
                            chartCurv.Series["CAL"].Points.AddXY(curvCount, Convert.ToDouble(spData[i++]));
                            chartCurv.Series["REF"].Points.AddXY(curvCount, Convert.ToDouble(spData[i++]));
                            adc = Convert.ToDouble(spData[i++]) * 12000;
                            //chartCurv.Series["ADC"].Points.AddXY(curvCount, adc);
                        }));
                    }
                    else
                    {
                        chartCurv.Series["CAL"].Points.AddXY(curvCount, Convert.ToDouble(spData[i++]));
                        chartCurv.Series["REF"].Points.AddXY(curvCount, Convert.ToDouble(spData[i++]));
                        adc = Convert.ToDouble(spData[i++]) * 12000;
                        //chartCurv.Series["ADC"].Points.AddXY(curvCount, adc);
                    }
                }
            }

            catch (InvalidOperationException exc)
            {
                MessageBox.Show(exc.ToString());
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        public void chart_curv_clear()
        {
            foreach (var series in chartDispCurv.Series)
            {
                series.Points.Clear();
            }
        }
    }
}
