﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Tempus3 = null;

		bool msgFoundSync3 = false;
		byte msgOffset3 = 0;
		byte msgLen3 = 0;
		byte[] msgBuff3 = new byte[MAX_UART_RX_BUFF_LEN];

		void SerialPort_Tempus3_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int intRecSize = sPort_Tempus3.BytesToRead;

			try
			{
				mtx.WaitOne();

				if (intRecSize != 0)
				{
					byte[] buff = new byte[intRecSize];
					int iii;

					iii = sPort_Tempus3.Read(buff, 0, intRecSize);
					
					LOG("Got " + intRecSize + "\n");

					if (intRecSize > TEMPUS_MAX_RX_LEN)
					{
						ERR("Rx got too long..." + intRecSize + "\n");
                        msgFoundSync3 = false;
                        msgOffset3 = 0;
                        msgLen3 = 0;
                    }
                    else
                    {
                        try
                        {
                            if (msgFoundSync3)
                            {
                                byte len = (byte)(msgOffset3 + intRecSize);
                                Buffer.BlockCopy(buff, 0, msgBuff3, msgOffset3, intRecSize);
                                msgOffset3 += (byte)intRecSize;

                                if ((msgLen3 == 0) && (len > 4))
                                {
                                    msgLen3 = (byte)(msgBuff3[3] + 6);
                                }

                                if (len == msgLen3)
                                {
                                    MsgParser(msgBuff3, 2);
                                    msgFoundSync3 = false;
                                    msgOffset3 = 0;
                                    msgLen3 = 0;
                                }
                                else if (len > msgLen3)
                                {
                                    LOG("Msg parser error\n");
                                    msgFoundSync3 = false;
                                    msgOffset3 = 0;
                                    msgLen3 = 0;
                                }
                                else if (len < msgLen3)
                                {

                                }
                            }
                            else
                            {
                                if (msgOffset3 == 0)
                                {
                                    if (intRecSize > 1)
                                    {
                                        for (int i = 0; i < intRecSize; i++)
                                        {
                                            if (buff[i] == 0xbb)
                                            {
                                                if (i > intRecSize)
                                                {
                                                    msgOffset3 = (byte)intRecSize;
                                                }
                                                else
                                                {
                                                    if (buff[i + 1] == 0x66)
                                                    {
                                                        msgFoundSync3 = true;
                                                        Buffer.BlockCopy(buff, 0, msgBuff3, 0, intRecSize);
                                                        msgOffset3 += (byte)intRecSize;

                                                        if (msgLen3 == 0)
                                                        {
                                                            if (msgOffset3 >= 4)
                                                            {
                                                                msgLen3 = (byte)(msgBuff3[3] + 6);
                                                            }

                                                            if (msgLen3 == intRecSize)
                                                            {
                                                                MsgParser(msgBuff3, 2);
                                                                msgFoundSync3 = false;
                                                                msgOffset3 = 0;
                                                                msgLen3 = 0;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff3, msgOffset3, intRecSize);
                                        msgOffset3 = (byte)intRecSize;
                                    }
                                }
                                else
                                {
                                    if ((msgOffset3 + intRecSize) > MAX_UART_RX_BUFF_LEN)
                                    {
                                        ERR("UART Offset is too big\n");
                                        msgFoundSync3 = false;
                                        msgOffset3 = 0;
                                        msgLen3 = 0;
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff3, msgOffset3, intRecSize);

                                        if ((msgBuff3[0] == 0xbb) && (msgBuff3[1] == 0x66))
                                        {
                                            msgFoundSync3 = true;
                                        }
                                        msgOffset3 += (byte)intRecSize;

                                        if (msgLen3 == 0)
                                        {
                                            if (msgOffset3 >= 4)
                                            {
                                                msgLen3 = (byte)(msgBuff3[3] + 6);
                                            }

                                            if (msgLen3 == msgOffset3)
                                            {
                                                MsgParser(msgBuff3, 2);
                                                msgFoundSync3 = false;
                                                msgOffset3 = 0;
                                                msgLen3 = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            ERR(CallerName() + " : " + ex.Message);
                            msgFoundSync3 = false;
                            msgOffset3 = 0;
                            msgLen3 = 0;
                        }
                    }
				}
				mtx.ReleaseMutex();
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				LOG("uart rx error : " + intRecSize + ", " + ex.Message + '\n');
			}
		}
		
		private void UartOpenTps3(string comName)
		{
			try
			{
				if (null == sPort_Tempus3)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus3 = new SerialPort();
						sPort_Tempus3.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Tempus3_DataReceived);
						sPort_Tempus3.PortName = comName;
						sPort_Tempus3.BaudRate = 9600;
						sPort_Tempus3.DataBits = (int)8;
						sPort_Tempus3.Parity = Parity.None;
						sPort_Tempus3.StopBits = StopBits.One;
						sPort_Tempus3.ReadTimeout = -1;
						sPort_Tempus3.WriteTimeout = -1;
						sPort_Tempus3.NewLine = "\n";
                        try
                        {
                            sPort_Tempus3.Open();
                        }
                        catch(Exception ex)
                        {
                            UartCloseTps3();
                            ERR(CallerName() + " : " + ex.Message);
                        }
#if false
                        catch(UnauthorizedAccessException ex)
                        {
                            UartCloseTps3();
                            ERR(CallerName() + " : " + ex.Message);
                            ERR("The current process, or another process on the system, already has the specified COM port open either by a SerialPort instance or in unmanaged code.\n");
                        }
                        catch(ArgumentOutOfRangeException ex)
                        {
                            UartCloseTps3();
                            ERR(CallerName() + " : " + ex.Message);
                            ERR("One or more of the properties for this instance are invalid.\n");
                        }
                        catch(ArgumentException ex)
                        {
                            UartCloseTps3();
                            ERR(CallerName() + " : " + ex.Message);
                            ERR("The file type of the port is not supported.\n");
                        }
                        catch(IOException ex)
                        {
                            UartCloseTps3();
                            ERR(CallerName() + " : " + ex.Message);
                            ERR("The port is in an invalid state.\n");
                        }
                        catch(InvalidOperationException ex)
                        {
                            UartCloseTps3();
                            ERR(CallerName() + " : " + ex.Message);
                            ERR("The specified port on the current instance of the SerialPort is already open.\n");
                        }
#endif
					}
				}

				if (sPort_Tempus3.IsOpen)
				{
					buttonTempus3ComOpen.Enabled = false;
					buttonTempus3ComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");

					comboBoxTps3.SelectedText = comName;
                    bsp_calibration_ctrl(sPort_Tempus3, true);  // port를 열면 바로 CAL cmd를 보내도록 수정
                    CheckBoxUpdate(checkBoxModule3, true);
                    CheckBoxUpdate(checkBoxChart3, true);
                    //PrdTimerStart();
                    if (timer1.Enabled == false)
                    {
                        timer1.Start();
                        buttonStop.Text = "Mon Stop";
                    }
                }
				else
				{
					if (comboBoxTps3.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonTempus3ComOpen.Enabled = false;
					buttonTempus3ComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				UartCloseTps3();
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartCloseTps3()
		{
			try
			{
				if (null != sPort_Tempus3)
				{
					if (sPort_Tempus3.IsOpen)
					{
						sPort_Tempus3.Close();
						sPort_Tempus3.Dispose();
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
					sPort_Tempus3 = null;
				}

                buttonTempus3ComOpen.Enabled = true;
                buttonTempus3ComClose.Enabled = false;
            }
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void request_tempus3()
		{
			bsp_get_dbgInfo(sPort_Tempus3);
            LOG("Request TPS3\n");
        }
	}
}
