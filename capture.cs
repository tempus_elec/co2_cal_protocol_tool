﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		public DateTime Delay(int MS)
		{
			DateTime ThisMoment = DateTime.Now;
			TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
			DateTime AfterWards = ThisMoment.Add(duration);

			while (AfterWards >= ThisMoment)
			{
				System.Windows.Forms.Application.DoEvents();
				ThisMoment = DateTime.Now;
			}

			return DateTime.Now;
		}

		public string Str2Hex(string strData)
		{
			string resultHex = string.Empty;
			byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

			foreach (byte byteStr in arr_byteStr)
				resultHex += string.Format("{0:X2}", byteStr);

			return resultHex;
		}

		public string ByteArrayToString(byte[] ba)
		{
			StringBuilder hex = new StringBuilder(ba.Length * 2);
			foreach (byte b in ba)
				hex.AppendFormat("{0:x2}", b);
			return hex.ToString();
		}

		public UInt16 GetCRC16(byte[] temp, byte length)
		{
			UInt16 CRC16 = 0xffff;
			UInt16 POLY = 0xa001;
			byte MessageLength = length;
			UInt32 shift = 0;
			UInt16 k = 0;
			UInt16 code;
			UInt16 crcTemp;
			for (k = 0; k < MessageLength; k++)
			{
				code = (UInt16)temp[k];
				CRC16 = (UInt16)(CRC16 ^ code);
				shift = 0;
				while (shift <= 7)
				{
					crcTemp = (UInt16)(CRC16 & 0x01);
					if (crcTemp != 0)
					{
						CRC16 = (UInt16)(CRC16 >> 1);
						CRC16 = (UInt16)(CRC16 ^ POLY);

					}
					else
					{
						CRC16 = (UInt16)(CRC16 >> 1);
					}
					shift++;
				}
			}
			return CRC16;
		}

		public string ConvertStringArrayToString(string[] array)
		{
			//
			// Concatenate all the elements into a StringBuilder.
			//
			StringBuilder strinbuilder = new StringBuilder();
			foreach (string value in array)
			{
				strinbuilder.Append(value);
				strinbuilder.Append(' ');
			}
			return strinbuilder.ToString();
		}

        public float GetFloatIEE754(byte[] array)
        {
            Array.Reverse(array);
            return BitConverter.ToSingle(array, 0);
        }

        public byte[] HexStrToByteArray(string str)
        {
            byte[] bytes = new byte[str.Length / 2];

            try
            {
                for (int i = 0; i < str.Length; i += 2)
                    bytes[i / 2] = Convert.ToByte(str.Substring(i, 2), 16);

                return bytes;
            }
            catch (Exception ex)
            {
                ERR(CallerName() + ex.Message + "\n");
            }
            return bytes;
        }

        public UInt16 HexStrToUInt16(string str)
        {
            try
            {
                str = str.Replace("0x", "");
                byte[] bVal = HexStrToByteArray(str);

                return (UInt16)((bVal[0] << 8) | (bVal[1]));
            }
            catch (Exception ex)
            {
                ERR(CallerName() + ex.Message + "\n");
            }
            return 0;
        }

        public byte HexStrToByte(string str)
        {
            try
            {
                str = str.Replace("0x", "");
                byte[] bVal = HexStrToByteArray(str);

                return bVal[0];
            }
            catch (Exception ex)
            {
                ERR(CallerName() + ex.Message + "\n");
            }

            return 0;
        }
    }
}
