﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
        // capture 전에 window활성화를 하기 위한 API import ////////////////////////////////
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;

        UInt16 capture_bmp_file_index = 0;

        public void ScreenCapture()
        {
            string outputFilename = desktop_path + "/screen_" + capture_bmp_file_index++ + ".png";

            ActivateWindow();

            Bitmap bitmap = new Bitmap(this.Width, this.Height);
            Graphics g = Graphics.FromImage(bitmap);

            g.CopyFromScreen(this.Location, new Point(0, 0), new Size(this.Width, this.Height));
            bitmap.Save(outputFilename, System.Drawing.Imaging.ImageFormat.Png);
        }

        public void ActivateWindow()
        {
            string processName = "10in1_CAL_Mon V1.0";
            Process[] procs = Process.GetProcessesByName(processName);
            string procTitle = null;

            if (procs != null && procs.Length > 0)
            {
                procTitle = procs[0].MainWindowTitle;
            }
            else
            {
                return;
            }

            // 윈도우 타이틀명으로 핸들을 찾는다 
            IntPtr procHandler = FindWindow(null, procTitle);
            if (!procHandler.Equals(IntPtr.Zero))
            {
                // 윈도우가 최소화 되어 있다면 활성화 시킨다 
                ShowWindowAsync(procHandler, SW_SHOWNORMAL);
            }
            // 윈도우에 포커스를 줘서 최상위로 만든다 
            SetForegroundWindow(procHandler);
        }
    }
}
