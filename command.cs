﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		bool calMode = true;

		enum TpsMsg
		{
			/*----User Area Message---------*/
			BSP_CMD_GET_VER = 0x10,
			BSP_RES_GET_VER = 0x11,
			BSP_CMD_GET_SER = 0x12,
			BSP_RES_GET_SER = 0x13,
			BSP_CMD_GET_PPM = 0x14,
			BSP_RES_GET_PPM = 0x15,
			BSP_CMD_GET_ALARM = 0x16,
			BSP_RES_GET_ALARM = 0x17,
			BSP_CMD_SET_ALARM = 0x18,
			BSP_RES_SET_ALARM = 0x19,
			BSP_CMD_GET_REF = 0x1A,
			BSP_RES_GET_REF = 0x1B,
			/*----End Of User Message-------*/

			/*-----Debug Message------------*/
			BSP_CMD_DBG_GET_INFO = 0x80,
			BSP_RES_DBG_GET_INFO = 0x81,
			BSP_CMD_DBG_GET_I2C_ADDR = 0x82,
			BSP_RES_DBG_GET_I2C_ADDR = 0x83,
			/*------------------------------*/
			/*------------------------------*/
			BSP_CMD_DBG_SET_SER = 0x90,
			BSP_RES_DBG_SET_SER = 0x91,
			BSP_CMD_DBG_SET_SC = 0x92,
			BSP_RES_DBG_SET_SC = 0x93,
			BSP_CMD_DBG_SET_SR = 0x94,
			BSP_RES_DBG_SET_SR = 0x95,
			BSP_CMD_DBG_SET_GAIN = 0x96,
			BSP_RES_DBG_SET_GAIN = 0x97,
			BSP_CMD_DBG_SET_I2C_ADDR = 0x98,
			BSP_RES_DBG_SET_I2C_ADDR = 0x99,
			BSP_CMD_DBG_SET_TS = 0x9A,
			BSP_RES_DBG_SET_TS = 0x9B,
			BSP_CMD_DBG_SET_DBG = 0x9C,
			BSP_RES_DBG_SET_DBG = 0x9D,
			BSP_CMD_DBG_SET_CURV = 0x9E,
			BSP_RES_DBG_SET_CURV = 0x9F,

			BSP_CMD_SET_CALIBRATION_MODE = 0xA0,
			BSP_RES_SET_CALIBRATION_MODE = 0xA1,

			BSP_CMD_GET_DETECTOR_ID = 0xA2,
			BSP_RES_GET_DETECTOR_ID = 0xA3,

            BSP_CMD_DBG_SET_ZMDI = 0xB0,
            BSP_RES_DBG_SET_ZMDI = 0xB1,
            BSP_CMD_DBG_GET_ZMDI = 0xB2,
            BSP_RES_DBG_GET_ZMDI = 0xB3,

            // Power Compensation Parameters
            BSP_CMD_DBG_SET_PC_PARAM = 0xB4,
            BSP_RES_DBG_SET_PC_PARAM = 0xB5,
            BSP_CMD_DBG_GET_PC_PARAM = 0xB6,
            BSP_RES_DBG_GET_PC_PARAM = 0xB7,
            BSP_CMD_DBG_SET_TC_PARAM = 0xB8,
            BSP_RES_DBG_SET_TC_PARAM = 0xB9,
            BSP_CMD_DBG_GET_TC_PARAM = 0xBA,
            BSP_RES_DBG_GET_TC_PARAM = 0xBB,
            /*---End Of DBG Message--------*/
        };

		private int bsp_calibration_ctrl(SerialPort port, bool en)
		{
			int ret = 0;

			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_SET_CALIBRATION_MODE, 0x01, 0x00, 0x00, 0x00 };
			UInt16 crc_val;

			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			if (en)
			{
				msg[4] = 1;
			}
			else
			{
				msg[4] = 0;
			}
			crc_val = GetCRC16(msg, 5);
			msg[5] = (byte)(crc_val & 0x00FF);
			msg[6] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);
			///////////////////////////////////

			return ret;
		}

		private int bsp_get_dbgInfo(SerialPort port)
		{
			int ret = 0;
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_GET_INFO, 0x00, 0x00, 0x00 };
			UInt16 crc_val;
			
			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}
			
			crc_val = GetCRC16(msg, 4);
			msg[4] = (byte)(crc_val & 0x00FF);
			msg[5] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);
			///////////////////////////////////

			return ret;
		}

		private int bsp_set_sc(SerialPort port)
		{
			int ret = 0;
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_SC, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			UInt16 crc_val;
			UInt32 sc = Convert.ToUInt32(textBoxSc.Text);

			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			msg[4] = (byte)(sc & 0x00FF);
			msg[5] = (byte)((sc & 0xFF00) >> 8);
			msg[6] = (byte)((sc & 0xFF0000) >> 16);
			msg[7] = (byte)((sc & 0xFF000000) >> 24);

			crc_val = GetCRC16(msg, 8);
			msg[8] = (byte)(crc_val & 0x00FF);
			msg[9] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);
			
			return ret;
		}

		private int bsp_set_sr(SerialPort port)
		{
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_SR, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			UInt16 crc_val;
			UInt32 sr = Convert.ToUInt32(textBoxSr.Text);

			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			msg[4] = (byte)(sr & 0x00FF);
			msg[5] = (byte)((sr & 0xFF00) >> 8);
			msg[6] = (byte)((sr & 0xFF0000) >> 16);
			msg[7] = (byte)((sr & 0xFF000000) >> 24);

			crc_val = GetCRC16(msg, 8);
			msg[8] = (byte)(crc_val & 0x00FF);
			msg[9] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);

			return 0;
		}

		private int bsp_set_gain(SerialPort port)
		{
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_GAIN, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			UInt16 crc_val;
			UInt32 gain = Convert.ToUInt32(textBoxGain.Text);
			
			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			msg[4] = (byte)(gain & 0x00FF);
			msg[5] = (byte)((gain & 0xFF00) >> 8);
			msg[6] = (byte)((gain & 0xFF0000) >> 16);
			msg[7] = (byte)((gain & 0xFF000000) >> 24);

			crc_val = GetCRC16(msg, 8);
			msg[8] = (byte)(crc_val & 0x00FF);
			msg[9] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);

			return 0;
		}

		private int bsp_set_ts(SerialPort port)
		{
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_TS, 0x01, 0x00, 0x00, 0x00 };
			UInt16 crc_val;
			byte ts = Convert.ToByte(textBoxTs.Text);

			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			msg[4] = ts;

			crc_val = GetCRC16(msg, 5);
			msg[5] = (byte)(crc_val & 0x00FF);
			msg[6] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);

			return 0;
		}

		private int bsp_update_value()
		{
			byte num = 0;

			if (radioButtonTempus1.Checked)
			{
				num = 0;
			}
			else if(radioButtonTempus2.Checked)
			{
				num = 1;
			}
			else if (radioButtonTempus3.Checked)
			{
				num = 2;
			}
			else if (radioButtonTempus4.Checked)
			{
				num = 3;
			}

			textBoxSc.Text = dbgInfo[num].sC.ToString();
			textBoxSr.Text = dbgInfo[num].sR.ToString();
			textBoxTs.Text = dbgInfo[num].ts.ToString();
			textBoxGain.Text = dbgInfo[num].gain.ToString();

			return 0;
		}

        private int bsp_update_dCdR_value()
        {
            UInt32 dC = 0, dR = 0;

            byte num = 0;

            if (radioButtonTempus1.Checked)
            {
                num = 0;
            }
            else if (radioButtonTempus2.Checked)
            {
                num = 1;
            }
            else if (radioButtonTempus3.Checked)
            {
                num = 2;
            }
            else if (radioButtonTempus4.Checked)
            {
                num = 3;
            }

            dC = dbgInfo[num].dC + Convert.ToUInt32(textBoxDcOffset.Text);
            dR = dbgInfo[num].dR + Convert.ToUInt32(textBoxDrOffset.Text);

            textBoxSc.Text = dC.ToString();
            textBoxSr.Text = dR.ToString();

            return 0;
        }

        private int bsp_get_detector_id(SerialPort port)
		{
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_GET_DETECTOR_ID, 0x0, 0x00, 0x00 };
			UInt16 crc_val;
			
			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			crc_val = GetCRC16(msg, 4);
			msg[4] = (byte)(crc_val & 0x00FF);
			msg[5] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);

			return 0;
		}

		private int bsp_get_version(SerialPort port)
		{
			byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_GET_VER, 0x0, 0x00, 0x00 };
			UInt16 crc_val;

			if ((port == null) || (port.IsOpen == false))
			{
				LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
				return -1;
			}

			crc_val = GetCRC16(msg, 4);
			msg[4] = (byte)(crc_val & 0x00FF);
			msg[5] = (byte)(crc_val >> 8);

			port.Write(msg, 0, msg.Length);
            Delay(300);
			return 0;
		}

        private int bsp_get_serial(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_GET_SER, 0x0, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }

            crc_val = GetCRC16(msg, 4);
            msg[4] = (byte)(crc_val & 0x00FF);
            msg[5] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);
            Delay(300);
            return 0;
        }

        private int bsp_set_serial(SerialPort port, string serial)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_SER, 0x4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            UInt16 crc_val;
            
            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }
            Delay(300);
            UInt32 sn = Convert.ToUInt32(serial);

            msg[4] = (byte)(sn & 0x00FF);
            msg[5] = (byte)((sn & 0xFF00) >> 8);
            msg[6] = (byte)((sn & 0xFF0000) >> 16);
            msg[7] = (byte)((sn & 0xFF000000) >> 24);

            crc_val = GetCRC16(msg, 8);
            msg[8] = (byte)(crc_val & 0x00FF);
            msg[9] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }

        private int bsp_get_alarm(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_GET_ALARM, 0x0, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }

            crc_val = GetCRC16(msg, 4);
            msg[4] = (byte)(crc_val & 0x00FF);
            msg[5] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);
            Delay(300);
            return 0;
        }

        private int bsp_set_alarm(SerialPort port, string alarm)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_SET_ALARM, 0x2, 0x00, 0x00, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }

            UInt16 al = Convert.ToUInt16(alarm);

            msg[4] = (byte)(al & 0x00FF);
            msg[5] = (byte)((al & 0xFF00) >> 8);

            crc_val = GetCRC16(msg, 6);
            msg[6] = (byte)(crc_val & 0x00FF);
            msg[7] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);
            Delay(300);
            return 0;
        }

        private int bsp_get_zmdi_addr(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_GET_ZMDI, 0x1, 0x00, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }

            //msg[4] = Convert.ToByte(textBoxZmdiAddr.Text);
            msg[4] = HexStrToByte(textBoxZmdiAddr.Text);
            crc_val = GetCRC16(msg, 5);
            msg[5] = (byte)(crc_val & 0x00FF);
            msg[6] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }

        private int bsp_set_zmdi_addr(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_ZMDI, 0x4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }
            Delay(300);
            //UInt16 val = Convert.ToUInt16(textBoxZmdiData.Text);
            UInt16 val = HexStrToUInt16(textBoxZmdiData.Text);

            //msg[4] = Convert.ToByte(textBoxZmdiAddr.Text);  // zmdi address
            msg[4] = HexStrToByte(textBoxZmdiAddr.Text);  // zmdi address
            msg[6] = (byte)(val & 0x00FF);
            msg[5] = (byte)((val & 0xFF00) >> 8);
            msg[7] = 0;

            crc_val = GetCRC16(msg, 8);
            msg[8] = (byte)(crc_val & 0x00FF);
            msg[9] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }

        private int bsp_get_curv(SerialPort port)
        {
            int ret = 0;
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_CURV, 0x00, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }

            crc_val = GetCRC16(msg, 4);
            msg[4] = (byte)(crc_val & 0x00FF);
            msg[5] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);
            ///////////////////////////////////
            
            return ret;
        }

        private int bsp_set_pwrComp_param(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_PC_PARAM, 0x2, 0x00, 0x00, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }
            Delay(100);
            //UInt16 val = Convert.ToUInt16(textBoxZmdiData.Text);
            UInt16 val = Convert.ToUInt16(textBoxPcParam.Text);

            msg[4] = (byte)(val & 0x00FF);
            msg[5] = (byte)((val & 0xFF00) >> 8);

            crc_val = GetCRC16(msg, 6);
            msg[6] = (byte)(crc_val & 0x00FF);
            msg[7] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }

        private int bsp_get_pwrComp_param(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_GET_PC_PARAM, 0x0, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }
            Delay(100);
            
            crc_val = GetCRC16(msg, 4);
            msg[4] = (byte)(crc_val & 0x00FF);
            msg[5] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }

        private int bsp_set_tempComp_param(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_SET_TC_PARAM, 0x2, 0x00, 0x00, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }
            Delay(100);
            //UInt16 val = Convert.ToUInt16(textBoxZmdiData.Text);
            UInt16 val = Convert.ToUInt16(textBoxTcParam.Text);

            msg[4] = (byte)(val & 0x00FF);
            msg[5] = (byte)((val & 0xFF00) >> 8);

            crc_val = GetCRC16(msg, 6);
            msg[6] = (byte)(crc_val & 0x00FF);
            msg[7] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }

        private int bsp_get_tempComp_param(SerialPort port)
        {
            byte[] msg = { 0xaa, 0x55, (byte)TpsMsg.BSP_CMD_DBG_GET_TC_PARAM, 0x0, 0x00, 0x00 };
            UInt16 crc_val;

            if ((port == null) || (port.IsOpen == false))
            {
                LOG(CallerName() + "시리얼 포트를 연결해 주세요\n");
                return -1;
            }
            Delay(100);

            crc_val = GetCRC16(msg, 4);
            msg[4] = (byte)(crc_val & 0x00FF);
            msg[5] = (byte)(crc_val >> 8);

            port.Write(msg, 0, msg.Length);

            return 0;
        }
    }
}
