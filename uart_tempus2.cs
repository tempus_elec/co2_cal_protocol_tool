﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Tempus2 = null;

		bool msgFoundSync2 = false;
		byte msgOffset2 = 0;
		byte msgLen2 = 0;
		byte[] msgBuff2 = new byte[MAX_UART_RX_BUFF_LEN];

		void SerialPort_Tempus2_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int intRecSize = sPort_Tempus2.BytesToRead;

			try
			{
				mtx.WaitOne();

				if (intRecSize != 0)
				{
					byte[] buff = new byte[intRecSize];
					int iii;

					iii = sPort_Tempus2.Read(buff, 0, intRecSize);
					
					LOG("Got " + intRecSize + "\n");

					if (intRecSize > TEMPUS_MAX_RX_LEN)
					{
						ERR("Rx got too long..." + intRecSize + "\n");
                        msgFoundSync2 = false;
                        msgOffset2 = 0;
                        msgLen2 = 0;
                    }
                    else
                    {
                        try
                        {
                            if (msgFoundSync2)
                            {
                                byte len = (byte)(msgOffset2 + intRecSize);
                                Buffer.BlockCopy(buff, 0, msgBuff2, msgOffset2, intRecSize);
                                msgOffset2 += (byte)intRecSize;

                                if ((msgLen2 == 0) && (len > 4))
                                {
                                    msgLen2 = (byte)(msgBuff2[3] + 6);
                                }

                                if (len == msgLen2)
                                {
                                    MsgParser(msgBuff2, 1);
                                    msgFoundSync2 = false;
                                    msgOffset2 = 0;
                                    msgLen2 = 0;
                                }
                                else if (len > msgLen2)
                                {
                                    LOG("Msg parser error\n");
                                    msgFoundSync2 = false;
                                    msgOffset2 = 0;
                                    msgLen2 = 0;
                                }
                                else if (len < msgLen2)
                                {

                                }
                            }
                            else
                            {
                                if (msgOffset2 == 0)
                                {
                                    if (intRecSize > 1)
                                    {
                                        for (int i = 0; i < intRecSize; i++)
                                        {
                                            if (buff[i] == 0xbb)
                                            {
                                                if (i > intRecSize)
                                                {
                                                    msgOffset2 = (byte)intRecSize;
                                                }
                                                else
                                                {
                                                    if (buff[i + 1] == 0x66)
                                                    {
                                                        msgFoundSync2 = true;
                                                        Buffer.BlockCopy(buff, 0, msgBuff2, 0, intRecSize);
                                                        msgOffset2 += (byte)intRecSize;

                                                        if (msgLen2 == 0)
                                                        {
                                                            if (msgOffset2 >= 4)
                                                            {
                                                                msgLen2 = (byte)(msgBuff2[3] + 6);
                                                            }

                                                            if (msgLen2 == intRecSize)
                                                            {
                                                                MsgParser(msgBuff2, 1);
                                                                msgFoundSync2 = false;
                                                                msgOffset2 = 0;
                                                                msgLen2 = 0;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff2, msgOffset2, intRecSize);
                                        msgOffset2 = (byte)intRecSize;
                                    }
                                }
                                else
                                {
                                    if ((msgOffset2 + intRecSize) > MAX_UART_RX_BUFF_LEN)
                                    {
                                        ERR("UART Offset is too big\n");
                                        msgFoundSync2 = false;
                                        msgOffset2 = 0;
                                        msgLen2 = 0;
                                    }
                                    else
                                    {
                                        Buffer.BlockCopy(buff, 0, msgBuff2, msgOffset2, intRecSize);

                                        if ((msgBuff2[0] == 0xbb) && (msgBuff2[1] == 0x66))
                                        {
                                            msgFoundSync2 = true;
                                        }
                                        msgOffset2 += (byte)intRecSize;

                                        if (msgLen2 == 0)
                                        {
                                            if (msgOffset2 >= 4)
                                            {
                                                msgLen2 = (byte)(msgBuff2[3] + 6);
                                            }

                                            if (msgLen2 == msgOffset2)
                                            {
                                                MsgParser(msgBuff2, 1);
                                                msgFoundSync2 = false;
                                                msgOffset2 = 0;
                                                msgLen2 = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            ERR(CallerName() + " : " + ex.Message);
                            msgFoundSync2 = false;
                            msgOffset2 = 0;
                            msgLen2 = 0;
                        }
                    }
				}
				mtx.ReleaseMutex();
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				LOG("uart rx error : " + intRecSize + ", " + ex.Message + '\n');
			}
		}

		private void UartOpenTps2(string comName)
		{
			try
			{
				if (null == sPort_Tempus2)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus2 = new SerialPort();
						sPort_Tempus2.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Tempus2_DataReceived);
						sPort_Tempus2.PortName = comName;
						sPort_Tempus2.BaudRate = 9600;
						sPort_Tempus2.DataBits = (int)8;
						sPort_Tempus2.Parity = Parity.None;
						sPort_Tempus2.StopBits = StopBits.One;
						sPort_Tempus2.ReadTimeout = -1;
						sPort_Tempus2.WriteTimeout = -1;
						sPort_Tempus2.NewLine = "\n";
                        try
                        {
                            sPort_Tempus2.Open();
                        }
                        catch (Exception ex)
                        {
                            UartCloseTps2();
                            ERR(CallerName() + " : " + ex.Message);
                        }
                    }
				}

				if (sPort_Tempus2.IsOpen)
				{
					buttonTempus2ComOpen.Enabled = false;
					buttonTempus2ComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");

					comboBoxTps2.SelectedText = comName;
                    bsp_calibration_ctrl(sPort_Tempus2, true);  // port를 열면 바로 CAL cmd를 보내도록 수정
                    CheckBoxUpdate(checkBoxModule2, true);
                    CheckBoxUpdate(checkBoxChart2, true);
                    //PrdTimerStart();
                    if (timer1.Enabled == false)
                    {
                        timer1.Start();
                        buttonStop.Text = "Mon Stop";
                    }
                }
				else
				{
					if (comboBoxTps2.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonTempus2ComOpen.Enabled = false;
					buttonTempus2ComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				UartCloseTps2();
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void UartCloseTps2()
		{
			try
			{
				if (null != sPort_Tempus2)
				{
					if (sPort_Tempus2.IsOpen)
					{
						sPort_Tempus2.Close();
						sPort_Tempus2.Dispose();
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
					sPort_Tempus2 = null;
				}

                buttonTempus2ComOpen.Enabled = true;
                buttonTempus2ComClose.Enabled = false;
            }
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void request_tempus2()
		{
			bsp_get_dbgInfo(sPort_Tempus2);
            LOG("Request TPS2\n");
        }
	}
}
