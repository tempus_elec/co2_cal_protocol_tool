﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
    public partial class Form1 : Form
    {
        SerialPort sPort_Cubic = null;
        double cm1106_ppm = 0;
        int cubicMsgLen = 0;
        byte[] cubicBuf = new byte[16 * 3];

        void SerialPort_Cubic_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int intRecSize = sPort_Cubic.BytesToRead;

            mtx.WaitOne();

            try
            {
                if (intRecSize != 0)
                {
                    byte[] buff = new byte[intRecSize];
                    int iii;

                    iii = sPort_Cubic.Read(buff, 0, intRecSize);

                    try
                    {
                        Buffer.BlockCopy(buff, 0, cubicBuf, cubicMsgLen, intRecSize);
                        cubicMsgLen += intRecSize;
                        if (cubicMsgLen >= 8)
                        {
                            cubicMsgLen = 0;
                            cm1106_ppm = cubicBuf[3] << 8 | cubicBuf[4];
                            LOG("CUBIC : " + cm1106_ppm + " ppm\n");
                            update_cubic_ppm(cm1106_ppm.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        ERR(CallerName() + " : " + ex.Message);
                        cubicMsgLen = 0;
                    }
                }
            }
            catch (System.IndexOutOfRangeException ex)  // CS0168
            {
                LOG("uart rx error : " + ex.Message + '\n');
            }
            mtx.ReleaseMutex();
        }

        private void UartOpenCubic(string comName)
        {
            try
            {
                if (null == sPort_Cubic)
                {
                    if (comName == null)
                    {
                        LOG("Please select serial port!!!");
                    }
                    else
                    {
                        sPort_Cubic = new SerialPort();
                        sPort_Cubic.DataReceived += new SerialDataReceivedEventHandler(SerialPort_Cubic_DataReceived);
                        sPort_Cubic.PortName = comName;
                        sPort_Cubic.BaudRate = 9600;
                        sPort_Cubic.DataBits = (int)8;
                        sPort_Cubic.Parity = Parity.None;
                        sPort_Cubic.StopBits = StopBits.One;
                        sPort_Cubic.ReadTimeout = -1;
                        sPort_Cubic.WriteTimeout = -1;
                        //sPort_Cubic.NewLine = "\n";
                        sPort_Cubic.Open();
                    }
                }

                if (sPort_Cubic.IsOpen)
                {
                    LOG(CallerName() + "Serial port connected... : )\n");
                    CheckBoxSet(checkBoxCubic, true);
                    //PrdTimerStart();
                }
                else
                {
                    if (comboBoxCubic.SelectedItem != null)
                        LOG(CallerName() + "Serial port is in use\n");
                }
            }
            catch (System.Exception ex)
            {
                UartCloseCubic();
                ERR(ex.Message);
            }
        }

        private void UartCloseCubic()
        {
            try
            {
                if (null != sPort_Cubic)
                {
                    if (sPort_Cubic.IsOpen)
                    {
                        sPort_Cubic.Close();
                        sPort_Cubic.Dispose();
                        LOG(CallerName() + "Serial port has been closed\n");
                    }
                    else
                    {
                        LOG(CallerName() + "Serial port closed\n");
                    }
                    sPort_Cubic = null;
                    CheckBoxUpdate(checkBoxCubic, false);
                }
            }
            catch (Exception ex)  // CS0168
            {
                ERR(ex.Message);
            }
        }

        private void update_cubic_ppm(string str)
        {
            TextBoxSet(textBoxCubic, str);
        }

        private void request_cubic()
        {
            byte[] msg = { 0x11, 0x01, 0x01, 0xED };

            cubicMsgLen = 0;

            if ((sPort_Cubic == null) || (sPort_Cubic.IsOpen == false))
            {
                LOG("Please select serial port\n");
                return;
            }
            LOG("Request Cubic\n");

            sPort_Cubic.Write(msg, 0, msg.Length);
        }
    }
}
