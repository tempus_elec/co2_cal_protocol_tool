﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		string cdcPort;
		
		void UartCom(string comName)
		{
			UInt16 isPortFound = 0;

			LOG("COM port finding...\n");
			try
			{
			// Clear ComboBox
				if(comboBoxRef != null)
				{
			        comboBoxRef.Items.Clear();
			        comboBoxRef.BeginUpdate();
					comboBoxRef.EndUpdate();
				}

				if (comboBoxTps1 != null)
				{
			        comboBoxTps1.Items.Clear();
			        comboBoxTps1.BeginUpdate();
					comboBoxTps1.EndUpdate();
				}

				if (comboBoxTps2 != null)
				{
			        comboBoxTps2.Items.Clear();
			        comboBoxTps2.BeginUpdate();
					comboBoxTps2.EndUpdate();
				}

				if (comboBoxTps3 != null)
				{
			        comboBoxTps3.Items.Clear();
			        comboBoxTps3.BeginUpdate();
					comboBoxTps3.EndUpdate();
				}

				if (comboBoxTps4 != null)
				{
                    comboBoxTps4.Items.Clear();
                    comboBoxTps4.BeginUpdate();
                    comboBoxTps4.EndUpdate();
				}

				if (comboBoxExt != null)
				{
                    comboBoxExt.Items.Clear();
                    comboBoxExt.BeginUpdate();
                    comboBoxExt.EndUpdate();
				}

                if (comboBoxWinsen != null)
                {
                    comboBoxWinsen.Items.Clear();
                    comboBoxWinsen.BeginUpdate();
                    comboBoxWinsen.EndUpdate();
                }

                if (comboBoxCubic != null)
                {
                    comboBoxCubic.Items.Clear();
                    comboBoxCubic.BeginUpdate();
                    comboBoxCubic.EndUpdate();
                }

                if (comboBoxElt != null)
                {
                    comboBoxElt.Items.Clear();
                    comboBoxElt.BeginUpdate();
                    comboBoxElt.EndUpdate();
                }

                if (comboBoxFigaro != null)
                {
                    comboBoxFigaro.Items.Clear();
                    comboBoxFigaro.BeginUpdate();
                    comboBoxFigaro.EndUpdate();
                }
                ////////////////////////////////////////////

                // Update COM ports
                foreach (string comport in SerialPort.GetPortNames())
			    {
				    comboBoxRef.Items.Add(comport);
				    comboBoxTps1.Items.Add(comport);
				    comboBoxTps2.Items.Add(comport);
				    comboBoxTps3.Items.Add(comport);
				    comboBoxTps4.Items.Add(comport);
				    comboBoxExt.Items.Add(comport);
                    comboBoxWinsen.Items.Add(comport);
                    comboBoxCubic.Items.Add(comport);
                    comboBoxElt.Items.Add(comport);
                    comboBoxFigaro.Items.Add(comport);
                    LOG("Found " + comport + "\n");

				    isPortFound++;
			    }
				
			    ///////////////////////////////////////////////////

			    CheckForIllegalCrossThreadCalls = false;

			    if (isPortFound != 0)
			    {
				    buttonRefComOpen.Enabled = true;
				    buttonTempus1ComOpen.Enabled = true;
				    buttonTempus2ComOpen.Enabled = true;
				    buttonTempus3ComOpen.Enabled = true;
				    buttonTempus4ComOpen.Enabled = true;
				    buttonExtComOpen.Enabled = true;
                    buttonWinsenComOpen.Enabled = true;
                    buttonCubicComOpen.Enabled = true;
                    buttonEltComOpen.Enabled = true;
                    buttonFigaroComOpen.Enabled = true;
                }
			    else
			    {
				    LOG("Couldn't found available COM port.\n");

				    buttonRefComOpen.Enabled = false;
				    buttonTempus1ComOpen.Enabled = false;
				    buttonTempus2ComOpen.Enabled = false;
				    buttonTempus3ComOpen.Enabled = false;
				    buttonTempus4ComOpen.Enabled = false;
				    buttonExtComOpen.Enabled = false;
                    buttonWinsenComOpen.Enabled = false;
                    buttonCubicComOpen.Enabled = false;
                    buttonEltComOpen.Enabled = false;
                    buttonFigaroComOpen.Enabled = false;
                }
		    }
			catch(Exception ex)
			{
				ERR(ex.Message);
			}
		}
	}
}
