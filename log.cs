﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Drawing;

namespace CO2_CAL_Protocol_Tool
{
	public partial class Form1 : Form
	{
		// log 저장을 위해 path와 file 정의 /////////////////////////////////////////////////
		FileStream file_streamLog;
		StreamWriter psWriterLog;

		public FileStream Config_stream = null;
		public StreamWriter psWriterConfig = null;
		public StreamReader psReaderConfig = null;

		FileStream streamCsv;
		StreamWriter psWriterCsv;

		public string desktop_path;

		DateTime now;

		public void LogInit()
		{
			file_streamLog = null;
			psWriterLog = null;

			now = DateTime.Now;
			desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
			desktop_path = desktop_path + "/CO2_CAL_Protocol_Tool_" + now.ToString("yyyy-MM-dd-H-mm-ss");
			DirectoryInfo di = new DirectoryInfo(desktop_path);
			if (di.Exists == false)
			{
				di.Create();
				OpenLogFiles();
				CsvOpen();
			}
			else
			{
				MessageBox.Show("Log folder creation failed");
			}
		}

		public void OpenLogFiles()
		{
			string path;

			try
			{
				now = DateTime.Now;

				if (psWriterLog == null)
				{
					// folder에 log file 열기

					// Default Log file
					path = desktop_path + "/defaultLog" + ".log";
					file_streamLog = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterLog = new StreamWriter(file_streamLog, System.Text.Encoding.Default);
					psWriterLog.Write("CO2 Default Log File\n");
				}
				else
				{
					MessageBox.Show("Log file creation has problem...\n");
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void CloseLogFiles()
		{
			try
			{
				if (psWriterLog == null)
				{
					LOG("psWriterLog Log file not opened\n");
				}
				else
				{
					psWriterLog.Flush();
					psWriterLog.Close();
					psWriterLog = null;
				}
				CsvClose();
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void ConfigFileOpen()
		{
			StreamReader cfgReader = null;
			string str = null;

			try
			{
				cfgReader = new StreamReader("./tempus.cfg");
				str = cfgReader.ReadLine();
				if (str == "Ports")
				{
					cdcPort = cfgReader.ReadLine();
					if (cdcPort != "")
					{
						LOG("Ref Port : " + cdcPort + "\n");
						UartOpenRef(cdcPort);
						comboBoxRef.Text = cdcPort;
					}
					else
					{
						LOG("Ref Port is null\n");
					}

					cdcPort = cfgReader.ReadLine();
					if (cdcPort != "")
					{
						LOG("Tempus 1 Port : " + cdcPort + "\n");
						UartOpenTps1(cdcPort);
						comboBoxTps1.Text = cdcPort;
					}
					else
					{
						LOG("Tempus 1 Port is null\n");
					}

					cdcPort = cfgReader.ReadLine();
					if (cdcPort != "")
					{
						LOG("Tempus 2 Port : " + cdcPort + "\n");
						UartOpenTps2(cdcPort);
						comboBoxTps2.Text = cdcPort;
					}
					else
					{
						LOG("Tempus 2 Port is null\n");
					}

					cdcPort = cfgReader.ReadLine();
					if (cdcPort != "")
					{
						LOG("Tempus 3 Port : " + cdcPort + "\n");
						UartOpenTps3(cdcPort);
						comboBoxTps3.Text = cdcPort;
					}
					else
					{
						LOG("Tempus 3 Port is null\n");
					}
					
					cdcPort = cfgReader.ReadLine();
					if (cdcPort != "")
					{
						LOG("Tempus 4 Port : " + cdcPort + "\n");
						UartOpenTps4(cdcPort);
						comboBoxTps4.Text = cdcPort;
					}
					else
					{
						LOG("Tempus 4 Port is null\n");
					}
					

					cdcPort = cfgReader.ReadLine();
					if (cdcPort != "")
					{
						LOG("Ext Port : " + cdcPort + "\n");
						UartOpenExt(cdcPort);
						comboBoxExt.Text = cdcPort;
					}
					else
					{
						LOG("Ext Port is null\n");
					}

                    cdcPort = cfgReader.ReadLine();
                    if (cdcPort != "")
                    {
                        LOG("Winsen Port : " + cdcPort + "\n");
                        UartOpenWinsen(cdcPort);
                        comboBoxWinsen.Text = cdcPort;
                    }
                    else
                    {
                        LOG("Winsen Port is null\n");
                    }

                    cdcPort = cfgReader.ReadLine();
                    if (cdcPort != "")
                    {
                        LOG("Cubic Port : " + cdcPort + "\n");
                        UartOpenCubic(cdcPort);
                        comboBoxCubic.Text = cdcPort;
                    }
                    else
                    {
                        LOG("Cubic Port is null\n");
                    }

                    cdcPort = cfgReader.ReadLine();
                    if (cdcPort != "")
                    {
                        LOG("ELT Port : " + cdcPort + "\n");
                        UartOpenElt(cdcPort);
                        comboBoxElt.Text = cdcPort;
                    }
                    else
                    {
                        LOG("ELT Port is null\n");
                    }

                    cdcPort = cfgReader.ReadLine();
                    if (cdcPort != "")
                    {
                        LOG("Figaro Port : " + cdcPort + "\n");
                        UartOpenFigaro(cdcPort);
                        comboBoxFigaro.Text = cdcPort;
                    }
                    else
                    {
                        LOG("Figaro Port is null\n");
                    }
                }
				else
				{
					ERR("Config file error\n");
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void ConfigFileSave(string name)
		{
			StreamWriter cfgWriter = null;
			string str;

			try
			{
				cfgWriter = new StreamWriter("./tempus.cfg");
				cfgWriter.WriteLine("Ports");
#if false
				str = comboBoxRef.Text;
				cfgWriter.WriteLine(str);
				str = comboBoxTps1.Text;
				cfgWriter.WriteLine(str);
				str = comboBoxTps2.Text;
				cfgWriter.WriteLine(str);
				str = comboBoxTps3.Text;
				cfgWriter.WriteLine(str);
				str = comboBoxTps4.Text;
				cfgWriter.WriteLine(str);
				str = comboBoxExt.Text;
				cfgWriter.WriteLine(str);
				//LOG(CallerName() + "tempus.cfg saved with " + name + "\n");
#else
				if(sPort_Ref != null)
				{
					str = sPort_Ref.PortName;
					cfgWriter.WriteLine(str);
				}
				else
				{
					cfgWriter.WriteLine("");
				}

				if (sPort_Tempus1 != null)
				{
					str = sPort_Tempus1.PortName;
					cfgWriter.WriteLine(str);
				}
				else
				{
					cfgWriter.WriteLine("");
				}

				if (sPort_Tempus2 != null)
				{
					str = sPort_Tempus2.PortName;
					cfgWriter.WriteLine(str);
				}
				else
				{
					cfgWriter.WriteLine("");
				}

				if (sPort_Tempus3 != null)
				{
					str = sPort_Tempus3.PortName;
					cfgWriter.WriteLine(str);
				}
				else
				{
					cfgWriter.WriteLine("");
				}

				if (sPort_Tempus4 != null)
				{
					str = sPort_Tempus4.PortName;
					cfgWriter.WriteLine(str);
				}
				else
				{
					cfgWriter.WriteLine("");
				}

				if (sPort_Ext != null)
				{
					str = sPort_Ext.PortName;
					cfgWriter.WriteLine(str);
				}
				else
				{
					cfgWriter.WriteLine("");
				}

                if (sPort_Winsen != null)
                {
                    str = sPort_Winsen.PortName;
                    cfgWriter.WriteLine(str);
                }
                else
                {
                    cfgWriter.WriteLine("");
                }

                if (sPort_Cubic != null)
                {
                    str = sPort_Cubic.PortName;
                    cfgWriter.WriteLine(str);
                }
                else
                {
                    cfgWriter.WriteLine("");
                }

                if (sPort_Elt != null)
                {
                    str = sPort_Elt.PortName;
                    cfgWriter.WriteLine(str);
                }
                else
                {
                    cfgWriter.WriteLine("");
                }

                if (sPort_Figaro != null)
                {
                    str = sPort_Figaro.PortName;
                    cfgWriter.WriteLine(str);
                }
                else
                {
                    cfgWriter.WriteLine("");
                }
#endif
                cfgWriter.Flush();
				cfgWriter.Close();
				cfgWriter = null;
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void _L(string str, Color userColor)
		{
			try
			{
				now = DateTime.Now;
				str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
				if (richTextBoxLog.InvokeRequired)
				{
					richTextBoxLog.Invoke(new MethodInvoker(delegate ()
					{
						richTextBoxLog.SelectionColor = userColor;
						richTextBoxLog.AppendText(str);
						richTextBoxLog.ScrollToCaret();
						psWriterLog.Write(str);
					}));
				}
				else
				{
					richTextBoxLog.SelectionColor = userColor;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
					psWriterLog.Write(str);
				}
			}
			catch(Exception ex)
			{
				ERR(ex.Message);
			}
		}

		public void LOG(string str)
		{
			if (checkBoxLog.Checked == true)
			{
				_L(str, System.Drawing.Color.Black);
			}
		}

		public void ERR(string str)
		{
			if (checkBoxErr.Checked == true)
			{
				_L(str, System.Drawing.Color.Red);
			}

		}

		public void INFO(string str)
		{
			if (checkBoxInfo.Checked == true)
			{
				_L(str, System.Drawing.Color.Blue);
			}
		}

		public void LOG_Clear()
		{
			richTextBoxLog.Clear();
		}

		public string CallerName([CallerMemberName] string callerName = "")
		{
			return callerName;
		}

		private void CsvOpen()
		{
			string path;

			try
			{
				if (psWriterCsv == null)
				{
					// folder에 log file 열기
					path = desktop_path + "/cal" + ".csv";
					streamCsv = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterCsv = new StreamWriter(streamCsv, System.Text.Encoding.Default);
#if false
					for (int i = 0; i < dataGridView1.Columns.Count; i++)
					{
						psWriterCsv.Write(dataGridView1.Columns[i].Name);
						if (i != dataGridView1.Columns.Count)
						{
							psWriterCsv.Write(",");
						}
					}
					psWriterCsv.Write("\n");
#else
					psWriterCsv.Write("Time,No,st0,nt0,st1,nt1,dC,sC,dR,sR,adc,ratio,ppm,temp,ts,gain,Winsen,CAL_Status,REF_Status,Amb,Rh\n");
#endif
					LOG("CSV Opened\n");
				}
				else
				{
					ERR("CSV file creation has problem...\n");
				}
			}
			catch (Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void CsvClose()
		{
			try
			{
				if (psWriterCsv == null)
				{
					ERR("psWriterCsv CSV file not opened\n");
				}
				else
				{
					psWriterCsv.Flush();
					psWriterCsv.Close();
					psWriterCsv = null;
				}
			}
			catch (Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void CSV_SAVE(string str)
		{
			string[] spData = str.Split(' ');

			try
			{
				for (int i = 0; i < (spData.Length-1); i++)
				{
					psWriterCsv.Write(spData[i] + ",");
				}
				psWriterCsv.Write(senseair_co2.ToString() + "," + sht_amb.ToString() + "," + sht_rh.ToString() + ",\n");
                psWriterCsv.Flush();

            }
			catch (Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}
	}
}
